/**
 * 
 * usage: shipit local bld
 * 
 */




// shipitfile.js
module.exports = shipit => {

	// Load shipit-deploy tasks
	require('shipit-deploy')(shipit)

	shipit.initConfig({

		default: {
			//branch: 'master',
			//repositoryUrl: 'https://user@bitbucket.org/am2studio/mix.git'
			//deployTo: '/home/public_html/mix-new',
		},


		local: {
			servers: 'locahost',
		},
		development: {
			servers: 'admin@myzonedev.com',
		},
		staging: {
			servers: 'admin@myzonedev.com',
		},
		production: {
			servers:  'admin@myzonedev.com',
		}
	});
	
	shipit.task('bld', async () => {
		shipit
			.local('gulp build && gulp revision', {
				cwd: './',
			})
			.then( ({ stdout }) => console.log(stdout) )
			.catch( ({ stderr }) => console.error(stderr) )
	});
}