<?php
    /* Template Name: Template - Submit Event */  
	
	get_header(); 
	
	// get post
	global $post; 
?>

<!-- start:content -->
<div id="content">

    <!-- start:main 	-->
    <main id="main" class="main">

        <div class="wrapper">
            <div class="content">

					<div class="submit-event">
						
						<?php
							Timber::render('components/section-bar.twig',[   
								'className'	=> 'section-bar--line section-bar--margin',
								'title'		=> 'Submit Event'
							]);
						?>

						<div class="submit-event__loading js-gf-loading">
							<img src="<?php echo AM2_TEMPPATH; ?>/assets/images/loading.gif" alt="loading..." />
						</div>

						<?php 
							if ( have_posts() ) :  
								while ( have_posts() ) : the_post();
									the_content();
								endwhile; 
							endif; 
						?> 

					</div>

            </div>
        </div>

    </main>
    <!-- end:main -->

</div>
<!-- end:content -->


<?php get_footer(); ?>