<?php

    /* Template Name: Template - Landing */  
    use AM2\Theme\Functions as Helpers; 

    get_header( 'landing' ); 
    
?>
<div id="content" class="content content--landing"> 
    <main id="main" class="main">
        <div class="wrapper">
            <div class="container">
                <div class="col">
                    <?php 
                        $links = Helpers::get_locations_links(); 
                    ?>
                    <div class="locations-select">
                        <span class="locations-select__headline"><?php _e( 'Select your city', 'am2' ); ?></span>
                        <ul class="locations-select__list landing-locations"> 
                        <?php
                        foreach( $links as $item ) {
                            $class = [ 'landing-locations__item-link' ];
                            if( $item['is_selected'] ) $class[] = 'landing-locations__item-link--selected';
    
                            $tmpl = '<a href="%s" class="%s" data-site_id="%s">%s <span>%s</span></a>';
                            $icon = AM2_SPRITEPATH . '#location--' . $item['icon'];
    
                            $li_class = [ 'landing-locations__item' ];
                            if( $item['icon'] === 'globe' ) $li_class[] = 'landing-locations__item--global';

                            ?>
                            <li class="<?php echo esc_attr( implode( ' ', $li_class ) ); ?>">
                                <?php 
                                    printf( $tmpl, 
                                        esc_url( $item['link'] ),
                                        esc_attr( implode( ' ', $class ) ),
                                        absint( $item['ID'] ),
                                        Timber::compile( 'svg-icon.twig', [ 'href' => $icon ] ),
                                        esc_html( $item['label'] )
                                    );
                                ?>
                            </li>
                            <?php
                        }
                        ?>
                        </ul>
                    </div>
                    <?php 
                ?> 
                </div>
            </div>
        </div>
    </main>
    <!-- /main -->
</div>
<!-- /content -->
<?php 

    get_footer( 'landing' ); 
    
?>