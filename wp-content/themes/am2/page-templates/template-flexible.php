<?php

    /* Template Name: Template - Flexible */ 
    use AM2\Theme\WordPressSeo as WPSEO;  

    get_header(); 
    
?>
<div id="content" class="content content--flexible">
    <?php 
        // Render Breadcrumbs
        //WPSEO::renderBreadcrumbs();
    ?>
    <main id="main" class="main">
    <?php 
        if ( have_posts() ) :  
            while ( have_posts() ) : the_post();  
            
            the_content();
            
            endwhile; 
        endif; 
    ?> 
    </main>
    <!-- /main -->
</div>
<!-- /content -->
<?php 

    get_footer(); 
    
?>