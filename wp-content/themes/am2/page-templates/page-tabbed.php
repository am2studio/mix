<?php
/* Template Name: Tabbed */
?>

<?php
$context = [
    'title'		=> get_the_title(),
	'terms'		=> get_field('terms')
];
?>

<?php get_header(); ?>

<!-- start:content -->
<div id="content">

    <!-- start:main 	-->
    <main id="main" class="main">

        <div class="wrapper">
            <div class="container">

                <?php 
                    Timber::render('terms.twig', $context);
                ?>

            </div>
        </div>

    </main>
    <!-- end:main -->

</div>
<!-- end:content -->

<?php get_footer(); ?>