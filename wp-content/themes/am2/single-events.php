<?php

	use AM2\Theme\Functions;
	use AM2\Theme\Models;

	// get post
	global $post; 

	// get event
	$event = (new AM2\Event())->get( $post->ID );

	$eventCategory = ( count($event['categories'])>0 ) ? reset($event['categories']) : [];
	$categoryId = ( $eventCategory != [] ) ? $eventCategory['term_id'] : 0;
	$eventCategory['permalink'] = $eventCategory['color'] = '';
	if( $categoryId ){
		$eventCategory['permalink'] = get_category_link($categoryId);
		$arr = get_option('category_'.$categoryId);
		$eventCategory['color'] = $arr['color'];
	}

	get_header(); 

?>

<!-- start:content -->
<div id="content">

	<!-- start:main -->
	<main id="main" class="main event" role="main">

		<section class="event-top">
			<div class="wrapper">
				<div class="container">
					
					<?php
						Timber::render('components/section-bar.twig',[   
							'className'	=> 'section-bar--line section-bar--margin',
							'title'		=> 'Mix Tix',
							'link'		=> ['url'=>get_site_url() . '/events', 'text'=>'View All Events']
						]);
					?>

				</div>
			</div>
		</section>

		<section>
			<div class="wrapper">
				<div class="container ">

					<div class="clearfix event-post">
						<div class="event-post__media">

							<img src="<?php echo $event['image']['large']['file']; ?>" alt="" />  

							<?php
								Timber::render('components/date-box.twig',[   
									'className'	=> 'event__date-box',
									'months'	=> $event['dates_formatted']['month_range'],
									'days'		=> $event['dates_formatted']['day_range']
								]);
							?>

						</div>
						<div class="event-post__content">

							<?php if( $eventCategory['permalink'] ): ?>
							<a class="event-post__category" href="<?php echo $eventCategory['permalink']; ?>" style="color: <?php echo $eventCategory['color']?>"><?php echo $eventCategory['name']; ?></a>
							<?php endif ?>

							<h1 class="event-post__title">
								<?php echo $post->post_title; ?>
							</h1>

							<?php if ( $event['is_featured'] ): ?>
								<div class="event-post__badge">
									<?php
										Timber::render('svg-icon.twig',[   
											'href'	=> get_template_directory_uri() . '/assets/images/sprite.svg#mix--star',
										]);
									?>
									featured
								</div>
							<?php endif; ?>

							<p class="event-post__label">Date and time</p>
							<h3 class="event-post__date"><?php echo date_i18n( get_option( 'date_format' ) .', '. get_option( 'time_format' ), strtotime($event['event_start']) ); ?></h3>

							<p class="event-post__label">Location</p>
							<div class="event-post__location">
								<p class="event-post__location-name"><?php echo $event['place']['name']; ?></p>
								<p class="event-post__location-address"><?php echo $event['place']['location']['address']; ?>, <?php echo $event['place']['location']['city']; ?></p>
								<a href="javascript:;" class="event-post__location-cta js-goto-map"  style="color: <?php echo $eventCategory['color']?>" >View map</a>
							</div>
                            <?php if( $event['tickets'] ) : ?>
							<a href="<?php echo $event['tickets'];?>" target="_blank" rel="nofollow" class="button event-post__cta">Buy Tickets</a>
							<br /><br />
                            <?php endif;?>

							<?php if( $event['info'] ) : ?>
								<p class="event-post__label">Description</p>
								<p class="event-post__text"><?php echo $event['info']; ?></p>
							<?php endif ; ?>
							
							<p class="event-post__label">Share</p>
							<?php echo Functions::render_social_share( 'post', false, $post->ID ); ?>

						</div>
					</div>
					<?php
						$_gKey = ( get_field( 'google_maps_api_key', 'options' ) ) ? get_field( 'google_maps_api_key', 'options' ) : 'AIzaSyDUnVXRcSvrChmlDntnRXjc_4_8N3EeCIA';
						$map_options = Functions::to_json_string([
							'api_key' => $_gKey,
							'blockId' => 'jsMap',
							'lat' => $event['place']['location']['geo_latitude'],
							'lng' => $event['place']['location']['geo_longitude'],
							'address' => $event['place']['location']['address'] . ', ' . $event['place']['location']['city']
						]);
					?>				
					<div class="event__map" id="jsMap" data-map_options="<?php echo $map_options; ?>">
					</div>
					
				</div>
			</div>
		</section>



		<?php // related (if no related by category, use latest with no category filter )
			$related_blog_post_ids = get_posts(array(
				'post_type'         => 'events',
				'post__not_in'      => array($id),
				'numberposts'       => 3,
				'posts_per_page'    => 3,
				'post_status'       => 'publish',
				'fields'            => 'ids',
				'category'			=> $categoryId
			));
		?>

		<?php if ($related_blog_post_ids) : ?>

			<section class="event-related">
				<div class="wrapper">
					<div class="container ">

						<?php
							Timber::render('components/section-bar.twig',[   
								'className'	=> 'section-bar--margin',
								'title'		=> 'Related Events',
								'link'		=> ['url'=>get_site_url() . '/events', 'text'=>'View All Events']
							]);
						?>

						<div class="clearfix">

							<?php foreach ($related_blog_post_ids as $related_event_id) :

								$relatedEvent = (new AM2\Event())->get($related_event_id);
								$related_featured_id = get_post_thumbnail_id($related_event_id);
								$thumb = get_the_post_thumbnail($related_event_id,'article_thumb');
								if( !$thumb ){
									$thumb = '<img class="card__image" src="' . AM2_TEMPPATH . '/assets/images/no-image-article-thumb.png">';
								}
								?>

								<?php
									$context = [
										'className'     => 'card--vertical card--date-box col-13',
										'permalink'     => get_the_permalink($related_event_id),
										'title'         => get_the_title($related_event_id),
										'thumb'         => $thumb,
										'hasVideo'      => get_field('has_video', $related_event_id),
										'hasGallery'    => get_field('has_gallery', $related_event_id),
										'date'          => date_i18n( 'l, M d - g A', strtotime($relatedEvent['event_start']) ),
										'dateBox'		=> $relatedEvent['dates_formatted'],
										'isFeatured'    => $relatedEvent['is_featured'],
										'location'      => $relatedEvent['place']['location']
									];
									// retrieve category data
									$categories = get_the_category();
									// set first category or no category
									$context['category'] = ( count($categories)>0 ) ? reset($categories) : [];
									$categoryId = ( $context['category'] != [] ) ? $context['category']->cat_ID : 0;
									if( $categoryId ){
										$context['category']->permalink = get_category_link($categoryId);
										$arr = get_option('category_'.$categoryId);
										$context['category']->color = $arr['color'];
									}
									// render view
									Timber::render('card.twig',$context);
								?>

							<?php endforeach; ?>

						</div>
						
					</div>
				</div>
			</section>

		<?php endif; ?>

		<section class="events-submit">
			<div class="wrapper">
				<div class="container">

					<h3 class="events-submit__title">Submit Upcoming Events</h3>
					<p class="events-submit__text">Do you have an upcoming event that you want featured on The MIX?</p>
					<a class="events-submit__cta button" href="<?php print network_site_url('submit-event'); ?>">Submit event here</a>
					
				</div>
			</div>
		</section>

	</main>
	<!-- end:main -->

</div>
<!-- end:content -->

<?php 

	// Get Footer
	get_footer();
	
?>