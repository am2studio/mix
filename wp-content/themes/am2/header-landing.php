<!DOCTYPE html>
<html class="no-js" <?php language_attributes( 'html' ); ?>> 
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Title Should be managed by WP with add_theme_support -->
		<?php $favicon = get_field( 'favicon', 'options' ); ?>
        <?php if( ! empty( $favicon ) ) { ?>
            <link rel="icon" href="<?php echo $favicon['url']; ?>">
        <?php } ?> 
		<?php wp_head(); ?> 
		<?php
			$tracking_codes_head = get_field( 'tracking_codes_head', 'options' );
			if ( ! empty( $tracking_codes_head ) ) {
				echo $tracking_codes_head;
			}
		?> 
		<style>.no-js .menu-container{display:none;}</style>
	</head> 
	<body <?php body_class(); ?>>

		<?php
			$tracking_codes_body = get_field( 'tracking_codes_body', 'options' );
			if ( ! empty( $tracking_codes_body ) ) {
				echo $tracking_codes_body;
			}
		?>  
		<div class="site-container"> 
			<header id="header" class="header header--landing">
				<div class="wrapper">
					<div class="container"> 
						<?php 
							$site_logo = get_field( 'branding', 'options' )['landing'];  
						?>
						<h1 class="logo">
							<a href="<?php echo site_url(); ?>" class="logo__link">
								<?php if( !empty( $site_logo ) ) { ?>
									<img class="logo__img" src="<?php echo $site_logo['url']; ?>" alt="<?php bloginfo(); ?>" />
								<?php } else { 
									bloginfo();
								} ?>
							</a>
						</h1>  
						<p class="site-tagline"><?php _e( 'Welcome to MIX Society', 'am2' ); ?></p>
					</div>
				</div>
			</header>
			<!--/header -->
		