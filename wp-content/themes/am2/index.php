<?php get_header(); ?>

<!-- start:content -->
<div id="content">

    <!-- start:main 	-->
    <main id="main" class="main archive">

        <div class="wrapper">
            <div class="container">


                <div class="section__header">
                    <h2 class="section__headline"><?php _e( 'Articles', 'am2' ); ?></h2>
                </div>

                <div class="archive-wrap">
                    <?php
                        if( have_posts() ) :
                            $i = 0;
                            while ( have_posts() ) :
                                $i++;
                                the_post();

                                $id                 = get_the_id();
                                if($i == 1) { // Different image and text size for featured (1st) article
                                    $excerpt = wp_trim_words( get_the_excerpt(), 80, '...' );
                                    $thumb   = get_the_post_thumbnail($id,'article_featured');
                                } else {
                                    $excerpt = wp_trim_words( get_the_excerpt(), 20, '...' );
                                    $thumb   = get_the_post_thumbnail($id,'article_thumb');
                                }

                                if( !$thumb ){
                                    $thumb = '<img class="card__image" src="' . AM2_TEMPPATH . '/assets/images/no-image-article-thumb.png">';
                                }

                                $context = [
                                    'content'       => $excerpt,
                                    'permalink'     => get_the_permalink(),
                                    'title'         => get_the_title(),
                                    'thumb'         => $thumb,
                                    'hasVideo'      => get_field('has_video' ),
                                    'hasGallery'    => get_field('has_gallery'),
                                    'date'          => get_the_date('l, M d - g A'),
                                    'className'     => 'card--'.$i,
                                ];

                                // retrieve category data
                                $categories = get_the_category();
                                // set first category or no category
                                $context['category'] = ( count($categories)>0 ) ? reset($categories) : [];
                                $categoryId = ( $context['category'] != [] ) ? $context['category']->cat_ID : 0;
                                if( $categoryId ){
                                    $context['category']->permalink = get_category_link($categoryId);
                                    $arr = get_option('category_'.$categoryId);
                                    $context['category']->color = $arr['color'];
                                }


                                if($i == 1) {
                                    echo '<div class="archive__col archive__col--left">';
                                }

                                if($i == 2) { // Adding subscribe block
                                     get_template_part( 'subscribe' );
                                }

                                 // render view
                                Timber::render('card.twig',$context);

                                if($i == 3) {
                                    echo '</div>';
                                    echo '<div class="archive__col archive__col--right">';
                                }

                        ?>
                        <?php endwhile; ?>
                            </div>
                            <?php if ( $wp_query->max_num_pages > 1 ) : ?>

                            <?php
                                $args = [];
                                $args_pagination = [
                                    'mid_size' => 3,
                                ];

                                $args['pagination'] = Timber::get_pagination($args_pagination);

                                Timber::render( 'pagination.twig', $args );
                            ?>

                            <?php endif; ?>
                        <?php  else :
                            echo '<p class="text--center no-posts">' . __( 'No posts found.', 'am2' ) . '</p>';
                        endif;
                    ?>
                </div>
            </div>
        </div>

    </main>
    <!-- end:main -->

</div>
<!-- end:content -->

<?php get_footer(); ?>