<?php
	// Use
	use AM2\Theme\Models;

    global $wp_query;

    // Replicate another query
    $videos = [];

    if( ! isset( $_GET['type'] ) && $_GET['type'] !== 'post' ) {
        $videos = new WP_Query([
            'post_type'     => 'video',
            'post_status'   => 'publish',
            'posts_per_page'=> $wp_query->get('posts_per_page'),
            's'             => $wp_query->get('s')
        ]);
    }

    $queries = [
        'video'    => $videos,
        'post'     => $wp_query
    ];

    $total_results = $wp_query->found_posts + $videos->found_posts;

    $class = [ 'content', 'content--search-results' ];
    if( isset( $_GET['type'] ) && in_array( $_GET['type'], [ 'post', 'video' ] ) ) {
        $class[] = 'content--search-' . $_GET['type'];
    }

	// Get Header
	get_header();
?>

<div id="content" class="<?php echo esc_attr( implode( ' ', $class ) ); ?>">
    <main id="main" class="main search-results">
        <section class="section section--search-header search-header">
            <div class="wrapper">
                <div class="container">
                    <h4 class="search__results">
                        <?php
                            // incorrect translation usage
                            // echo $wp_query->found_posts; _e(' results found for "'. get_search_query().'"', 'am2')
                        ?>
                        <?php
                            if( isset( $_GET['type'] ) && in_array( $_GET['type'], [ 'post', 'video' ] ) ) {
                                $type_label = $_GET['type'] === 'post' ? __( 'article', 'am2' ) : __( 'video', 'am2' );
                                $total_results .= ' ' . $type_label;
                            }
                            printf( __( '%s results found for "%s"', 'am2' ), $total_results, get_search_query() );
                        ?>
                    </h4>
                </div>
            </div>
        </section>
        <?php
            if( $total_results === 0 ) :
        ?>
        <div class="search-section search-section--no-results">
            <div class="wrapper">
                <div class="container">
                    <p>Suggestions</p>
                    <ul class="list">
                        <li>— Make sure all the words are properly written.</li>
                        <li>— Try using other keywords.</li>
                        <li>— Try using the general keywords.</li>
                    </ul>
                </div>
            </div>
        </div>
        <?php
            endif;

            foreach( $queries as $key => $current_query ) :
                if( $key === 'post' || isset( $_GET['type'] ) && $_GET['type'] === 'post' ) $label = __( 'Article', 'am2' );
                if( $key === 'video' || isset( $_GET['type'] ) && $_GET['type'] === 'video' ) $label = __( 'Video', 'am2' );

                if( $current_query !== [] && $current_query->have_posts() ) :
        ?>
        <section class="search-section search-section--<?php echo esc_attr( $key ); ?>">
            <div class="wrapper">
                <div class="container">
                    <div class="section__header">
                        <?php
                            // It does the job but should be like bellow for propper translation
                            // _e( $label . ' results', 'am2' );
                        ?>
                        <h2 class="section__headline"><?php
                            printf( __( '%s results', 'am2' ), $label );
                        ?></h2>
                        <?php if( $current_query->found_posts > 9 ) :
                            $search_url = add_query_arg( [
                                's'     => get_search_query(),
                                'type'  => $key
                            ], home_url() );
                            if( isset( $_GET['type'] ) && in_array( $_GET['type'], [ 'post', 'video' ] ) ) {
                                $search_url = remove_query_arg( 'type', $search_url );
                            }
                        ?>
                        <a href="<?php echo esc_url( $search_url ); ?>"
                            class="section__more"><?php
                                $key = ( $key === 'post' ) ? 'article' : $key;
                                $string = __( 'View all %s results', 'am2' );
                                if( isset( $_GET['type'] ) && in_array( $_GET['type'], [ 'post', 'video' ] ) ) {
                                    $string = __( 'Return to main results', 'am2' );
                                }
                                printf( $string, $key );
                        ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="container container--flex search-section__results">
                <?php
                    while( $current_query->have_posts() ) {
                        $current_query->the_post();
                        $post_type = get_post_type();

                        $class = [ 'search__item', 'search__item--' . $post_type, 'col-13' ];
                        $class = implode( ' ', $class );
                        echo '<div class="' . esc_attr( $class ) . '">';

                        if( $post_type === 'post' ) {

                            $args = [];
                            $args['post'] = Models::post( get_post() );
                            $args['post']['className'] = 'card--search';
                            unset( $args['post']['content'] );
                            unset( $args['post']['date'] );
                            unset( $args['post']['location'] );
                            
                            Timber::render( 'news-block.twig', $args ); 
                        }

                        if ( $post_type === 'video' ) {

                            $args = Models::video( get_post() );
                            if( ! has_post_thumbnail( get_post() ) )
                                $args['image'] = 'https://img.youtube.com/vi/' . $args['id'] . '/mqdefault.jpg';
                                
                            Timber::render( 'video-block_side.twig', $args );
                            
                        }

                        echo '</div>';

                        wp_reset_postdata();
                    }

                    // Run pagination if we are on Specific CPT Query
                    if(
                        isset( $_GET['type'] ) && in_array( $_GET['type'], [ 'post', 'video' ] ) &&
                        $current_query->max_num_pages > 1
                    ) {
                        $args = [];
                        $args['pagination'] = Timber::get_pagination([ 'mid_size' => 3 ]);
                        Timber::render( 'pagination.twig', $args );
                    } 
                    ?>
                </div>
                <?php if( ! isset( $_GET['type'] ) ) : ?>
                <div class="container search-section__more">
                    <div class="text text--center">
                        <a href="<?php echo esc_url( $search_url ); ?>" class="button button--primary"><?php
                            printf( __( 'View all %s results', 'am2' ), $key );
                        ?></a>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </section> <!-- ./search-section -->
        <?php
                endif; // end query have posts

                wp_reset_query(); // reset current query

            endforeach;  // end query loop
        ?>
    </div>
    </main>
    <!-- end:main -->
</div>
<!-- end:content -->

<?php get_footer(); ?>