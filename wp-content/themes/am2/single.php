<?php

    use AM2\Theme\Functions;

	get_header();
    $id = get_the_ID();
?>

<!-- start:content -->
<div id="content">

	<!-- start:main -->
	<main id="main" class="main">

        <div class="wrapper">
            <div class="container clearfix">

                <div class="col-23 single-post__left">
                    <?php if ( have_posts() ) : ?>
                        <?php while ( have_posts() ) : the_post(); ?>
                            <?php
                                // retrieve content

                                $id = get_the_id();

                                $article_youtube_url  = get_post_meta($id,'article_youtube_url', true);

                                if(!empty($article_youtube_url)) {
                                    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $article_youtube_url, $match);
                                    $youtube_id = $match[1];
                                }

                                $authorId = get_the_author_meta( 'ID' );

                                if(has_post_thumbnail() && empty($youtube_id)) {
                                    $image = get_the_post_thumbnail($id,'article_featured');
                                }


                                $context = [
                                    'featured_image'=> $image,
                                    'youtube_video' => $youtube_id,
                                    'content'       => get_the_content(),
                                    'title'         => get_the_title(),
                                    'date'          => get_the_date(),
                                    'author'        => get_the_author_meta('display_name', $authorId),
                                    'author_url'    => get_author_posts_url( $authorId ),
                                    'hasVideo'      => get_field('has_video'),
                                    'hasGallery'    => get_field('has_gallery'),
                                    'socialShare'   => Functions::render_social_share( 'post', false, get_the_ID() )
                                ];
                                // retrieve category data
                                $categories = get_the_category();
                                // set first category or no category
                                $context['category'] = ( count($categories)>0 ) ? reset($categories) : [];
                                $categoryId = ( $context['category'] != [] ) ? $context['category']->cat_ID : 0;
                                if( $categoryId ){
                                    $context['category']->permalink = get_category_link($categoryId);
                                    $arr = get_option('category_'.$categoryId);
                                    $context['category']->color = $arr['color'];
                                }
                                // gallery
                                $gallery = [];
                                $images = get_field('photo');
                                if( is_array($images) && count($images) > 0 ){
                                    foreach( $images AS $imageId ){
                                        $arrImg = [];
                                        $arrImg['id'] = $imageId['photo_image'];
                                        $arrImg['thumb'] = wp_get_attachment_image_url( $imageId['photo_image'], 'post_photo_thumb' );
                                        $arrImg['large'] = wp_get_attachment_image_url( $imageId['photo_image'], 'full' );
                                        array_push( $gallery, $arrImg );
                                    }
                                }else{
                                    $gallery = [];
                                }
                                $context['gallery'] = $gallery;

                                // set view
                                Timber::render('post.twig',$context);
                            ?>
                        <?php  endwhile; ?>
                    <?php endif; ?>
                </div>

                <div class="col-14 single-post__right no-margin">

                    <?php // related (if no related by category, use latest with no category filter )
                    $latest_blog_post_ids = get_posts(array(
                        'post_type'         => 'post',
                        'post__not_in'      => array($id),
                        'numberposts'       => 5,
                        'posts_per_page'    => 5,
                        'post_status'       => 'publish',
                        'fields'            => 'ids',
                        'category'          => $categoryId
                    ));?>

                    <?php if ($latest_blog_post_ids) : ?>

                        <div class="sidebar">

                            <?php
                                Timber::render('components/section-bar.twig', [
                                    'className' => '',
                                    'title'     => 'Related Articles',
                                    'link'      => ['url'=>$context['category']->permalink, 'text'=>'View All']
                                ]);
                            ?>

                            <div class="clearfix">
                                <?php foreach ($latest_blog_post_ids as $latest_post_id) :
                                    $latest_featured_id = get_post_thumbnail_id($latest_post_id);
                                    $thumb = get_the_post_thumbnail($latest_post_id,'article_thumb');
                                    if( !$thumb ){
                                        $thumb = '<img class="card__image" src="' . AM2_TEMPPATH . '/assets/images/no-image-article-thumb.png">';
                                    }
                                    ?>
                                    <?php
                                        $context = [
                                            'className'     => 'card--sidebar',
                                            'permalink'     => get_the_permalink($latest_post_id),
                                            'title'         => get_the_title($latest_post_id),
                                            'thumb'         => $thumb,
                                            'hasVideo'      => get_field('has_video', $latest_post_id),
                                            'hasGallery'    => get_field('has_gallery', $latest_post_id)
                                        ];
                                        // retrieve category data
                                        $categories = get_the_category();
                                        // set first category or no category
                                        $context['category'] = ( count($categories)>0 ) ? reset($categories) : [];
                                        $categoryId = ( $context['category'] != [] ) ? $context['category']->cat_ID : 0;
                                        if( $categoryId ){
                                            $context['category']->permalink = get_category_link($categoryId);
                                            $arr = get_option('category_'.$categoryId);
                                            $context['category']->color = $arr['color'];
                                        }
                                        // render view
                                        Timber::render('card.twig',$context);
                                    ?>

                                <?php endforeach; ?>
                            </div>

                        </div>

                    <?php endif; ?>

                </div>

            </div>
        </div>

        <section class="single-related">
            <div class="wrapper">
                <div class="container clearfix">

                    <?php
                        $archive_url = get_post_type_archive_link( 'post' );
                    ?>

                    <?php
                        Timber::render('components/section-bar.twig', [
                            'className' => 'section-bar--margin',
                            'title'     => 'Latest Articles',
                            'link'      => ['url'=>$archive_url, 'text'=>'View All']
                        ]);
                    ?>

                    <div class="clearfix">

                        <?php // related (if no related by category, use latest with no category filter )
                        $related_blog_post_ids = get_posts(array(
                            'post_type'         => 'post',
                            'post__not_in'      => array($id),
                            'numberposts'       => 4,
                            'posts_per_page'    => 4,
                            'post_status'       => 'publish',
                            'fields'            => 'ids'
                        ));?>

                        <?php if ($related_blog_post_ids) : ?>
                            <?php foreach ($related_blog_post_ids as $related_post_id) :
                                $related_featured_id = get_post_thumbnail_id($related_post_id);
                                $thumb = get_the_post_thumbnail($related_post_id,'article_thumb');
                                if( !$thumb ){
                                    $thumb = '<img class="card__image" src="' . AM2_TEMPPATH . '/assets/images/no-image-article-thumb.png">';
                                }
                                ?>
                                <?php
                                    $context = [
                                        'className'     => 'card--vertical col-14',
                                        'permalink'     => get_the_permalink($related_post_id),
                                        'title'         => get_the_title($related_post_id),
                                        'thumb'         => $thumb,
                                        'hasVideo'      => get_field('has_video', $related_post_id),
                                        'hasGallery'    => get_field('has_gallery', $related_post_id),
                                        'date'          => get_the_date('F j, Y '),
                                        'content'       => wpautop( wp_trim_words( get_the_excerpt() !== '' ? get_the_excerpt() : get_the_content(), 15, '...' ) )
                                    ];
                                    // retrieve category data
                                    $categories = get_the_category();
                                    // set first category or no category
                                    $context['category'] = ( count($categories)>0 ) ? reset($categories) : [];
                                    $categoryId = ( $context['category'] != [] ) ? $context['category']->cat_ID : 0;
                                    if( $categoryId ){
                                        $context['category']->permalink = get_category_link($categoryId);
                                        $arr = get_option('category_'.$categoryId);
                                        $context['category']->color = $arr['color'];
                                    }
                                    // render view
                                    Timber::render('card.twig',$context);
                                ?>

                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div>

                </div>
            </div>
        </section>

	</main>
	<!-- end:main -->

</div>
<!-- end:content -->

<?php get_footer(); ?>