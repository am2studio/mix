<?php namespace AM2\Theme\Walkers;
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit(); 
// Use
use Walker_Nav_Menu as Walker_Nav_Menu;

// Menu Walker Class
class Menu extends Walker_Nav_Menu {   
    /*
     * Add vertical menu class and submenu data attribute to sub menus
     */   
    function start_lvl( &$output, $depth = 0, $args = array() ) {
		// Depth CSS Classes
		$indent 	= ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
		$classes 	= array( 'menu', 'menu-item__dropdown' );
		$classes[] 	= 'menu-item__dropdown--depth-' . $depth;
		$classes 	= implode( ' ', $classes ); // Dropdown Classes
	  
		// Built the HTML
		$output .= "\n" . $indent . '<ul class="' . esc_attr( $classes ) . '">' . "\n";
    }
	/*
     * Element
     */ 
	function start_el( &$output, $item, $depth = 0, $args = Array(), $id = 0 ) {

		$indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
	  
		// Passed CSS Classes
		$_classes 	= empty( $item->classes ) ? array() : (array) $item->classes;
		$replace	= array(
			'menu-item-' 			=> 'menu-item--',
			'current-menu-ancestor' => 'menu-item--current-ancestor',
			'current-menu-parent' 	=> 'menu-item--current-parent',
			'current-menu-item' 	=> 'menu-item--current-item',
			'post_type' 			=> 'post-type'
		);
		$classes 	= array();
		$classes 	= str_replace( array_keys( $replace ), $replace, $_classes ); // Replace default classes with BEM Classes
		$classes[] 	= 'menu-item--depth-' . $depth; // Add The Depth Class
		$classes 	= implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
	  
		// Menu Item Start
		$output .= $indent . '<li id="menu-item-'. $item->ID . '" class="' . esc_attr( $classes ). '">';
	  
		// Menu Item Link Attributes
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) . '"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) . '"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) . '"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) . '"' : '';
		$attributes .= ' class="menu-item__link ' . ( $depth > 0 ? 'menu-item__link--dropdown' : 'menu-item__link--main' ) . '"';
	  
		$item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
			$args->before,
			$attributes,
			$args->link_before,
			$item->title,
			$args->link_after,
			$args->after
		);
	  
		// Add the link to .output
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}