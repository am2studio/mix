<?php namespace AM2\Theme;
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit();

// Use
use AM2\Theme\Functions as Helpers;

// Video
class Video {
	/**
	 * Instance
	 *
	 * @access 	private
	 * @var 	object
	 */
	private static $instance;

	/**
	 * Initiator
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	/**
	 * Class Constructor.  
	 * Contains the functions used on video posts 
	 * @uses - set_post_views()
	 * @uses - set_post_meta() 	
	 */
	public function __construct() {  
		add_action( 'acf/save_post', [ $this, 'set_meta' ], 20 ); // To be able to read get_field( 'name' )
	}

    /**
	 * Set Post
     * @param  int   $postID { WP_Post id } 
	 */
	public static function set_post_views( $postID ) {
		if( is_user_logged_in() ) return;

        $user_ip = $_SERVER['REMOTE_ADDR']; 
		$key = $user_ip . '/' . $postID; 
		$value = array( $user_ip, $postID ); 
		$visited = get_transient( $key ); 
		
		if ( false === ( $visited ) ) {  
			set_transient( $key, $value, 60*60*12 );  
			$count_key = 'views';
			$count = get_post_meta( $postID, $count_key, true );
			if( $count == '' ) {
				$count = 0;
				delete_post_meta( $postID, $count_key );
				add_post_meta( $postID, $count_key, '0' );
			} else {
				$count++;
				update_post_meta( $postID, $count_key, $count );
			} 
		}
	}  

	/**
	 * Update Youtube Views
     * @param  int   $postID { WP_Post id } 
	 */
	public static function update_youtube_views( $post_id ) { 
		$key = 'am2/transient/' . $post_id . '/yt_views_updated';  

		$api_key = get_field( 'google_maps_api_key', 'options' ); // 'AIzaSyDlfUMbtmX_u5YgWXQdGKHrExdGMLzgbVM';
		$api_key = $api_key ? $api_key : 'AIzaSyDlfUMbtmX_u5YgWXQdGKHrExdGMLzgbVM';
		
		if ( false === ( $visited = get_transient( $key ) ) && $api_key ) {  
			set_transient( $key, $post_id, 60*60*12 );  

			$video = get_field( 'youtube_url', $post_id );
			parse_str( parse_url( $video, PHP_URL_QUERY ), $videoVars );
			$video_id = $videoVars['v']; 

			$api_url = add_query_arg( array(
				'part'  => 'statistics',
				'id'	=> $video_id,
				'key'	=> $api_key
			), esc_url_raw( 'https://www.googleapis.com/youtube/v3/videos' ) );  

			if( ! is_wp_error( $request = wp_remote_get( $api_url ) ) ) { 
				// Get Body Decoded as array
				$data = json_decode( wp_remote_retrieve_body( $request ), true );  
				if( array_key_exists( 'error', $data ) ) return; 
				// Data 
				$statistics = $data['items'][0]['statistics'];  
				Helpers::set_post_meta( $post_id, '_YT_viewCount', $statistics['viewCount'] ); 
			}   
		}
	}

	/**
	 * Set Videos Metadata
	 * @param 	int		$post_id 
	 * @return 	mixed	bail early / save action 
	 */
	public function set_meta( $post_id ) {
		if( get_post_type( $post_id ) !== 'video' ) return;

		$api_key = get_field( 'google_maps_api_key', 'options' ); // 'AIzaSyDlfUMbtmX_u5YgWXQdGKHrExdGMLzgbVM';
		$api_key = $api_key ? $api_key : 'AIzaSyDlfUMbtmX_u5YgWXQdGKHrExdGMLzgbVM';

		if( $api_key ) { 
			$video = get_field( 'youtube_url', $post_id );
			parse_str( parse_url( $video, PHP_URL_QUERY ), $videoVars );
			$video_id = $videoVars['v'];
			
			$api_url = add_query_arg( array(
				'part'  => 'snippet,contentDetails,statistics,player',
				'id'	=> $video_id,
				'key'	=> $api_key
			), esc_url_raw( 'https://www.googleapis.com/youtube/v3/videos' ) );  

			if( ! is_wp_error( $request = wp_remote_get( $api_url ) ) ) { 
				// Get Body Decoded as array
				$data = json_decode( wp_remote_retrieve_body( $request ), true ); 

				if( array_key_exists( 'error', $data ) ) return;

				// Data
				$player = $data['items'][0]['player'];
				$snippet = $data['items'][0]['snippet'];
				$details = $data['items'][0]['contentDetails'];
				$statistics = $data['items'][0]['statistics']; 

				// Save some meta 
				Helpers::set_post_meta( $post_id, '_YT_publishedAt',	$snippet['publishedAt'] );
				Helpers::set_post_meta( $post_id, '_YT_duration',		$details['duration'] 	);  
				Helpers::set_post_meta( $post_id, '_YT_viewCount',		$statistics['viewCount']); 
			}  
		} 
	}

	/**
	 * Convert YT video duration to time
	 * @param 	string 	$youtube_time
	 */
	public static function duration_to_time( $youtube_time ) { 
		if( ! $youtube_time ) $youtube_time = 'PT0S'; // Get current if no Youtube Time 
		$start = new \DateTime( '@0' ); // Unix epoch
		$start->add( new \DateInterval( $youtube_time ) ); 
		$start = $start->format( 'H:i:s' ); 
		$exploded = explode( ':', $start );
		if( $exploded[0] == '00' ) return implode( ':', [ $exploded[1], $exploded[2] ] );
		else return $start;
	} 

	/**
	 * Convert time to seconds only
	 * @param 	string  $time
	 */
	public static function time_to_seconds( $time ) { 
		$time = preg_replace( "/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $time );
		sscanf( $time, "%d:%d:%d", $hours, $minutes, $seconds );
		return $hours * 3600 + $minutes * 60 + $seconds;
	}

	/**
	 * Generate Iframe From YT link
	 * @param 	string $video { video url }
	 */
	public static function generate_iframe( $video ) {
		return preg_replace(
			"/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
			"<iframe src=\"//www.youtube.com/embed/$2\" allowfullscreen frameborder=\"0\"></iframe>",
			$video
		);
	}

	/**
	 * Get Video Id
	 * @param 	string $video { video url }
	 */
	public static function get_video_id( $video ) {
		if( ! $video ) return;
		parse_str( parse_url( $video, PHP_URL_QUERY ), $videoVars );  
		if( $videoVars['v'] ) return $videoVars['v'];
		return; 
	}
}