<?php namespace AM2\Theme;

global $env;
$env = constant( 'ENV' );

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit();
class Functions {
	/**
	 * Instance
	 *
	 * @var $_instance
	 */
	private static $_instance = NULL;

	/**
	 * Initiator
	 *
	 * @since 	v3.3
	 * @return 	object
	 */
	public static function get_instance() {
		if( self::$_instance == NULL ) self::$_instance = new self;
		return self::$_instance;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		/**
		 * How to use this class
		 * {
		 * This methods are called statically and since this is namespaced you can do it with the following
		 * 1 - A the top of the file you want to use this type use AM2\Theme\Functions\Helpers as YourDesiredName (or just use)
		 * 2 - call a specific helper function from this class with the following: YourDesiredName::to_json_string([
		 * 		'name' => 'Testing'
		 * ])
		 * }
		 */
	} 

	/**
	 * Set or update existing post meta
     * @param  	int   	$post_id 
     * @param 	string 	$key
	 * @param 	string 	$value
	 * @param 	bool	$single
	 */
	public static function set_post_meta( $post_id, $key = '', $value, $single = true ) {
		if( ! $post_id ) $post_id = get_the_ID(); 
		$value = trim( $value );
		if( get_post_meta( $post_id, $key, $single ) ) { 
			update_post_meta( $post_id, $key, $value ); 
		} else add_post_meta( $post_id, $key, $value );
	}

	/**
	 * Helper function to encode an array to an excaped json string
	 * Useful to use it for placing encoded object in html attrs
	 * Eg: passing slick slider attributes inline instead of writting another script tag
	 * @param 	array $json
	 * @return 	string
	 */
	public static function to_json_string( $json = array() ) {
		if( ! is_array( $json ) ) return '';
		$json = str_replace( '"', "'", json_encode( $json ) );
		return htmlspecialchars( $json, ENT_QUOTES, 'UTF-8' );
	}

	/**
	 * Detect active plugin by constant, class or function existence.
	 * @param 	array 	$plugins Array of array for constants, classes and / or functions to check for plugin existence.
	 * @return 	bool 	True if plugin exists or false if plugin constant, class or function not detected.
	 */
	public static function detect_plugin( array $plugins ) {
		// Check for classes.
		if ( isset( $plugins['classes'] ) ) foreach ( $plugins['classes'] as $name ) if ( class_exists( $name ) ) return true;
		// Check for functions.
		if ( isset( $plugins['functions'] ) ) foreach ( $plugins['functions'] as $name ) if ( function_exists( $name ) ) return true;
		// Check for constants.
		if ( isset( $plugins['constants'] ) ) foreach ( $plugins['constants'] as $name ) if ( defined( $name ) ) return true;
		// No class, function or constant found to exist.
		return false;
	}

	/**
	 * Get Contact Methods set in ACF
	 * Added since most of the websites uses this kind of element in footer/header/menu or contact page
	 * @param 	array $methods (acf repeater with type and label fields)
	 * ===============================
	 * @return 	array $methods (formated array with icons/link/label)
	 */
	public static function get_contact_methods( array $methods ) {
		if( $methods === false ) return [];

		$new_methods = [];
		foreach( $methods as $method ) {
			$icon = '';
			$link = '';
			if( $method['type'] === 'fax' ) $icon = AM2_SPRITEPATH . '#icon--fax';
			if( $method['type'] === 'phone' ) $icon = AM2_SPRITEPATH . '#icon--tel';
			if( $method['type'] === 'fax' || $method['type'] === 'phone' ) {
				$number = filter_var( $method['content'], FILTER_SANITIZE_NUMBER_INT );
				$link = 'tel:' . $number;
			}
			if( $method['type'] === 'email' ) {
				$icon = AM2_SPRITEPATH . '#icon--mail';
				$email = filter_var( $method['content'], FILTER_SANITIZE_EMAIL );
				$link = 'mailto:' . $email;
			};

			$new_methods[] = [
				'type' 	=> $method['type'],
				'icon' 	=> $icon,
				'link' 	=> $link,
				'label'	=> $method['content']
			];
		}

		return $new_methods;
	}

	/**
	 * Render Contact Methods
	 * @todo 	Transfer the HTML into twig file
	 * @uses 	function self 		- get_contact_methods to format an acf field
	 * @param 	array 	$field		- the acf field to render from
	 * @param 	array	$skip		- which keys to skips ( eg, default - used as text only - address  )
	 * @param	string	$modifier	- BEM Modifier, is appended with --modifier to the class
	 * @param	boolean	$echo		- echo/return the html
	 * ===================================================
	 * @return 	string	$methods
	 */
	public static function render_contact_methods( array $field, $skip = [], $modifier, $echo = true ) {
		$output = '';

		if( $methods = self::get_contact_methods( $field ) ) {
			$class = [ 'contact-methods' ];
			if( $modifier && is_string( $modifier ) ) $class[] = 'contact-methods--' . esc_attr( $modifier );

			$output .= '<ul class="' . implode( ' ', $class ) . '">';

			foreach( $methods as $method ) {
				if( in_array( $method['type'], (array) $skip ) ) continue;

				$template = '<li class="contact-methods__item">%s</li>';
				$link_tmp = '<a class="contact-methods__link" href="%s">%s</a>';
				$icon_tmp = '<svg class="contact-methods__svg"><use xlink:href="%s"></use></svg>';
				$text_tmp = '<span class="contact-methods__label">' . $method['label'] . '</span>';

				if( $method['icon'] !== '' ) $text_tmp = sprintf( $icon_tmp, $method['icon'] ) . $text_tmp;
				if( $method['link'] !== '' ) $text_tmp = sprintf( $link_tmp, $method['link'], $text_tmp );

				// Render an item
				$output .= sprintf( $template, $text_tmp );
			}

			$output .= '</ul>';
		}

		if( $echo ) echo $output;
		else return $output;
	}

	/**
	 * Render Social Profiles
	 * @param 	string	$bem_modifier
	 * @param 	boolean	$title
	 */
	public static function render_social_links( $bem_modifier = '', $title = false ) {
		$output 	= '';

		// Get Options
		$socials 	= get_field( 'social_profiles', 'options' );

		// Make Classes
		$_classes 	= array( 'social', 'social--profiles' );
		if( $bem_modifier ) $_classes[] = 'social--' . $bem_modifier;
		$classes 	= implode( ' ', $_classes );


		if( ! empty( $socials ) && is_array( $socials ) ) {

			$output .= '<div class="' . esc_attr( rtrim( $classes ) ) . '">';

			if ( $title != false && is_string( $title ) ) :
				$output .= '<p class="social__title">' . __( 'We are social!', 'am2' ) . '</p>';
			endif;

			$output .= '<ul class="social__list clearfix">';
			foreach( $socials as $key => $value ) {
				$name   = trim( strtolower( $value['name'] ) );
				$url    = esc_url( $value['link'] );
				$sprite = esc_url( AM2_SPRITEPATH . '#social--' . $name );

				$svg  = sprintf( '<svg class="svg-icon svg-icon--social-profiles"><use xlink:href="%1s"></use></svg>', $sprite );
				$link = sprintf( '<a class="social__link" href="%1s" target="_blank">%2s</a>', $url, $svg );
				$item = sprintf( '<li class="social__item social__item--%1s">%2s</li>', $name, $link );
				$output .= $item;
			}
			$output .= '</ul>';
			$output .= '</div>';
		}

		return $output;
	}

	/**
	 * Generates social sharing icon buttons
	 * @param 	string 		$bem_modifier
	 * @param	bool/string	$title
	 * @param	string		$id
	 * @return	string		HTML
	 */
	public static function render_social_share( $bem_modifier = '', $title = false, $id = '' ) {
		if ( empty( $id ) ) {
			global $post;
			$id = $post->ID;
		}
	 
		$socials 	= array();
	
		$options 	= get_field( 'social_sharing', 'options' );
		
		$_classes 	= array( 'social', 'social--sharing' );
		if( $bem_modifier ) $_classes[] = 'social--' . esc_html( $bem_modifier );
		$classes 	= implode( ' ', $_classes );
	
		$postTitle 	= get_the_title( $id );
		$link  		= get_permalink( $id );
		$image 		= get_the_post_thumbnail_url( $id );
	
		if ( ( $options['linkedin'] ) == '1'  ) {
			$socials['linkedin']['href'] = 'https://www.linkedin.com/';
			$socials['linkedin']['link'] = 'https://www.linkedin.com/shareArticle?url=' . $link . '';
		}
	
		if ( ( $options['facebook'] ) == '1' ) {
			$socials['facebook']['href'] = 'https://www.facebook.com/';
			$socials['facebook']['link'] = 'https://www.facebook.com/sharer/sharer.php?u=' . $link . '&title=' . $postTitle . '';
		}
	
		if ( ( $options['pinterest'] ) == '1' ) {
			$socials['pinterest']['href'] = 'https://www.pinterest.com/';
			$socials['pinterest']['link'] = 'https://pinterest.com/pin/create/bookmarklet/?media=' . $image . '&url=' . $link . '&is_video=false&description=' . $postTitle . '';
		}
	
		if ( ( $options['twitter'] ) == '1' ) {
			$socials['twitter']['href'] = 'https://www.twitter.com/';
			$socials['twitter']['link'] = 'https://twitter.com/intent/tweet?status=' . $postTitle . '+' . $link . '';
		}
	
		if ( ( $options['google'] ) == '1' ) {
			$socials['googleplus']['href'] = 'https://plus.google.com/';
			$socials['googleplus']['link'] = 'https://plus.google.com/share?url=' . $link . '';
		}
	
		if ( ( $options['email'] ) == '1' ) {
			$socials['email']['href'] = home_url();
			$socials['email']['link'] = 'mailto:?subject=' . $postTitle . '&amp;body=Found an awesome post. Read it here: ' . $link . '';
		}

		if( ! empty( $socials ) ) {
			$output = '';
			$output .= '<div class="' . esc_attr( $classes ) . '">';
	
			if ( $title != false && is_string( $title ) ) :
				$output .= '<p class="social__title">' . __( 'Share this article', 'am2' ) . '</p>';
			endif;
	
			$output .= '<ul class="social__list clearfix">';
	
			foreach( $socials as $key => $social ) {
				$icon = AM2_SPRITEPATH . '#social--' . $key; 
	
				$output .= '<li class="social__item social__item--' . $key . '">';
				$output .= '<a class="social__link" rel="nofollow" href="' . $social['href'] . '" onclick="';
				$output .= 'popUp=window.open(\'' . $social['link'] . '\', \'popupwindow\', \'scrollbars=yes,width=800,height=400\');popUp.focus(); return false">';
				$output .= '<svg class="svg-icon svg-icon--sharing"><use xlink:href="' . $icon . '"></use></svg>';
				$output .= '</a></li>';
			}
	
			$output .= '</ul>';
			$output .= '</div>';
		}
	
		return $output;
	}

	/**
	 * Retrieve location links
	 * @see 	wp_get_sites() if automation is wanted
	 * @return	array	$links
	 */
	public function get_locations_links() {
		// Get only from main site since let's not waste time to set it up everywhere
		$sites = get_field( 'location_links', 'options' );
		if( empty( $sites ) ) {
			switch_to_blog( 1 );
			$sites = get_field( 'location_links', 'options' );
			restore_current_blog();
		}

		// Before the variable bellow, which we use for current option
		$current = trailingslashit( get_site_url( get_current_blog_id() ) );

		$links = [];
		if( ! empty( $sites ) ) :
			foreach( $sites as $key => $site ) {
				$wp_blog = get_blog_details( $site['id'] );
				$city_name = $site['label'] ? $site['label'] : $wp_blog['blogname'];
				$site_url = $site['url'] ? $site['url'] : get_site_url( $wp_blog->blog_id );
				$site_url = trailingslashit( $site_url );

				$links[] = [
					'ID'	=> $site['id'],
					'label' => $city_name,
					'link'	=> $site_url,
					'icon'	=> ( 'global' === strtolower( $city_name ) ) ? 'globe' : 'pin',
					'is_selected' => ( $current === $site_url ) ? true : false
				];
			}
		endif;

		return $links;
	}

	/**
	 * Location select
	 * @param	string	$bem_modifier
	 * @param	string 	$label
	 */
	public function render_location_switcher( $bem_modifier = '', $label = '' ) {
		// Get only from main site since let's not waste time to set it up everywhere
		$sites = self::get_locations_links();
		if( ! empty( $sites ) ) {
			$class = [ 'location-select' ];
			if( $bem_modifier ) $class[] = 'location-select--' . $bem_modifier;
			?>
			<div class="<?php echo esc_attr( implode( ' ', $class ) ); ?>">
				<?php if ( $label != false && is_string( $label ) ) : ?>
				<p class="location-select__label"><?php echo esc_html( $label ); ?></p>
				<?php endif; ?>
				<span class="location-select__input">
					<span class="location-select__input-icon"><?php
						$icon = AM2_SPRITEPATH . '#location--pin';
						\Timber::render( 'svg-icon.twig', [ 'href' => $icon ] );
					?></span>
					<select name="location-select" id="location-select" class="location-select__field">
					<option value="" disabled><?php _e( 'Choose Location', 'am2' ); ?></option>
					<?php
						foreach( $sites as $key => $site ) { ?>
							<option value="<?php echo esc_attr( $site['link'] ); ?>"
							<?php if( $site['is_selected'] ) echo ' selected'; ?>><?php
								echo esc_html( $site['label'] );
							?></option>
							<?php
						}
					?>
					</select>
				</span>
			</div>
		<?php
		}
	}

	/**
	 * Get and render global site link with icon
	 * @param 	integer	$global_id { id of the global site }
	 */
	public function render_global_site_link( $global_id = 3, $label = '' ) {
		$global = get_blog_details( $global_id );
		?>
		<div class="location-visit-global">
			<?php if ( $label != false && is_string( $label ) ) : ?>
			<p class="location-visit-global__label"><?php echo esc_html( $label ); ?></p>
			<?php endif; ?>
			<a class="location-visit-global__link" href="<?php echo esc_url( get_site_url( $global->blog_id ) ); ?>">
				<span class="location-visit-global__icon"><?php
					$icon = AM2_SPRITEPATH . '#location--globe';
					\Timber::render( 'svg-icon.twig', [ 'href' => $icon ] );
				?></span>
				<span><?php _e( 'Global', 'am2' ); ?></span>
			</a>
		</div>
		<?php
	}
}