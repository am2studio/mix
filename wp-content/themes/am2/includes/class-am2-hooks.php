<?php namespace AM2\Theme;
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit();

// Use
use AM2\Theme\Support\WordPressSeo as WPSEO;
use AM2\Theme\Functions as Helpers;

// Hooks
class Hooks {
	/**
	 * Instance
	 *
	 * @access 	private
	 * @var 	object
	 */
	private static $instance;

	/**
	 * Initiator
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	/**
	 * Class Constructor.
	 * Contains action/filter hooks
	 */
	public function __construct() {
		// ACF Dependency
		add_action( 'admin_notices', 	array( $this, 'check_acf' ) );

		// Theme support
		add_action( 'after_setup_theme', array( $this, 'theme_support' ) );

		// Tiny MCE args
		add_filter( 'tiny_mce_before_init', array( $this, 'tiny_mce_args' ) );
		add_filter( 'mce_buttons_2', array( $this, 'tiny_mce_fontsize_args' ) );

		// SVG Thumbnail Fix
		add_filter( 'upload_mimes',	array( $this, 'svg_mime_types' ) );

		// Add Entry Post Cclass / Remove Hentry
		add_filter( 'post_class', 	array( $this, 'post_classes' ) );

		// ACF Google Maps API
		add_filter( 'acf/fields/google_map/api', array( $this, 'acf_google_map_api' ) );

		// Disable CF7 Style
		if( function_exists( 'wpcf7_load_css' ) ) add_filter( 'wpcf7_load_css', '__return_false' );

		// Disable Admin Bar
		add_filter( 'show_admin_bar', '__return_false' );

		// Disable Gutenberg for Pages
		add_filter( 'use_block_editor_for_post_type', function( $can_edit, $post_type ) {
			if( ! ( is_admin() && ! empty( $_GET['post'] ) ) ) return $can_edit;
			if( $this->gutenberg_disable( $_GET['post'] ) ) $can_edit = false;
			if( $post_type === 'page' ) $can_edit = false;
			return $can_edit;
		}, 10, 2 );

		// Add Additional Body Classes
		add_filter( 'body_class',	array( $this, 'body_classes' ) );

		// Add extra field to category edit form hook
		add_action ( 'edit_category_form_fields', array( $this, 'extra_category_fields') );

		// Save extra category extra field hook
		add_action ( 'edited_category', array( $this, 'save_extra_category_fileds') );

		add_action( 'init', [$this, 'redirect_301_old_links'] );

		// Additional Hooks (actions/filters) Bellow
		Hooks\GForms::get_instance();
		Hooks\Queries::get_instance();
		Video::get_instance(); // Video Helpers ( constructor has a hook that runs on save post )
	}

	public function redirect_301_old_links() {

	    $current_site_id = get_current_blog_id();

	    $old_categories = [
            'action-sports',
            'arts-music',
            'cannabis',
            'city-people',
            'creator-community',
            'events-entertainment',
            'sightseeing',
            'food-drink',
            'health-wellness',
            'technology',
            'the-mix-now'
        ];

	    $request_uri = $_SERVER['REQUEST_URI'];


	    if( strlen($request_uri) < 2 ) {
	        return;
        }

        if( substr($request_uri, -1) == '/' ) {
            $request_uri = substr($request_uri, 0, -1);
        }


        $parts = explode("/", $request_uri);


	    $match = array_intersect($old_categories, $parts);



	    if( !is_array($match) || count($match) == 0 ) {
	        return;
        }

        $pos = strrpos($request_uri, '/');


        $id = $pos === false ? $request_uri : substr($request_uri, $pos + 1);

        if( empty($id) ) {
            return;
        }

        // All old posts are in Vancouver site ID = 2
        switch_to_blog(2);

        global $wpdb;

        $sql = "SELECT ID FROM $wpdb->posts WHERE (post_name = %s OR post_name LIKE %s) AND post_type = 'post'";


        $results = $wpdb->get_results($wpdb->prepare($sql, $id, '%' .$id . '-%'));

        if( !$results || !is_array($results)) {
           return false;
        }

        $actual_permalink = get_permalink($results[0]->ID);
        switch_to_blog($current_site_id);

        if( $actual_permalink ) {
            wp_redirect($actual_permalink, 301);
            exit();
        }

        return;


    }

	/**
	 * Adds custom classes to the array of body classes.
	 * @param 	array $classes Classes for the body element.
	 * @return 	array
	 */
	public function body_classes( $classes ) {

		if( is_main_site( get_current_blog_id() ) ) {
			$classes[] = 'am2-is-main-site';
		} else {
			$classes[] = 'am2-is-not-main-site';
		}

		if( $f_content = get_field( 'flexible_content' ) ) {
			// Add "blocks" classes to body so you can check for it
			// and use JSManager routes to load JS if the block is used on page
			$blocks = array_column( $f_content, 'acf_fc_layout' );
			foreach( $blocks as $block ) $classes[] = 'am2-block-' . $block;
		}

		$siteId = get_current_blog_id();
		$classes[] = 'am2-is-site-' . $siteId;

		return $classes;
	}

	/**
	 * Gutenberg Killswitch
	 */
	public function gutenberg_disable( $id = false ) {
		$excluded_templates = array(
			'page-templates/template-flexible.php'
		);

		$excluded_ids = array(
			get_option( 'page_on_front' )
		);

		if( empty( $id ) ) return false;

		$id = intval( $id );
		$template = get_page_template_slug( $id );

		return in_array( $id, $excluded_ids ) || in_array( $template, $excluded_templates );
	}

	/**
	 * Tiny MCE Args
	 */
	public function tiny_mce_args( $options ) {
		$options['height']           = '250px';
		$options['fontsize_formats'] = '10px 12px 13px 14px 15px 16px 18px 20px 22px 24px 26px 28px 30px 32px 34px 36px 38px 40px 42px 44px 46px 48px 64px';

		return $options;
	}

	/**
	 * Tiny MCE Font Size Args
	 */
	public function tiny_mce_fontsize_args( $options ) {
		array_shift( $options );
		array_unshift( $options, 'fontsizeselect' );
		array_unshift( $options, 'fontweightselect' );
		array_unshift( $options, 'formatselect' );

		return $options;
	}

	/**
	 *  Enable svg images in media uploader
	 */
	public function svg_mime_types( $mimes ) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}

	/**
	 *  Add Google Maps API Key to ACF
	 */
	public function acf_google_map_api( $api ) {
		$api['key'] = get_field( 'google_maps_api_key', 'options' );
		return $api;
	}

	/**
	 * Filter classes to the array of post classes.
	 */
	public function post_classes( $classes ) {
		if ( is_admin() ) return $classes;
		// Add "entry" to the post class array
		$classes[] = 'entry';
		// Remove "hentry" from post class array
		$classes = array_diff( $classes, array( 'hentry' ) );
		return $classes;
	}

	/**
	 * Add theme support
	 */
	public function theme_support() {
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		/**
		 * All thumbnails are administrated here
		 */
		add_image_size( 'am2_shop_catalog', 265, 240, true );
		add_image_size( 'article_thumb', 273, 183, true );
		add_image_size( 'article_featured', 680, 680, true ); // the container is 580px but may get larger
		add_image_size( 'post_photo_thumb', 130, 130, true );
		add_image_size( 'video_large', 915, 550, true ); // Single Video
		add_image_size( 'video_thumb', 355, 200, true ); // Single/Archive
		add_image_size( 'video_category_featured', 700, 305, true );
		add_image_size( 'video_category_block', 350, 455, true );
		add_image_size( 'event_featured', 370, 210, true );
	}

	/**
	 * Check for dependencies and notify the user
	 */
	function check_acf() {
		// Check if ACF is activated
		if ( ! class_exists( 'acf' ) ) {
			echo '<div class="notice notice-error">
					<p>' . __( 'AM2 Theme is dependant on Advanced Custom Fields plugin. Please install and activate it.', 'am2' ) . '</p>
				</div>';
		} else {
			return;
		}
	}

	//add extra fields to category edit form callback function
	function extra_category_fields( $tag ) {    //check for existing featured ID
			$t_id = $tag->term_id;
			$cat_meta = get_option( "category_$t_id");
		?>
		<tr class="form-field">
		<th scope="row" valign="top"><label for="extra1"><?php _e('Color'); ?></label></th>
		<td>
		<input type="color" name="Cat_meta[color]" id="Cat_meta[color]" size="25" style="width:60%;" value="<?php echo $cat_meta['color'] ? $cat_meta['color'] : '#ffffff'; ?>"><br />
				<span class="description"><?php _e('Color'); ?></span>
			</td>
		</tr>
		<?php
	}

	// save extra category extra fields callback function
	function save_extra_category_fileds( $term_id ) {
		if ( isset( $_POST['Cat_meta'] ) ) {
			$t_id = $term_id;
			$cat_meta = get_option( "category_$t_id");
			$cat_keys = array_keys($_POST['Cat_meta']);
				foreach ($cat_keys as $key){
				if (isset($_POST['Cat_meta'][$key])){
					$cat_meta[$key] = $_POST['Cat_meta'][$key];
				}
			}
			//save the option array
			update_option( "category_$t_id", $cat_meta );
		}
	}
}