<?php namespace AM2\Theme\Hooks;
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit(); 

// Hooks
class GForms {
	/**
	 * Instance
	 *
	 * @access 	private
	 * @var 	object
	 */
	private static $instance;

	/**
	 * Initiator
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	/**
	 * Class Constructor.
	 * Contains action/filter hooks
	 */
	public function __construct() { 
		// Gforms input to button
		add_filter( 'gform_submit_button', 		[ $this, 'gform_submit_button' ], 		10, 2 );
		add_filter( 'gform_ajax_spinner_url', 	[ $this, 'gform_ajax_spinner_url' ], 	10, 2 ); 
	}

	/**
	 * Gforms spinner url
	 */
	public function gform_ajax_spinner_url( $image_src, $form ) {
		return AM2_TEMPPATH . '/resources/svg-builder/gforms/spinner.svg';
	}

	/**
	 * Gforms input to button
	 */
	public function gform_submit_button( $button, $form ) { 
		$html = "<button class='button gform_button button--primary button--full-width' id='gform_submit_button_{$form['id']}'>";
		$html .= "<span>" . $form['button']['text'] . "</span>"; 
		$html .= '<span class="button__icon"></span>'; 
		$html .= "</button>";
		return  $html;
	} 
}