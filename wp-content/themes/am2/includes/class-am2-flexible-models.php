<?php namespace AM2\Theme\Flexible;
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit();

// Use
use AM2\Theme\Video;
use AM2\Theme\Functions;
use AM2\Theme\Models as Get;

// Flexible Layout Models
Models::get_instance();
class Models {

	/**
	 * Instance
	 *
	 * @access 	private
	 * @var 	object
	 */
	private static $instance;

	/**
	 * Initiator
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	/**
	 * Constructor
	 * You can always split this files into multiple smaller files, either functional bases or class based
	 * I would advise to include related files to this flexible content into this file instead of functions php
	 * If autoloader will be aproved to be used in am2 theme with class based files, you will simply call a Class::get_instance();
	 * method without the need of include filename.
	 */
	function __construct() {
		// Main Flexible
		add_filter( 'the_content', array( $this, 'flexible_layout' ) );

		/**
		 * Make sure you are not overriding the passed arguments
		 * You need to append to the existing ones with this dynamic filter
		 * @uses {
		 * 	1 - acf_fc_layout key should never be overriden( is used to pull the twig template )
		 *  2 - block_position key contains the block position in the flexible_content
		 *  3 - all other arguments you pass to it ( including ACF fields - so you can use them directly in twig)
		 * }
		 */
		// Custom ARGS passed to all
		$all_blocks = [
			'files',
			'articles',
			'accordion',
			'latest_news'
		];
		foreach( $all_blocks as $block ) add_filter( 'am2/filter/flexible/' . $block . '/model', [ $this, 'args_to_blocks' ] );

		// Blocks That Require PHP
		//add_filter( 'am2/filter/flexible/entry_hero/model', 		[ $this, 'flexible_entry_hero_model' 	] );
		add_filter( 'am2/filter/flexible/articles/model',			[ $this, 'flexible_articles_model' 	] );
		add_filter( 'am2/filter/flexible/files/model', 				[ $this, 'flexible_files_model' 	] );
		add_filter( 'am2/filter/flexible/latest_news/model', 		[ $this, 'flexible_latest_news_model' 	] );
		add_filter( 'am2/filter/flexible/videos/model', 			[ $this, 'flexible_latest_videos_model' 	] );
		add_filter( 'am2/filter/flexible/featured_videos/model',	[ $this, 'flexible_featured_videos_model' 	] );
		add_filter( 'am2/filter/flexible/video_categories/model', 	[ $this, 'flexible_video_categories_model' 	] );
		add_filter( 'am2/filter/flexible/double_column/model', 		[ $this, 'flexible_2_col_model' 	] );
		add_filter( 'am2/filter/flexible/columns/model', 			[ $this, 'flexible_columns_model' 	] );
		add_filter( 'am2/filter/flexible/subscribe/model', 			[ $this, 'flexible_subscribe_model' ] );
		add_filter( 'am2/filter/flexible/slider/model', 			[ $this, 'flexible_slider_model' 	] );

		// Filter to change the default twig filename of a specific block
		add_filter( 'am2/filter/flexible/videos/view', function() {
			// Here block_name is the ACF fieldname in the Flexible Content
			return 'latest-videos.twig';
		} );
	}

	/**
	 * Add Flexible Layout to content
	 * @return string
	 */
	public function flexible_layout( $data ) {
		$flexible 	= new Layout( get_field( 'flexible_content' ) );
		$output 	= $flexible->render();

		$data .= $output;

		return $data;
	}

	/**
	 * Custom ARGS Added to all blocks
	 * @uses - use this array to pass args that you want to add to all blocks
	 */
	public function args_to_blocks( $args ) {
		$args['AM2_TEMPPATH']	= AM2_TEMPPATH;
		$args['AM2_SPRITEPATH']	= AM2_SPRITEPATH;
		return $args;
	}

	/**
	 * Add Flexible Layout to Double Column Block
	 * @uses 	Flexible\Layout
	 * @return 	array ( with mixed data )
	 * This fields passes rendered html into a render key of the block
	 * You can do this also in twig directly but since it uses the same
	 * blocks, I want to avoid setting up another twig template for child blocks
	 */
	public function flexible_2_col_model( $args ) {
		if( $args['double_column'] ) {
			$parent_position = $args['block_position'];
			foreach( $args['double_column'] as $key => $block ) {
				$flexible = new Layout( $block );
				$args['render'][$key] = $flexible->render();
				// Render method contains the rendered HTML
			}
		}

		return $args;
	}

	/**
	 * Add Flexible Layout to Multiple Columns Block
	 * @uses 	Flexible\Layout
	 * @return 	array ( with mixed data )
	 * This fields passes rendered html into a render key of the block
	 * You can do this also in twig directly but since it uses the same
	 * blocks, I want to avoid setting up another twig template for child blocks
	 */
	public function flexible_columns_model( $args ) {
		if( $args['columns'] ) {
			$args['column_count'] = count( $args['columns'] );
			$columns = $args['columns'];
			unset( $args['columns'] );
			foreach( $columns as $key => $block ) {
				$flexible = new Layout( $block['content'] );
				$args['columns'][$key]['html'] = $flexible->render();
				// Render method contains the rendered HTML
			}
		}

		return $args;
	}

	/**
	 * Slider
	 */
	public function flexible_slider_model( $args ) {

		foreach ( $args['slides'] as $key => $slide ) {

			if ( $slide['mode'] ) {
				$post_type = $slide['post_type']; // Post type returns article or video
				$id = $slide[$post_type];

				if( $post_type == 'article') {
					$categories = get_the_category( $id );
				} else {
					$categories = wp_get_post_terms( $id, 'video_category' );
					$category = count( $categories ) > 0 ? reset( $categories ) : false;
					if( $video_url 	= get_field( 'youtube_url', $id ) ) {
						$video_id = Video::get_video_id( $video_url );
						$args['slides'][$key]['video'] = [
							'url' 	=> $video_url,
							'image' => 'https://img.youtube.com/vi/'. $video_id . '/maxresdefault.jpg'
						];
						$args['iframe_options'] = Functions::to_json_string([
							'videoID' 			=> $video_id,
							'triggerSelector'	=> 'event', // On Slick Change
							'srcVars' 			=> [
								'modestbranding' => '1',
								'autoplay' 	=> '1',
								// https://stackoverflow.com/questions/40685142/youtube-autoplay-not-working
								'mute'		=> '1', // Autoplay with sound is not permited by Chrome
								// From Youtube Docs
								'rel' 		=> '0', // As of sep 2018, 0 show related from channel (cannot be disabled)
								'controls' 	=> '0',
								'hd' 		=> '1',
								'loop'		=> '1',
								'playlist'	=> $video_id
							]
						]);
					}
				}
				$args['slides'][$key]['headline'] 	= get_the_title( $id );
				$args['slides'][$key]['link']['url']		= get_the_permalink( $id );
				$args['slides'][$key]['caption']	= $category ? $category->name : false;
			}
		}

		$args['slick_options'] = Functions::to_json_string([
			"slidesToShow" 		=> 1,
			"slidesToScroll" 	=> 1,
			"autoplay"			=> false,
			"autoplaySpeed" 	=> $args['options']['duration'],
			"pauseOnHover"		=> true,
			"dots" 		=> false,
			"arrows" 	=> false,
			"fade" 		=> true,
		]);

		return $args;
	}

	/**
	 * Subscribe
	 */
	public function flexible_subscribe_model( $args ) {

		if( $args['subscribe_options'] === 'global' ) {
			$args['title'] 			= get_field('subscribe_title','options');
			$args['text'] 			= get_field('subscribe_text','options');
			$args['image']          = get_field('subscribe_background_image','options');
			$args['form'] 			= do_shortcode(get_field('subscribe_form_shortcode','options'));
		}

		return $args;
	}
	/**
	 * Featured Videos
	 */
	public function flexible_featured_videos_model( $args ) {

		if( $args['videos'] ) :
			$videos = $args['videos'];
			unset( $args['videos'] );
			$args['columns'] = [
				'left' => [],
                'right'=> [],
            ];
            if(sizeof($videos) <= 2) {
                $args['two_or_less'] = true;
            }
			foreach ( $videos as $key => $video ) :
				$model = Get::video( $video );
				unset( $model['category']['link'] );
				unset( $model['category']['name'] );
				if( $key < 2 ) {
					if( has_post_thumbnail( $video ) ) $model['image'] = get_the_post_thumbnail_url( $video->ID, 'large' );
					else $model['image'] = 'https://img.youtube.com/vi/' . $model['id'] . '/maxresdefault.jpg';
					$args['columns']['left'][] = $model;
				} else $args['columns']['right'][] = $model;
			endforeach;
		endif;

		return $args;
	}

	/**
	 * Latest News
	 */
	public function flexible_latest_news_model( $args ) {
 		if( $args['mode'] === 'manual' ) {
			if( $args['posts'] ) :
				$posts = $args['posts'];
				unset( $args['posts'] );
				$args['columns'] = [
					'left' => [],
					'right'=> []
				];
				foreach ( $posts as $key => $post ) :
					$model = Get::post( $post );
					if( $key < 1 ) {
						$content = $post->post_excerpt ? $post->post_excerpt : $post->post_content;
						$thumbnail = get_post_thumbnail_id( $post );
						$model['content'] = wp_trim_words( $content, 80, '...' );
						$model['thumb'] = wp_get_attachment_image( $thumbnail, 'article_featured' );
						$args['columns']['left'][] = $model;
					} else $args['columns']['right'][] = $model;
				endforeach;
			endif;
		} else {
			$query = [
				'post_type'     => 'post',
				'post_status'   => 'publish',
				'posts_per_page'=> '5'
			];
			$args['link'] = [];
			$args['link']['title'] = __( 'View More', 'am2' );
			if( ! empty( $args['category'] ) ) {
				$query['category'] = $args['category'];
				$args['link']['url'] = get_term_link( $args['category'], 'category' );
			} else {
				$args['link']['url'] = get_post_type_archive_link( 'post' );
			}

			$query = new \WP_Query( $query );

			if( $query->have_posts() ) :
				unset( $args['posts'] );
				while ( $query->have_posts() ) :
					$query->the_post();
					$model = Get::post( get_post() );
					if( $query->current_post < 1 ) {
						$thumbnail = get_post_thumbnail_id( $post );
						$post = get_post();
						$content = $post->post_excerpt ? $post->post_excerpt : $post->post_content;
						$model['content'] = wp_trim_words( $content, 80, '...' );
						$model['thumb'] = wp_get_attachment_image( $thumbnail, 'article_featured' );
						$args['columns']['left'][] = $model;
					} else $args['columns']['right'][] = $model;
					wp_reset_postdata();
				endwhile;
			endif;

			wp_reset_query();
		}

		return $args;
	}

	/**
	 * Latest Videos Model
	 */
	public function flexible_latest_videos_model( $args ) {
		$query = [
			'post_type'     => 'video',
			'post_status'   => 'publish',
			'posts_per_page'=> '4'
		];

		if( $args['mode'] === 'auto' ) {
			if( ! empty( $args['category'] ) ) { // If there is a category link to it
				$args['headline'] = get_term_by( 'id', $args['category'], 'video_category' )->name;
				$args['link'] = [
					'title'	=> __( 'View More', 'am2' ),
					'url'	=> get_term_link( $args['category'], 'video_category' )
				];
				$query['tax_query'] = [ [
					'taxonomy' => 'video_category',
					'field'    => 'id',
					'terms'    => [ $args['category'] ]
				] ];
			} else { // Link to default post archive
				$args['headline'] = __( 'Latest Videos', 'am2' );
				$args['link'] = [
					'title'	=> __( 'View More', 'am2' ),
					'url'	=> get_post_type_archive_link( 'video' )
				];
			}
		} elseif( ! empty( $args['videos'] ) ) {
			$query['post__in'] = $args['videos'];
		}

		$query = new \WP_Query( $query );

		if( $query->have_posts() ) :
			unset( $args['videos'] );
			while ( $query->have_posts() ) :
				$query->the_post();
				$args['videos'][] = Get::video( get_post() );
				wp_reset_postdata();
			endwhile;
		endif;

		wp_reset_query();

		return $args;
	}

	/**
	 * Video Categories Model
	 */
	public function flexible_video_categories_model( $args ) {
		$_cat_ids = ! empty( $args['categories'] ) ? $args['categories'] : get_terms( [
			'taxonomy'      => 'video_category',
			'hide_empty'    => false,
			'fields'		=> 'ids',
			'parent'        => 0
		] );

		unset( $args['categories'] );

		foreach( $_cat_ids as $id ) {
			$term_obj = get_term_by( 'id', $id, 'video_category' );
			$image = get_field( 'featured_image', 'video_category_' . $id );
			$image = $image ? $image['sizes']['video_category_block'] : AM2_TEMPPATH . '/assets/images/no-image.png';
			$args['categories'][] = [
				'name' 	=> $term_obj->name,
				'color'	=> get_field( 'color', 'video_category_' . $id ),
				'image'	=> $image,
                'permalink' => get_term_link( $term_obj, 'video_category' ),
                'description' => term_description($term_obj->term_id),
            ];

        }

		return $args;
	}

	/**
	 * Entry Hero
	 */
	/* public function flexible_entry_hero_model( $args ) {
		// Append BG
		$args['headline']	= ( $args['headline'] ) ? $args['headline'] : get_the_title();
		$args['background'] = ( has_post_thumbnail() ) ? get_the_post_thumbnail_url() : false;

		return $args;
	} */

	/**
	 * Files
	 */
	public function flexible_files_model( $args ) {
		// Append Button Args
		$args['button']	= [
			'label' => __( 'Download', 'am2' )
		];

		if( $args['files'] ) :
		$file_data_array = array();
		foreach ( $args['files'] as $file ) {
			$file_data                = array();
			$file_data['mime_type']   = $file['file']['mime_type'];
			$file_data['type']        = '';
			$file_data['name']        = $file['name'];
			$file_data['url']         = $file['file']['url'];
			$file_data['description'] = $file['file_description'];
			$file_data['size']        = filesize( get_attached_file( $file['file']['id'] ) );
			$file_data['size']        = size_format( $file_data['size'], 2 );
			if ( empty( $file_data['url'] ) ) {
				$file_data['url'] = 'javascript:void()';
			}

			switch ( $file_data['mime_type'] ) {
				case 'application/pdf':
					$file_data['type'] = 'pdf';
					break;
				case 'application/vnd.ms-powerpoint':
				case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
					$file_data['type'] = 'ppt';
					break;
				case 'application/rar':
					$file_data['type'] = 'rar';
					break;
				case 'application/zip':
					$file_data['type'] = 'zip';
					break;
				case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
					$file_data['type'] = 'docx';
					break;
				case 'application/msword':
					$file_data['type'] = 'doc';
					break;
				case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
				case 'application/vnd.ms-excel':
					$file_data['type'] = 'xls';
					break;
				default:
					$file_data['type'] = 'NA';
					$file_ext          = explode( '.', $file_data['url'] );
					$file_data['ext']  = $file_ext[ count( $file_ext ) - 1 ];
					break;
			}
			$file_data_array[] = $file_data;
		}
		endif;

		// Replace with new array
		$args['files'] = $file_data_array;
		return $args;
	}

	/**
	 * Custom Article Showcase
	 */
	public function flexible_articles_model( $args ) {
		if ( empty( $args['articles'] ) ) return;

		$articles = $args['articles'];
		$options  = $args['options'];

		// Unset the default key;
		unset( $args['articles'] );
        foreach( $articles as $key => $article ) {
            $type     = $article['type'];
			$context = [];
			$context['type']  = $type;
			if( 'entry' === $type ) {
				$article = $article['entry'];
				$context['title'] = $article->post_title;
				$context['image'] = has_post_thumbnail( $article->ID ) ? get_the_post_thumbnail( $article->ID, 'large', [
					'class' => 'entry-item__image'
				] ) : false;
				$context['date']  = [
					'gmt'   => get_the_date( DATE_W3C, $article->ID ),
					'human' => get_the_date( get_option( 'date_format'), $article->ID )
				];
				$context['excerpt'] = wp_trim_words( $article->post_content, 55, '...' );
				$context['buttons'] = [
					[
						'button' => [ // Since we use a repeater for link acf field for custom
							'title' => $options['buttons']['label'],
							'url'	=> get_permalink( $article->ID ),
							'target'=> $options['buttons']['target']
						]
					]
				];
			} else {
				$article = $article['custom'];
				$has_img = $article['image'];
				$context['title'] = $article['title'];
				$context['image'] = $has_img ? wp_get_attachment_image( $has_img['ID'], 'large', false, [
					'class' => 'entry-item__image'
				] ) : false;
				$context['date']  = false;
				$context['excerpt'] = $article['description'];
				$context['buttons'] = $article['buttons'];
			}
			// Append the article
            $args['articles'][] = $context;
		}

		return $args;
	}
}