<?php namespace AM2\Theme\Flexible;
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit();
// Flexible Layout
class Layout {
	/**
	 * Props
	 */
	private $field;
	private $block_position = 0;

	/**
	 * Constructor
	 */
	function __construct( $field ) {
		$this->field = $field; 
	}

	/**
	 * Render Method
	 */
	public function render( $output = '' ) {  

		$blocks = $this->field;

		// Bail if empty
		if ( empty( $blocks ) ) return null;

		// Get Rendered View
		foreach ( $blocks as $block ) $output .= $this->compile_view( $block );

		// Return
		return $output;
	}

	/**
	 * Render a Flexible Twig View - method
	 */
	public function compile_view( $block, $args = array() ) {
		// Bail early
		if ( ! $block ) return;

		// Block Counter
		$this->block_position++;

		$defaults = array(
			'block_position' => $this->block_position
		);
		
		// Arguments
		$args = apply_filters( 'am2/filter/flexible/' . $block['acf_fc_layout'] . '/model', wp_parse_args( $block, $defaults ) );

		// View Name and Path
		$viewname = implode( '-', explode( '_', strtolower( $args['acf_fc_layout'] . '.twig' ) ) ); 
		$viewname = apply_filters( 'am2/filter/flexible/' . $args['acf_fc_layout'] . '/view', $viewname, $args );
		$viewpath = AM2_TEMPDIR . '/views/' . $viewname;

		if ( file_exists( wp_normalize_path( $viewpath ) ) ) {
			$output = '';
			$content = '';
			// Dinamic Hook + Specific Block Position
			$output .= apply_filters( 'am2/filter/flexible/before/' . $args['acf_fc_layout'], $content );
			$output .= apply_filters( 'am2/filter/flexible/before/' . $args['acf_fc_layout'] . '/' . $args['block_position'], $content );
			$output .= \Timber::Compile( $viewname, $args );
			// Dinamic Hook + Specific Block Position
			$output .= apply_filters( 'am2/filter/flexible/after/' . $args['acf_fc_layout'] . '/' . $args['block_position'], $content );
			$output .= apply_filters( 'am2/filter/flexible/after/' . $args['acf_fc_layout'], $content );
			return $output;
		} else {
			$html = '<div class="notification notification--error notification--layout text text--center">';
			$html .= sprintf( '<div class="notification__title">%s</div>', 
				sprintf( __( 'The twig template file for this block ("%s") is missing!', 'am2' ), $viewname ) 
			);
			$html .= sprintf( '<p>%s<br />%s</p>', 
				__( 'Please make sure you created a TWIG template with the same above.', 'am2' ),
				sprintf( 
					__( 'Or use <code>%s</code> filter to change default template name.', 'am2' ), 
					'am2/filter/flexible/' . $args['acf_fc_layout'] . '/view' 
				)
			);
			$html .= '</div>';
			return $html;
		}
	}
}