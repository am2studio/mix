<?php namespace AM2\Theme;
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit();
// Menus
class Menus {
	/**
	 * Instance
	 *
	 * @access 	private
	 * @var 	object
	 */
	private static $instance;

	/**
	 * Initiator
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	/**
	 * Class Constructor.
	 */
	public function __construct() {
		// Register Menu Locations and Options Pages
		add_action( 'after_setup_theme', array( $this, 'register_menus' ) );
		add_action( 'after_setup_theme', array( $this, 'register_acf_menus' ) );
	}

	/*
	* Option Pages
	*/
	public function register_acf_menus() {
		if ( function_exists( 'acf_add_options_page' ) ) {
			acf_add_options_page();
			acf_add_options_sub_page( 'Header' );
			acf_add_options_sub_page( 'Tracking Codes' );
			acf_add_options_sub_page( 'Website Settings' );
			acf_add_options_sub_page( 'Social Options' );
			acf_add_options_sub_page( 'Subscribe' );
			acf_add_options_sub_page( 'Footer' );
			acf_add_options_sub_page( '404' );
		}
	}

	/*
	* Menu Locations
	*/
	public function register_menus() {
		register_nav_menus( array(
				'main_menu'   	=> __( 'Main Menu', 	'am2' ),
				'mobile_menu' 	=> __( 'Mobile Menu', 	'am2' ),
				'footer_menu' 	=> __( 'Footer Menu', 	'am2' ),
				'footer_landing' 	=> __( 'Footer Landing Menu', 	'am2' ),
			)
		);
	}
}