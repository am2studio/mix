<?php namespace AM2\Theme;
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit();
// Assets
class Assets {
	/**
	 * Instance
	 *
	 * @access 	private
	 * @var 	object
	 */
	private static $instance;

	/**
	 * Initiator
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	/**
	 * Class Constructor.
	 */
	public function __construct() {
		global $env;
		$this->is_dev 		= ( $env === 'development' ) ? true : false;
		$this->assets_dir 	= ( $this->is_dev ) ? 'dev' : 'build';

		// Admin
		if ( is_admin() )  {
			add_action( 'admin_head', 			array( $this, 'svg_thumb_display' 	) );
		} else {
			add_action( 'init', 				array( $this, 'remove_head_links'	) );
			add_action( 'wp_enqueue_scripts', 	array( $this, 'enqueue_assets'		) );
			add_action( 'wp_enqueue_scripts', 	array( $this, 'inline_scripts' 		) );
			add_action( 'wp_enqueue_scripts', 	array( $this, 'localize_scripts'	) );

			/**
			 * To use this feature we need to have critical CSS generated to avoid flash of unstyled content
			 * This will improve page speed on modern browsers while on older, the polyfill will do the same
			 */
			//add_filter(	'style_loader_tag',  	array( $this, 'css_preload' ), 999, 3 );
			//add_action(	'wp_head', 				array( $this, 'css_preload_poly' ),101);

			//add_action( 'wp_enqueue_scripts', 	array( $this, 'google_fonts'		) );
			/**
			 * If using default version of jQuery this will move it to footer #1 pos
			 * But since we may have inline scripts, we enqued new version of jQuery in head bellow
			 * However, when using AutoOptimize with Agregate Option on, you can have all JS in footer
			 * Works only with Default WP Jquery and only if jQuery is dependant in Production env (by default is not)
			 */
			//add_action( 'wp_default_scripts', 	array( $this, 'jquery_to_footer' 	) );
		}
	}

	/**
	 * FontEnd Assets
	 */
	public function enqueue_assets() {
		$bundle_deps = [ 'jquery', 'js-cookie' ];

		// Use different jQuery version
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', 			\AM2_TEMPPATH . '/resources/js/vendor/jquery.min.js', null, null, false );
		wp_enqueue_script( 'jquery' );

		// Other Scripts Bellow this line
		wp_register_script( 'svg4everybody',	\AM2_TEMPPATH . '/resources/js/vendor/svg4everybody.min.js', null, null, true );
		wp_enqueue_script( 'svg4everybody' );

		wp_register_script( 'js-cookie',		\AM2_TEMPPATH . '/resources/js/vendor/js-cookie.min.js', null, null, true );
		wp_enqueue_script( 'js-cookie' );

		if( is_single() ) {
            wp_register_script( 'js-lightbox',	\AM2_TEMPPATH . '/resources/js/vendor/lightbox.min.js', null, null, true );
            wp_enqueue_script( 'js-lightbox' );
		}

		wp_register_script( 'js-moment',        \AM2_TEMPPATH . '/resources/js/vendor/moment.js', null, null, true );
		wp_enqueue_script( 'js-moment' );

		wp_register_script( 'js-lightpick',		\AM2_TEMPPATH . '/resources/js/vendor/lightpick.js', null, null, true );
		wp_enqueue_script( 'js-lightpick' );

		wp_enqueue_script( 'babel-polyfill', '//cdnjs.cloudflare.com/ajax/libs/babel-polyfill/7.2.5/polyfill.min.js', null, null, true  );
		/* wp_script_add_data( 'babel-polyfill', 'conditional', 'IE' ); */

		//wp_register_script( 'slick',		\AM2_TEMPPATH . '/resources/js/vendor/slick.min.js', null, null, true );
		//wp_enqueue_script( 'slick' );

		foreach( [ 'css', 'js' ] as $dir ) {

			$buildDir = new \DirectoryIterator( STYLESHEETPATH . '/assets/' . $this->assets_dir . '/' . $dir );

			foreach( $buildDir as $file ) {

				$fullName = basename( $file );
				$name = substr( basename( $fullName ), 0, strpos( basename( $fullName ), '.' ) );

				if ( pathinfo( $file, PATHINFO_EXTENSION ) === 'css' ) {
					wp_register_style( $name, $this->get_asset_uri( $file, 'css' ) , [], null );
					wp_enqueue_style( $name );
				}

				if ( pathinfo( $file, PATHINFO_EXTENSION ) === 'js' ) {
					wp_register_script( $name, $this->get_asset_uri( $file, 'js' ), $bundle_deps, null, true );
					wp_enqueue_script( $name );
				}
			}
		}
	}

	/**
     * Get Asset File URL
     *
     * @param   string  $file
     * @param   string  $directory
     *
     */
	public function get_asset_uri( $file, $directory ) {
		return esc_url( get_theme_file_uri( '/assets/' . $this->assets_dir . '/' . $directory . '/' . basename( $file ) ) );
	}

	/**
	 * Preload CSS on production
	 */
	public function css_preload( $html, $handle, $href ) {
		if( $this->is_dev ) return $html;

		$dom = new \DOMDocument();
		$dom->loadHTML( $html );
		$tag = $dom->getElementById( $handle . '-css' );
		$tag->setAttribute( 'rel', 'preload' );
		$tag->setAttribute( 'as', 'style' );
		$tag->setAttribute( 'onload', "this.onload=null;this.rel='stylesheet'");
		$tag->removeAttribute( 'type' );
		$html = $dom->saveHTML( $tag );

		return $html;
	}

	/**
	 * Preload CSS on production polyfill for older browsers
	 */
	public function css_preload_poly() {
		if( $this->is_dev ) return;
		$script = '';
		$script .= '!function(n){"use strict";n.loadCSS||(n.loadCSS=function(){});';
		$script .= 'var o=loadCSS.relpreload={};if(o.support=function(){';
		$script .= 'var e;try{e=n.document.createElement("link").relList.supports("preload")}catch(t){e=!1}';
		$script .= 'return function(){return e}}(),o.bindMediaToggle=function(t){var e=t.media||"all";';
		$script .= 'function a(){t.addEventListener?t.removeEventListener("load",a):t.attachEvent&&t.detachEvent("onload",a),';
		$script .= 't.setAttribute("onload",null),t.media=e}t.addEventListener?t.addEventListener("load",a):t.attachEvent&&t.attachEvent("onload",a),';
		$script .= 'setTimeout(function(){t.rel="stylesheet",t.media="only x"}),setTimeout(a,3e3)},o.poly=function(){';
		$script .= 'if(!o.support())for(var t=n.document.getElementsByTagName("link"),e=0;e<t.length;e++){';
		$script .= 'var a=t[e];"preload"!==a.rel||"style"!==a.getAttribute("as")||a.getAttribute("data-loadcss")||(a.setAttribute("data-loadcss",!0),';
		$script .= 'o.bindMediaToggle(a))}},!o.support()){o.poly();var t=n.setInterval(o.poly,500);';
		$script .= 'n.addEventListener?n.addEventListener("load",function(){o.poly(),';
		$script .= 'n.clearInterval(t)}):n.attachEvent&&n.attachEvent("onload",function(){';
		$script .= 'o.poly(),n.clearInterval(t)})}"undefined"!=typeof exports?exports.loadCSS=loadCSS:n.loadCSS=loadCSS}("undefined"!=typeof global?global:this);';

		printf( '<script>%s</script>', $script );
	}

	/**
	 * Add am2 JS object
	 */
	public function localize_scripts() {
		global $wp_scripts;

		$am2_obj = array(
			'core' 				=> array(),
			'functionsQueue' 	=> array(),
			'assetsEnqueue' 	=> $wp_scripts->queue,
			'templateDirectory' => \AM2_TEMPPATH,
			'spritePath' 		=> \AM2_SPRITEPATH,
			'REST'            	=> esc_url_raw( rest_url() )
		);

		wp_localize_script( 'am2-bundle', 'am2', $am2_obj );

	}

	/**
	 * Add Inline JS
	 * @since	v3
	 * @version v3
	 */
	public function inline_scripts() {
		global $wp_scripts;
		$svg4everybody = ( in_array( 'svg4everybody', $wp_scripts->queue ) )
        ? array_search( 'svg4everybody', $wp_scripts->queue )
        : array_search( 'am2-bundle', $wp_scripts->queue );

		wp_add_inline_script( $wp_scripts->queue[$svg4everybody], 'svg4everybody();' );
	}

	/**
	 * Add Google Font
	 * @since	v3
	 * @version v3
	 * @return	fonts_url
	 */
	public function google_fonts() {
		// Since Autooptimize has option to load this async let's use it until we receive the files
		$google_fonts = apply_filters( 'am2/filter/google-fonts/', array(
			'roboto' => 'Roboto:300,400,500',
		) );

		$query_args = array(
			'family' => implode( '|', $google_fonts ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = esc_url( add_query_arg( $query_args, 'https://fonts.googleapis.com/css' ) );

		wp_enqueue_style( 'am2-google-fonts', $fonts_url, array(), null );

	}

	/**
	 * jQuery to Footer
	 * @since	v3
	 * @version v3
	 */
	public function jquery_to_footer( $wp_scripts ) {
		$wp_scripts->add_data( 'jquery', 			'group', 1 );
		$wp_scripts->add_data( 'jquery-core', 		'group', 1 );
		$wp_scripts->add_data( 'jquery-migrate', 	'group', 1 );
	}

	/**
	 * Display svg images on media uploader and feature images
	 */
	public function svg_thumb_display( $style = '' ) {
		$style .= '<style>';
		$style .= 'td.media-icon img[src$=".svg"],img[src$=".svg"].attachment-post-thumbnail{width:100%!important;height:auto!important;}';
		$style .= '</style>';
		echo $style;
	}

	/**
	 * Clean WP_Head of unwanted of stuff
	 */
	public function remove_head_links() {
		remove_action( 'wp_head', 'rsd_link' );
		remove_action( 'wp_head', 'wlwmanifest_link' );
		remove_action( 'wp_head', 'wp_generator' );
		remove_action( 'wp_head', 'feed_links', 2 );
		remove_action( 'wp_head', 'feed_links_extra', 3 );
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
		remove_action( 'wp_head', 'wp_shortlink_wp_head' );
		remove_action( 'wp_head', 'rest_output_link_wp_head' );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
	}
}