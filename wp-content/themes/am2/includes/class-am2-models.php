<?php namespace AM2\Theme; 
// Use
use AM2\Theme\Video as Video;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit(); 

class Models {
	/**
	 * Instance
	 *
	 * @var $_instance
	 */
	private static $_instance = NULL;

	/**
	 * Initiator
	 *
	 * @since 	v3.3
	 * @return 	object
	 */
	public static function get_instance() {
		if( self::$_instance == NULL ) self::$_instance = new self;
		return self::$_instance;
	}

	/**
	 * Constructor
	 */
	public function __construct() {}

	/**
	 * Helper function to encode an array to an excaped json string
	 * Useful to use it for placing encoded object in html attrs
	 * Eg: passing slick slider attributes inline instead of writting another script tag
	 * @param 	object	$post
	 * @return 	array
	 */
	public static function video( $post = null ) {
		if( ! $post ) global $post; 

		$video = get_field( 'youtube_url', $post->ID );
		parse_str( parse_url( $video, PHP_URL_QUERY ), $videoVars );  

		$yt_thumbnail = 'https://img.youtube.com/vi/'. $videoVars['v'] . '/hqdefault.jpg';
		$image = has_post_thumbnail( $post->ID ) ? get_the_post_thumbnail_url( $post->ID, 'large' ) : $yt_thumbnail;

		// Get First Category
		$terms = wp_get_post_terms( $post->ID, 'video_category', [ 'parent' => 0 ] );
		$category 	= count( $terms ) > 0 ? reset( $terms ) : null; 
		$cat_color 	= get_field( 'color', 'video_category_' . $category->term_id );
		$cat_link 	= get_term_link( $category, 'video_category' );

		$video_duration 	= get_post_meta( $post->ID, '_YT_duration', 	true );
		$video_publishAt 	= get_post_meta( $post->ID, '_YT_publishedAt',	true ); 

		return [
			'id'      		=> $videoVars['v'],
			'image'   		=> $image,
			'title'         => get_the_title( $post->ID ),
			'permalink'		=> get_the_permalink( $post->ID ),
			'description'   => $post->post_excerpt ? $post->post_excerpt : wp_trim_words( $post->post_content, 65, '...' ),
			'category'		=> [
				'name'	=> ! is_wp_error( $category ) ? $category->name : '',
				'color'	=> $cat_color ? $cat_color : '#202020',
				'link'	=> ! is_wp_error( $cat_link ) ? $cat_link : ''
			],
			'meta'	=> [
				'youtube_url'	=> $video,
				'published_at'	=> $video_publishAt !== '' ? $video_publishAt : get_the_date( DATE_W3C, $post->ID ),
				'view_count'	=> get_post_meta( $post->ID, '_YT_viewCount', true ),
				'duration'		=> [
					'human'		=> Video::duration_to_time( $video_duration ),
					'robot'		=> $video_duration 
				]
			] 
		]; 
	} 

	/**
	 * Helper function to encode an array to an excaped json string
	 * Useful to use it for placing encoded object in html attrs
	 * Eg: passing slick slider attributes inline instead of writting another script tag
	 * @param 	object	$post
	 * @return 	array
	 */
	public static function post( $post = null ) {
		if( ! $post ) global $post; 

		$id 		= $post->ID;

		$categories = get_the_category( $id ); 
		$category 	= ( count( $categories ) > 0 ) ? reset( $categories ) : [];
		$categoryId = ( $category != [] ) ? $category->cat_ID : 0;
		if( $categoryId ) {
			$category->permalink = get_category_link( $categoryId );
			$arr = get_option( 'category_' . $categoryId );
			$category->color = $arr['color'];
		} 

		$thumb	= get_the_post_thumbnail( $id, 'article_thumb' ); 
		if( ! $thumb ) $thumb = '<img class="card__image" src="' . AM2_TEMPPATH . '/assets/images/no-image-article-thumb.png">';

		$content = $post->post_excerpt ? $post->post_excerpt : $post->post_content;

		return [
			'title'         => get_the_title( $id ),
			'content'       => wp_trim_words( $content, 20, '...' ),
			'permalink'     => get_the_permalink( $id ),
			'category'		=> $category,
			'thumb'         => $thumb,
			'hasVideo'      => get_field( 'has_video', $id ),
			'hasGallery'    => get_field( 'has_gallery', $id ),
			'date'          => get_the_date( 'l, M d - g A', $id ),
			'className'     => 'card--' . $id,
		];  
	} 
}