<?php namespace AM2\Theme\Rest;
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit();
// Rest Fields
Models::get_instance();
class Models {
	/**
	 * Instance
	 *
	 * @access 	private
	 * @var 	object
	 */
	private static $instance;

	/**
	 * Initiator
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self;
		}
		return self::$instance;
	}
	
	/**
	 * Constructor
	 * {
	 * Example class of the WP way on how to expand default API routers of post or CPTs
	 * Any other functions that hooks into rest should be managed here
	 * or a subfile 'class-am2-rest-models-subfile.php' as a naming convention ( for further implementation of autoloader )
	 * }
	 */
	function __construct() {
		// Rest API data 
		add_action( 'rest_api_init', array( $this, 'expand_api_routes' ) ); 
	} 

	/**
	 * Images API Response 
	 */
	public function get_image_src( $object, $field_name, $request ) { 
		$image 	= [
			// This could be an array since you may want to pass maybe an url/another size etc
			'html' => wp_get_attachment_image( $object['featured_media'], 'am2_news_thumb' )
		];
		return $image;
	}

	/**
	 * Date API Response
	 * @return 	string 	$date	formated date object based on default option set in WP admin ( Settings )
	 */
	public function get_post_date( $object, $field_name, $request ) {
		$date = get_the_date( get_option( 'date_format' ), $object['id'] );
		return $date;
	} 

	/**
	 * Expand API Route
	 */
	public function expand_api_routes() {
		register_rest_field(
			'post', 'featured_image', // post type to append to, key name in the object response
			array(
				'get_callback'    => [ __CLASS__, 'get_image_src' ], // Method Above
				'update_callback' => null,
				'schema'          => null,
			)
		);

		register_rest_field(
			'post', 'date_human', // post type to append to, key name in the object response
			array(
				'get_callback'    => [ __CLASS__, 'get_post_date' ], // Method Above
				'update_callback' => null,
				'schema'          => null,
			)
		);  
	}
}