<?php namespace AM2\Theme;
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;
use AM2\Theme\Functions as Helpers;
/**
 * Small helper class that renders breadcrumbs from YoastSEO
 * Added since I saw a lot of custom solutions instead of using Yoast
 */
class WordPressSeo {
	/**
	 * Instance
	 *
	 * @access 	private
	 * @var 	object
	 */
	private static $instance;

	/**
	 * Initiator
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) self::$instance = new self;
		return self::$instance;
	}

	public function __construct() {
		if( apply_filters( 'am2/filter/support/breadcrumbs/wc_yoast', true )
		&& Helpers::detect_plugin( [ 'classes' => [ 'woocommerce' ] ] ) 
		&& Helpers::detect_plugin( [ 'functions' => [ 'yoast_breadcrumb' ] ] ) ) {
			remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
			add_action( 'woocommerce_before_main_content', [ $this, 'renderBreadcrumbs' ], 20, 0 );
		}
		add_theme_support( 'title-tag' );	 
	}

	/**
	 * Yoast BreadCrumbs
	 *
	 * @return  void
	 */
	public function renderBreadcrumbs() {
		if( ! function_exists( 'yoast_breadcrumb' ) || is_front_page() ) return;
		 
		$args = [
			'before' => '<div class="section section--breadcrumbs breadcrumbs"><div class="wrapper"><div class="container">',
			'after' => '</div></div></div>'
		];
		yoast_breadcrumb( $args['before'], $args['after'] );
	}
}