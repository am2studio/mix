<?php namespace AM2\Theme\Hooks;
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit();

// Use
use AM2\Theme\Functions as Helpers;

// Hooks
class Queries {
	/**
	 * Instance
	 *
	 * @access 	private
	 * @var 	object
	 */
	private static $instance;

	/**
	 * Initiator
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	/**
	 * Class Constructor.
	 * Contains action/filter hooks regarding altering the queries
	 */
	public function __construct() {
        // Alters the search archive
		add_action( 'pre_get_posts', [ $this, 'pre_get_video' ] );
		add_action( 'pre_get_posts', [ $this, 'pre_get_author' ] );
		add_action( 'pre_get_posts', [ $this, 'pre_get_search' ] );
		add_action( 'pre_get_posts', [ $this, 'pre_get_taxonomy_video' ] );
		//add_filter( 'posts_orderby', [ $this, 'order_search_by_posttype' ], 10, 2 );
	}

    /**
	 * Alters the video tax
     * @param  object   $query { WP_QUERY object }
     * @return object
	 */
	public function pre_get_taxonomy_video( $query ) {
        if( is_admin() ) return; // Not on admin

        // Just for main and taxonomy (if rest api change is required, main condition should be removed )
		if( $query->is_main_query() && $query->is_tax( 'video_category' ) ) {
			$query->set( 'posts_per_page', '16' ); // This is the taxonomy archive
        }

        return $query;
	}

	/**
	 * Alters the video Latest
     * @param  object   $query { WP_QUERY object }
     * @return object
	 */
	public function pre_get_video( $query ) {
        if( is_admin() ) return; // Not on admin

		// Just for main video query
		if( $query->is_main_query() && in_array( $query->get( 'post_type' ), [ 'video' ] ) ) {
			$query->set( 'posts_per_page', '4' );
			// Leave it to 4, this is archive, 4 latest posts + 4 from each parent taxonomy
        }

        return $query;
	}

	/**
	 * Alters the video author archive
     * @param  object   $query { WP_QUERY object }
     * @return object
	 */
	public function pre_get_author( $query ) {
        if( is_admin() ) return; // Not on admin

		// Just for main video query
		if( $query->is_main_query() && $query->is_author() && isset( $_GET['type'] ) && $_GET['type'] === 'video' ) {
			$query->set( 'post_type', 'video' );
        }

        return $query;
	}

	/**
	 * Alters the video pre_get_search archive
     * @param  object   $query { WP_QUERY object }
     * @return object
	 */
	public function pre_get_search( $query ) {
        if( is_admin() ) return; // Not on admin

		// Just for main search query
		if( $query->is_main_query() && $query->is_search() ) {
			if( isset( $_GET['type'] ) ) {
				if( $_GET['type'] === 'video' ) {
					$query->set( 'post_type', 'video' );
				}
				if( $_GET['type'] === 'post' ) {
					$query->set( 'post_type', 'post' );
				}
			} else {
				$query->set( 'post_type', 'post' );
			}
			$query->set( 'posts_per_page', '9' ); // home bigger number to be able to split queries
        }

        return $query;
	}

	/**
	 * Posts Search Order By
	 */
	function order_search_by_posttype( $orderby, $wp_query ) {
		if( ! $wp_query->is_admin && $wp_query->is_search() ) :
			global $wpdb;
			$orderby =
				"
				CASE WHEN {$wpdb->prefix}posts.post_type = 'video' THEN '1'
					 WHEN {$wpdb->prefix}posts.post_type = 'post' THEN '2'
				ELSE {$wpdb->prefix}posts.post_type END ASC,
				{$wpdb->prefix}posts.post_title ASC";
		endif;
		return $orderby;
	}
}