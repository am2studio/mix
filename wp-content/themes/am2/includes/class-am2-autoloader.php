<?php namespace AM2\Theme;
/**
 * @package 	AM2 Theme.
 * @subpackage  Autoloader
 * @since		v3
 */

/**
 * Autoloader class.
 *
 * @since 3
 */
class Autoloader {

	/**
	 * Vars.
	 *
	 * @access 	private
	 * @since 	3
	 * @var 	array 	$cached_paths	Cached FilePaths
	 * @var 	string	$_namespace		Theme Namespace
	 * @var 	string	$_separator		Namespace Separator
	 */
	private $cached_paths 	= array();
	private $_namespace		= 'AM2\Theme'; 	// Namespace
	private $_separator 	= '\\'; 		// Namespace separator

	/**
	 * Class constructor.
	 *
	 * @access 	public
	 * @since 	3
	 */
	public function __construct() {

		spl_autoload_register( array( $this, 'autoload' ) );
		
	}

	/**
	 * The WeCodeArt class autoloader.
	 * Finds the path to a class that we're requiring and includes the file.
	 *
	 * @access 	protected
	 * @since 	3
	 * @param 	string 	$class_name 	The name of the class we're trying to load.
	 */
	protected function autoload( $class_name ) {

		// Not a WeCodeArt file, early exit.
		if ( 0 !== stripos( $class_name, $this->_namespace ) ) {
			return;
		}

		// Check if we've got it cached and ready.
		if ( isset( $this->cached_paths[ $class_name ] ) && file_exists( $this->cached_paths[ $class_name ] ) ) {
			include_once $this->cached_paths[ $class_name ];
			return;
		}

		// If no chach, run get_paths, set cache and include.
		$paths = $this->get_paths( $class_name );

		foreach ( $paths as $path ) {
			$path = wp_normalize_path( $path );
			if ( file_exists( $path ) ) {
				$this->cached_paths[ $class_name ] = $path;
				include_once $path;
				return;
			}
		}
	}

	/**
	 * Get an array of possible paths for the file.
	 *
	 * @access 	protected
	 * @since 	3
	 * @param 	string 	$class_name 	The name of the class we're trying to load.
	 * @return 	array
	 */
	protected function get_paths( $class_name ) {

		$paths = array();

		// Build the filename
		$filename = 'class-' . strtolower( str_replace( $this->_separator, '-', $class_name ) ) . '.php';
		$filename = str_replace( 'theme', '', $filename ); 	// Remove 'Theme' string from namespace
		$filename = str_replace( '--', '-', $filename );	// Replace double '--' caused by AM2\Theme double namespacing

		// Check same directory
		$paths[] = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $filename;

		// Look into sub-directory
		$substr   = str_replace( $this->_namespace . $this->_separator, '', $class_name );
		$exploded = explode( $this->_separator, $substr );
		$levels   = count( $exploded );

		// Build the filepath
		$previous_path = '';
		for ( $i = 0; $i < $levels; $i++ ) {
			$paths[]        = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $previous_path . strtolower( $exploded[ $i ] ) . DIRECTORY_SEPARATOR . $filename;
			$previous_path .= strtolower( $exploded[ $i ] ) . DIRECTORY_SEPARATOR;
		}

		// Return Paths
		return $paths;
		
	}
}
