<?php namespace AM2\Theme;
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit();
// Custom Post Types
class CPT {
	/**
	 * Instance
	 *
	 * @access 	private
	 * @var 	object
	 */
	private static $instance;

	/**
	 * Initiator
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	/**
	 * Class Constructor.
	 */
	public function __construct() {
		// Register CPT and Taxonomies
		add_action( 'init', array( $this, 'register_cpt' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_filter( 'wpseo_canonical', array($this, 'change_canonical_of_duplicates' ));
	}

	public function change_canonical_of_duplicates($url) {
	    global $post;
	    global $wpdb;
	    $current_site_id = get_current_blog_id();
	    if( is_singular() ) {

	        $sql = "SELECT * FROM {$wpdb->base_prefix}mpd_log 
                    WHERE destination_id = {$current_site_id} 
                    AND destination_post_id = {$post->ID}";

	        $results = $wpdb->get_row($sql);

	        if( !$results ) {
	            return $url;
            }

            switch_to_blog(1);

            $original_post = get_post($results->source_post_id);
            if( $original_post ) {
                $permalink = get_permalink($original_post);
            }
            switch_to_blog($current_site_id);
            if(!empty($permalink)) {
                return $permalink;
            }

            return $url;




        }
        return $url;
    }
	
	/** 
	 * Register CPT
	 */
	public function register_cpt() {
		$labels = array(
			'name'               => _x( 'Videos', 'post type general name', 'am2' ),
			'singular_name'      => _x( 'Video', 'post type singular name', 'am2' ),
			'add_new'            => _x( 'Add Video', 'Videos', 'am2' ),
			'add_new_item'       => __( 'Add Video', 'am2' ),
			'edit_item'          => __( 'Edit Video', 'am2' ),
			'new_item'           => __( 'New Video', 'am2' ),
			'view_item'          => __( 'View Video', 'am2' ),
			'search_items'       => __( 'Search Videos', 'am2' ),
			'not_found'          => __( 'No Videos found', 'am2' ),
			'not_found_in_trash' => __( 'No Videos found in the trash', 'am2' ),
			'parent_item_colon'  => '',
			'show_in_nav_menus'  => true
		);
	
		$rewrite = array(
			'slug'                  => 'videos',
			'with_front'            => true,
			'pages'                 => true,
			'feeds'                 => true,
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'query_var'          => true,
			'rewrite'            => $rewrite,
			'capability_type'    => 'post',
			'hierarchical'       => true,
			'menu_position'      => null,
			'show_in_rest'       => true,
			'rest_base'          => 'videos',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
			'has_archive'        => true,
			'publicly_queryable' => true, 
			'supports'           => array( 'title', 'author', 'excerpt', 'thumbnail', 'editor' ) 
		); 
		
		register_post_type( 'video', $args ); 
	}

	/**
	 * Register Taxonomies
	 */
	public function register_taxonomies() {
		register_taxonomy(
			'video_category',
			'video',
			array(
				'label'        => __( 'Video Categories', 'am2' ),
				'labels'	   => [
					'name'              => _x( 'Video Categories', 'taxonomy general name', 'am2' ),
					'singular_name'     => _x( 'Video Category', 'taxonomy singular name', 'am2' ),
					'search_items'      => __( 'Search Videos', 'am2' ),
					'all_items'         => __( 'All Video Categories', 'am2' ),
					'parent_item'       => __( 'Parent Video Category', 'am2' ),
					'parent_item_colon' => __( 'Parent Video Category:', 'am2' ),
					'edit_item'         => __( 'Edit Video Category', 'am2' ),
					'update_item'       => __( 'Update Video Category', 'am2' ),
					'add_new_item'      => __( 'Add New Video Category', 'am2' ),
					'new_item_name'     => __( 'New Video Category Name', 'am2' ),
					'menu_name'         => __( 'Video Categories', 'am2' ),
				],
				'capabilities' => array(),
				'hierarchical' => true,
				'show_in_rest' => true,
				'rest_base'    => 'video_categories'
			)
		);
	}
}