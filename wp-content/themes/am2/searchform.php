<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<label class="search-form__label">
		<span class="screen-reader-text"><?php _e( 'Search for', 'am2' ); ?>:</span>
		<?php //<input type="hidden" name="search-type" value="regular"> ?>
		<input type="search" class="search-form__field" name="s"
			placeholder="<?php esc_attr_e( 'Search', 'am2' ); ?> …"
			value="<?php echo esc_attr( get_search_query() ); ?>"
			title="<?php esc_attr_e( 'Search for', 'am2' ); ?>:" />
	</label>
	<button type="submit" class="search-form__submit">
		<span class="screen-reader-text"><?php _e( 'Search', 'am2' ); ?>:</span> 
		<svg class="svg-icon svg-icon--header">
			<use xlink:href="<?php echo AM2_SPRITEPATH ?>#icon--search"></use>
		</svg>
	</button>
</form>
