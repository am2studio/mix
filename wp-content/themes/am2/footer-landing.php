			<footer id="footer" class="footer footer--site footer--landing">
				<div class="wrapper">
					<div class="container">
						<div class="col">
							<?php
								wp_nav_menu( array(
										'container'       => 'nav',
										'container_id'    => 'footer-landing-menu',
										'container_class' => 'menu menu--footer-landing',
										'menu_class'      => 'menu',
										'theme_location'  => 'footer_landing',
										'fallback_cb'	  => false,
										'walker' 		  => new AM2\Theme\Walkers\Menu()
									)
								);
							?>
							<div class="footer__copyright">
								<?php
									$template = 'The MIX &copy %s - %s All Rights Reserved';
									printf( $template, '2015', date('Y') );
								?>
							</div>
						</div>
					</div>
				</div>
			</footer>
			<!--/footer -->
		</div>
		<!-- /site-container -->

		<?php wp_footer(); ?>

		<?php
			$tracking_codes_footer = get_field( 'tracking_codes_footer', 'options' );
			if ( ! empty( $tracking_codes_footer ) ) {
				echo $tracking_codes_footer;
			}
		?> 
	</body>
</html>