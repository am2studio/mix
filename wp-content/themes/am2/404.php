<?php
	use AM2\Theme\Models as Models; 

	// Vars
	$context = [
		'page_title'		=> get_field('404_title', 'options'),
		'page_subtitle'		=> get_field('404_subtitle', 'options'),
		'page_text'			=> get_field('404_text', 'options'),

		'home_label' 		=> get_field('home_label', 'options'),
		'home_url'			=> get_site_url(),
		'contact_us_label' 	=> get_field('contact_us_label', 'options'),
		'contact_us_url' 	=> get_field('contact_us_url', 'options'),
	];	

	// Get Header
	get_header(); 
	
?>
<div id="content" class="content content--404 page-404">
	<main id="main" class="main">
		<div class="layout-narrow">
			<div class="wrapper">
				<div class="container text-center">
				<?php 

					Timber::render( '404.twig', $context );

				?>
				</div>
			</div>
		</div>
		<div class="section section--latest-videos latest-videos">
			<div class="wrapper">
				<div class="container">
                    <div class="section__header">
                        <h2 class="section__headline"><?php _e( 'Latest', 'am2' ); ?></h2>
                        <a href="<?php echo esc_url( get_post_type_archive_link( 'video' ) ); ?>" class="section__more"><?php
                            _e( 'View All', 'am2' );
                        ?></a>
                    </div>
                </div>
				<div class="container container--flex">
					<?php
						$latest = []; // Holds last 4 videos
						$latest = new WP_Query([
							'post_type'     => 'video',
							'post_status'   => 'publish',
							'posts_per_page'=> 4 
						]);

						if( $latest->have_posts() ) :

							while ( $latest->have_posts() ) :

								$latest->the_post();

								$class = [ 'video-category__item', 'col-14' ]; 
								$class = trim( implode( ' ', $class ) );

								$args = [];
                            	$args['post'] = Models::video( get_post() );

								echo '<div class="' . esc_attr( $class ) . '">';
								Timber::render( 'video-block.twig', $args );
								echo '</div>'; 

								wp_reset_postdata();

							endwhile; 

						else : 

							echo '<p class="text text--center no-posts">' . __( 'No videos found.', 'am2' ) . '</p>';

						endif; 

						wp_reset_query();

					?> 
				</div>
			</div>
		</div>
	</main>
	<!-- end:main -->
</div>
<!-- end:content -->
<?php 

	// Get Footer
	get_footer(); 
	
?>