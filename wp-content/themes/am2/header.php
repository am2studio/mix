<?php
	use AM2\Theme\Functions as Helpers;
?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes( 'html' ); ?>>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php
			// Favicon
			$favicon = get_field( 'branding', 'options' )['favicon'];
			if( ! empty( $favicon ) ) printf( '<link rel="icon" href="%s">', $favicon['url'] );
			// WP Head
			wp_head();
		?>
		<?php
			// Tracking Codes
			if ( get_field( 'tracking_codes_head', 'options' ) ) the_field( 'tracking_codes_head', 'options' );
		?>
		<style>.no-js .menu-container{display:none;}</style>
	</head>
	<body <?php body_class(); ?>>
		<?php
			if ( get_field( 'tracking_codes_body', 'options' ) ) the_field( 'tracking_codes_body', 'options' );
		?>
		<div class="site-container">
			<header id="header" class="header header--site">
				<div class="wrapper">
					<div class="container">
						<div class="col-13 menu-container-wrapper">
							<div class="resp-buttons clearfix">
								<div class="resp-buttons__menu">
									<div class="resp-buttons__menu-button">
										<div class="menu-toggle menu-toggle--xbutterfly js-menu-toggle" aria-label="Toggle Menu">
											<span class="menu-toggle__lines" aria-hidden="true"></span>
										</div>
									</div>
								</div>
							</div>
							<!-- /responsive buttons -->
							<div class="menu-container">
								<?php
									wp_nav_menu( array(
										'container'       => 'nav',
										'container_id'    => 'main-nav',
										'container_class' => 'main-navigation',
										'menu_class'      => 'menu menu--main-menu',
										'theme_location'  => 'main_menu',
										'fallback_cb'	  => false,
										'walker' 		  => new AM2\Theme\Walkers\Menu(),
										)
                                    );

                                    if( get_field('location_visibility','options') == true ) {
                                        // Global Site Link
                                        Helpers::render_global_site_link( 1, __( 'Visit MIX Global', 'am2' ) );

                                        // Location Switcher Select
                                        Helpers::render_location_switcher( 'menu', __( 'Switch location', 'am2' ) );
                                    }
                                    // Social Profiles
                                    echo Helpers::render_social_links( 'header' );
								?>
							</div>
							<!-- /main nav -->
						</div>
						<div class="col-13 logo-wrapper">
                            <?php
                                $site_logo = get_field( 'branding', 'options' )['site_logo'];
                                $site_logo_width = get_field( 'branding', 'options' )['logo_width'];
                            ?>
							<h1 class="logo">
								<a href="<?php echo site_url(); ?>" class="logo__link" <?php if($site_logo_width): ?> style="max-width:<?php echo $site_logo_width; ?>" <?php endif; ?>>
									<?php if( !empty( $site_logo ) ) { ?>
                                    <img src="<?php echo $site_logo['url']; ?>" alt="<?php bloginfo(); ?>" />
									<?php } else {
										bloginfo();
									} ?>
								</a>
							</h1>
						</div>
						<div class="col-13 search-form-wrapper">
							<?php get_search_form( true ); ?>
							<!-- /search -->
						</div>
					</div>
				</div>
			</header>
			<!--/header -->
