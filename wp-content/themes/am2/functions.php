<?php
/*
 * AM2 Constants
 */
global $env;
$env = constant( 'ENV' );

define( 'AM2_TEMPDIR', 		get_template_directory() 		);
define( 'AM2_TEMPPATH', 	get_template_directory_uri() 	);
$svgDir = new \DirectoryIterator( \AM2_TEMPDIR . '/assets/build/svg' ); // Target SVG Build Path
$svgName = '';
foreach( $svgDir as $file ) {
    if( pathinfo( $file, PATHINFO_EXTENSION ) === 'svg' ) $svgName = basename( $file );
    break;
}
if( $svgName ) define( 'AM2_SPRITEPATH', get_template_directory_uri() . '/assets/build/' . $svgName );
else define( 'AM2_SPRITEPATH', get_template_directory_uri() . '/assets/images/sprite.svg' );

/*
 * AM2 Autoloader
 */
require_once( AM2_TEMPDIR . '/includes/class-am2-autoloader.php' );
new AM2\Theme\Autoloader();

AM2\Theme\Hooks     ::get_instance();   // Core Filters & Actions
AM2\Theme\Menus     ::get_instance();   // Menu Pages and Menu Locations
AM2\Theme\CPT       ::get_instance();   // CPT & Taxonomies
AM2\Theme\Assets    ::get_instance();   // Assets Manager
AM2\Theme\Rest\Models       ::get_instance();
AM2\Theme\Flexible\Models   ::get_instance();
AM2\Theme\WordPressSeo      ::get_instance();

// am2-functions.php should be a class with static methods
// Other classes that are not called here are deprecated
// Eg: excerpt function, tinymce stuff, use gutenberg where possible