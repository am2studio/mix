<?php
	use AM2\Theme\Functions as Helpers;
?>
<footer id="footer" class="footer footer--site">
	<div class="wrapper">
		<div class="container">

			<div class="footer__top clearfix">

				<div class="footer__top-left">

					<!-- site logo -->
					<?php $site_logo = get_field( 'footer_logo', 'options' ); ?>
					<a href="<?php echo site_url(); ?>" class="footer__logo">
						<img src="<?php echo $site_logo; ?>" alt="" />
					</a>

					<!-- site menu -->
					<?php
						wp_nav_menu( array(
							'container'       => 'nav',
							'container_id'    => 'footer-nav',
							'container_class' => 'footer-navigation',
							'menu_class'      => 'menu menu--footer-menu',
							'theme_location'  => 'footer_menu',
							'fallback_cb'	  => 'am2_fallback_menu'
							)
						);
					?>

				</div>

				<div class="footer__top-right">

					<!-- site social links -->
					<?php echo Helpers::render_social_links( 'footer' ); ?>

				</div>

			</div>

			<div class="footer__divider"></div>

			<div class="footer__bottom clearfix">

				<!-- site copyright -->
				<div class="col-12 footer__copyright">
					<?php echo get_field( 'copyright','options'); ?>
				</div>

				<!-- site links -->
				<div class="col-12 no-margin footer__links">
					<?php $footer_links = get_field( 'footer_submenu', 'options' ); ?>
					<?php if( is_array( $footer_links ) ) foreach( $footer_links AS $footer_link): ?>
						<a href="<?php echo $footer_link['link_url'] ?>">
							<?php echo $footer_link['link_text'] ?>
						</a>
					<?php endforeach ?>
				</div>

			</div>

		</div>
	</div>
</footer>
<!--/footer -->

</div>
<!-- /site-container -->

<?php wp_footer(); ?>

<?php
	$tracking_codes_footer = get_field( 'tracking_codes_footer', 'options' );
	if ( ! empty( $tracking_codes_footer ) ) {
		echo $tracking_codes_footer;
	}
?>

</body>
</html>