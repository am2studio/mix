<?php
$shortcode =  get_field('subscribe_form_shortcode', 'options');

if($shortcode) {

    // Let's render form here via shortcode and pass it to view
    $form = do_shortcode($shortcode);

    $subscribe = [
        'title'     => get_field('subscribe_title', 'options'),
        'text'      => get_field('subscribe_text', 'options'),
        'image'     => get_field('subscribe_background_image','options'),
        'form'      => $form,
    ];

    Timber::render('subscribe.twig', $subscribe);

}