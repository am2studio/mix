<?php
	// Use
	use AM2\Theme\Functions;
	use AM2\Theme\Models;
	use AM2\Theme\Video;

	// Get Header
	get_header();
?>

<div id="content" class="content content--video-single">
	<main id="main" class="main main--video-single" itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
			// What we need ?
			global $post;
			$current_post = $post;

            $playlist = get_field('youtube_playlist_code',$current_post->ID);
            $video 	= get_field( 'youtube_url', $current_post->ID );
			parse_str( parse_url( $video, PHP_URL_QUERY ), $videoVars );

			$yt_max = 'https://img.youtube.com/vi/' . $videoVars['v'] . '/maxresdefault.jpg';
			$image = has_post_thumbnail( $current_post->ID ) ? get_the_post_thumbnail_url( $current_post->ID, 'video_large' ) : $yt_max;

			$uploadDate = get_post_meta( $current_post->ID, '_YT_publishedAt', true );
			$meta = [
				'duration' 		=> get_post_meta( $current_post->ID, '_YT_duration', true ),
				'thumbnailUrl'	=> $image,
				'embedURL'		=> $video,
				'uploadDate'	=> $uploadDate ? $uploadDate : get_the_date( DATE_W3C )
			];

			// Update meta
			Video::set_post_views( $current_post->ID );
			Video::update_youtube_views( $current_post->ID );
		?>
		<div class="section section--video-player video-player">
			<div class="wrapper">
				<div class="container container--flex">
					<div class="col-1">
						<div class="video-player__header">
							<h4 class="video-player__headline"><?php
								$_terms = wp_get_post_terms( $post->ID, 'video_category', [ 'parent' => 0 ] );
								$_cat 	= end( array_reverse( $_terms ) );
								if( $_cat ) echo esc_html( $_cat->name );
							?></h4>
							<div class="video-player__duration"><?php echo esc_html(
								Video::duration_to_time( $meta['duration'] )
							); ?></div>
						</div>
						<div class="video-player__media responsive-embed responsive-embed--single">
                            <?php if($playlist): ?>
                                <div class="video-player-wrap">
                                    <?php echo $playlist; ?>
                                </div>
                            <?php else: ?>
                                <?php
								$YT_iframe = Functions::to_json_string([
                                        'videoID' => $videoVars['v'],
                                        'triggerSelector' => false,
                                        'srcVars' => [
                                            'modestbranding' => '1',
                                            'autoplay' 	=> '0',
                                            // https://stackoverflow.com/questions/40685142/youtube-autoplay-not-working
                                            'mute'		=> '0', // Autoplay with sound is not permited by Chrome
                                            // From Youtube Docs
                                            'rel' 		=> '0', // As of sep 2018, 0 show related from channel (cannot be disabled)
                                            'controls' 	=> '1',
                                            'hd' 		=> '1'
                                        ]
                                    ]);
                                ?>
                                <div id="video-player-wrap" data-plugin-youtube data-options="<?php echo $YT_iframe; // @wpcs ok ?>">
                                    <figure class="video-player__placeholder"><?php
                                        if( has_post_thumbnail() ) :
                                        echo wp_get_attachment_image( get_post_thumbnail_id( $post->ID ), 'video_large', null, [
                                            'class' => 'video-player__placeholder-src',
                                            'alt'	=> get_the_title()
                                        ]);
                                        else : printf( '<img class="video-player__placeholder-src" src="%s" alt="%s" />',
                                            $image,
                                            get_the_title()
                                        );
                                        endif;
                                        ?>
                                    </figure>
                                    <div class="video-player__play am2-play">
                                        <svg class="am2-play__svg">
                                            <use xlink:href="<?php echo AM2_SPRITEPATH; ?>#icon--play-circle"></use>
                                        </svg>
                                    </div>
                                </div>
                                <meta itemprop="name" content="<?php echo esc_attr( get_the_title() ); ?>" />
                                <meta itemprop="description" content="<?php echo esc_attr( get_the_excerpt() ); ?>" />
                                <meta itemprop="duration" content="<?php echo esc_attr( $meta['duration'] ); ?>" />
                                <meta itemprop="thumbnailUrl" content="<?php echo esc_url( $meta['thumbnailUrl'] ); ?>" />
                                <meta itemprop="embedURL" content="<?php echo esc_url( $meta['embedURL'] ); ?>" />
                                <meta itemprop="uploadDate" content="<?php echo esc_attr( $meta['uploadDate'] ); ?>" />
                            <?php endif; ?>

						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="section section--video-content video-content">
			<div class="wrapper">
				<div class="container container--flex">
					<div class="col-23">
                        <div class="video-content__header">
							<div class="video-content__meta">
								<span class="video-content__meta-date"><?php the_date(); ?></span>
								<span class="video-content__meta-author">
									<span><?php _e( 'by', 'am2' ); ?></span>
									<?php
										$author_id 	= $current_post->post_author;
										$author_url	= add_query_arg( [ 'type' => 'video'], get_author_posts_url( $author_id ) );
										$author_name= get_the_author_meta( 'display_name', $author_id );
										printf( '<a href="%s" rel="author">%s</a>', esc_url( $author_url ), esc_html( $author_name ) );
									?>
								</span>

							</div>
							<h1 class="video-content__title"><?php the_title(); ?></h1>

						</div>
						<div class="video-content__entry">
							<?php
								$sticky = Functions::to_json_string([
									'minWidth'			=> '641',
									'containerSelector' => '.video-content .container--flex',
									'padding' 			=> [ 'top' => '110' ]
								]);
							?>
							<div class="video-content__sharing">
								<div class="am2-sharing" data-plugin-sticky data-options="<?php echo $sticky; //@wpcs ok ?>">
									<?php echo Functions::render_social_share( 'video', false, get_the_ID() ); ?>
								</div>
							</div>
							<div class="video-content__text" itemprop="description"><?php the_content(); ?></div>
						</div>
					</div>
					<div class="col-13 no-margin">
						<div class="video-player__header video-player__header--related">
							<h4 class="video-player__headline"><?php
								_e( 'Related Videos', 'am2' );
							?></h4>
							<?php if( $_cat ) : ?>
							<a href="<?php echo esc_url( get_term_link( $_cat ) ); ?>" class="video-player__header-link"><?php
								_e( 'View All', 'am2' );
							?></a>
							<?php endif; ?>
						</div>
						<?php
							$args = [
								'post_type'	 	=> 'video',
								'post_status'	=> 'publish',
								'posts_per_page'=> 4
							];

							if( $selected_posts = get_field( 'related_videos' ) ) { // Show Selected
								// Remove Current if is selected by mistake or other reason
								$selected_posts = array_diff( $selected_posts, [ $current_post->ID ] );
								$args['post__in'] = $selected_posts;
							} else { // Get Previous by date, from same tax, excluding current
								$args['date_query'] = [ [ 'before' => get_the_date( 'Y-m-d H:i:s' ) ] ];
								$args['post__not_in'] = [ $current_post->ID ];
								if( $_cat ) :
									$args['tax_query'] = [
										[
											'taxonomy' => 'video_category',
											'field'    => 'id',
											'terms'    => [ $_cat->term_id ]
										]
									];
								endif;
							}

							$prev_posts = new WP_Query( $args );

							if( $prev_posts->have_posts() ) :
						?>
						<div class="video-player__related"><?php
							while ( $prev_posts->have_posts() ) :

								$prev_posts->the_post(); // Set Post

								$args = Models::video( get_post() );
								if( ! has_post_thumbnail( get_post() ) )
									$args['image'] = 'https://img.youtube.com/vi/' . $args['id'] . '/hqdefault.jpg';

								Timber::render( 'video-block_side.twig', $args );

								wp_reset_postdata();	// Reset Post

							endwhile;
						?></div>
						<?php
							endif;
							wp_reset_query(); // Reset Query
						?>
					</div>
				</div>
			</div>
		</div>
		<?php endwhile; endif;
		$args = [
			'post_type'	 	=> 'video',
			'post_status'	=> 'publish',
			'posts_per_page'=> 4,
			'order'			=> 'rand'
		];

		if( $selected_posts = get_field( 'may_like_videos' ) ) { // Show Selected
			// Remove Current if is selected by mistake or other reason
			$selected_posts = array_diff( $selected_posts, [ $current_post->ID ] );
			$args['post__in'] = $selected_posts;
		} else { // Get 4 Videos, excepting current, from same tax
			$args['post__not_in'] = [ $current_post->ID ];
			if( $_cat ) :
				$args['tax_query'] = [
					[
						'taxonomy' => 'video_category',
						'field'    => 'id',
						'terms'    => [ $_cat->term_id ]
					]
				];
			endif;
		}

		$may_like = new WP_Query( $args );

		if( $may_like->have_posts() ) :
		?>
		<div class="section section--video-may-like video-may-like">
            <div class="wrapper">
                <div class="container">
                    <div class="section__header">
                        <h2 class="section__headline"><?php _e( 'Videos You May Like', 'am2' ); ?></h2>
                    </div>
                </div>
                <div class="container">
                     <?php
						while ( $may_like->have_posts() ) :

							$may_like->the_post();

							$class = [ 'video-category__item', 'col-14' ];
							$class = trim( implode( ' ', $class ) );

							$args = [];
                            $args['post'] = Models::video( get_post() );

							echo '<div class="' . esc_attr( $class ) . '">';
							Timber::render( 'video-block.twig', $args );
							echo '</div>';

							wp_reset_postdata();

						endwhile;

                        wp_reset_query();
                    ?>
                </div>
            </div>
        </div>
		<?php endif;

			// Newsletter
			get_template_part( 'subscribe' );

		?>

	</main>
	<!-- /main -->
</div>
<!-- /content -->
<?php

	// Get Footer
	get_footer();

?>