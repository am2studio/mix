<?php 

// get categories
$categories = [];
$categories = get_terms( [
	'taxonomy'      => 'category',
	'hide_empty'    => false,
	'parent'        => 0
] );


// get page
$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
?>


<?php get_header(); ?>

<!-- start:content -->
<div id="content">

	<!-- start:main -->
	<main id="main" class="main events" role="main">

		<section class="events-top">
			<div class="wrapper">
				<div class="container">

					<?php
						Timber::render('components/section-bar.twig',[   
							'className'	=> 'section-bar--line section-bar--margin',
							'title'		=> 'Mix Tix',
							'link'		=> ['url' => get_bloginfo('wpurl') . '/submit-event', 'text'=>'Submit Event']
						]);
					?>

					<?php
						Timber::render('components/archive-form.twig',[   
							'categories'	=> $categories,
							'values'		=> eventzone()->events->filter_args,
                            'post_url'      => get_bloginfo('wpurl') . '/events'
						]);
					?>
					
				</div>
			</div>
		</section>

		<section class="events-listing">
			<div class="wrapper">
				<div class="container">

					<div class="clearfix events-listing__container">

						<?php
                            global $wp_query;


							if( count($wp_query->found_posts) ){
								foreach( $wp_query->posts AS $post ){
								    $archive_event = eventzone()->events->get($post);
								    //print_r($archive_event);
									$context = [
										'className'     => 'card--vertical card--date-box col-13',
										'permalink'     => $archive_event['permalink'],
										'title'         => $archive_event['name'],
										'isFeatured'    => $archive_event['is_featured'],
										'date'          => date_i18n( 'l, M d - g A', strtotime($archive_event['event_start']) ),
										'location'      => $archive_event['place']['location'],
										'dateBox'		=> $archive_event['dates_formatted'],
									];

									if( $archive_event['image'] && isset($archive_event['image']['event_featured']) ) {
									    $context['thumb']   = '<img src="'.$archive_event['image']['event_featured']['file'].'" alt="" />';
                                    }
									$context['category'] = ( count($archive_event['categories'])>0 ) ? reset($archive_event['categories']) : [];
									$categoryId = ( $context['category'] != [] ) ? $context['category']['term_id'] : 0;
									if( $categoryId ){
										$context['category']['permalink'] = get_category_link($categoryId);
										$arr = get_option('category_'.$categoryId);
										$context['category']['color'] = $arr['color'];
									}
									// render view
									Timber::render('card.twig',$context);
								}
							}

						?>

					</div>
					
					<?php
						$args = [];
//						var_dump( eventzone()->events->event_query);
						$args['pagination'] = Timber::get_pagination();
						//print_r($args['pagination']);
						Timber::render( 'pagination.twig', $args );
					?>

				</div>
			</div>
		</section>

		<section class="events-submit">
			<div class="wrapper">
				<div class="container">

					<h3 class="events-submit__title">Submit Upcoming Events</h3>
					<p class="events-submit__text">Do you have an upcoming event that you want featured on The MIX?</p>
					<a class="events-submit__cta button" href="<?php print network_site_url('submit-event'); ?>">Submit event here</a>
					
				</div>
			</div>
		</section>

	</main>
	<!-- end:main -->

</div>

<?php get_footer(); ?>