<?php
    // Use
    use AM2\Theme\Models as Models;

    // Globals
    global $wp_query;

    // Get Header
    get_header();

    $contentClass = [ 'content', 'content--video-category' ];
    if( $wp_query->max_num_pages > 1 ) $contentClass[] = 'has-pagination';
?>
<div id="content" class="<?php echo esc_attr( implode( ' ', $contentClass ) ); ?>">
    <main id="main" class="main">
        <div class="section section--video-category video-category">
            <div class="wrapper">
                <div class="container container--flex video-category__header">
                    <?php
                        $featured = get_field( 'category_image', 'video_category_' . get_queried_object_id() );
                        if (empty($featured)) {
                            $featured = get_field( 'featured_image', 'video_category_' . get_queried_object_id() );
                        }
                        $class = $featured ? 'col-12' : 'col-1';
                    ?>
                    <div class="<?php echo esc_attr( $class ); ?>">
                        <div class="video-category__content">
                            <h1 class="video-category__name"><?php single_term_title(); ?></h1>
                            <div class="video-category__description"><?php echo term_description(); ?></div>
                        </div>
                    </div>
                    <?php if ( $featured ) : ?>
                    <div class="col-12">
                        <div class="video-category__featured">
                            <?php
                            echo wp_get_attachment_image( $featured['id'],'large',  null, [
                                'class' => 'video-category__featured-image'
                            ]);
                            ?>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="container container--flex video-category__listing">
                <?php
                    if( have_posts() ) :
                        while ( have_posts() ) :

                            the_post();

                            $class = [ 'video-category__item', 'col-14' ];
                            $class = trim( implode( ' ', $class ) );

                            $args = [];
                            $args['post'] = Models::video( get_post() );

                            echo '<div class="' . esc_attr( $class ) . '">';
                            Timber::render( 'video-block.twig', $args );
                            echo '</div>';

                        endwhile;
                    else :
                        echo '<p class="text text--uppercase no-posts">' . __( 'No videos found in this category.', 'am2' ) . '</p>';
                    endif;
                ?>
                </div>
            </div>
        </div>
        <?php if ( $wp_query->max_num_pages > 1 ) : ?>
		<div class="section section--video-category-pagination video-category-pagination">
			<div class="wrapper">
        		<div class="container">
					<?php
                        $args = [];
                        $args['pagination'] = Timber::get_pagination([ 'mid_size' => 3 ]);
                        Timber::render( 'pagination.twig', $args );
					?>
				</div>
			</div>
		</div>
		<?php endif; ?>
    </main>
    <!-- /main -->
</div>
<!-- /content -->
<?php

    // Get Footer
    get_footer();

?>