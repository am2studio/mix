<?php
    use AM2\Theme\Functions as Helpers;
    use AM2\Theme\WordPressSeo as WPSEO;
    use AM2\Theme\Models as Models;

    global $wp_query;

    // Get Header
    get_header();

?>
<div id="content" class="content content--video-archive">
    <main id="main" class="main main--video-archive">
        <div class="section section--latest-videos latest-videos">
            <div class="wrapper">
                <div class="container">
                    <div class="section__header">
                        <h2 class="section__headline"><?php _e( 'newest', 'am2' ); ?></h2>
                        <!-- there is no point for view all link from the design -->
                    </div>
                </div>
                <div class="container">
                <?php
                    if( have_posts() ) :
                        while ( have_posts() ) :

                            the_post();

                            $class = [ 'latest-videos__item', 'col-14' ];
                            $class = trim( implode( ' ', $class ) );

                            $args = [];
                            $args['post'] = Models::video( get_post() );

                            echo '<div class="' . esc_attr( $class ) . '">';
                            Timber::render( 'video-block.twig', $args );
                            echo '</div>';

                        endwhile;
                    else :
                        echo '<p class="text text--center no-posts">' . __( 'No videos found.', 'am2' ) . '</p>';
                    endif;
                ?>
                </div>
            </div>
        </div>
        <?php
            $all_video_categories = [];
            $all_video_categories = get_terms( [
                'taxonomy'      => 'video_category',
                'hide_empty'    => false,
                'parent'        => 0,
                'orderby' => 'term_order'
            ] );

            foreach( $all_video_categories as $key => $cat ) :
        ?>
        <div class="section section--video-category video-category video-category--<?php echo $cat->slug; ?>">
            <div class="wrapper">
                <div class="container">
                    <div class="section__header">
                        <h2 class="section__headline"><?php echo esc_html( $cat->name ); ?></h2>
                        <a href="<?php echo esc_url( get_term_link( $cat, 'video_category' ) ); ?>" class="section__more"><?php
                            _e( 'View All', 'am2' );
                        ?></a>
                    </div>
                </div>
                <div class="container">
                    <?php
                        $cat_query = [];
                        $cat_query = new WP_Query([
                            'post_type'     => 'video',
                            'post_status'   => 'publish',
                            'posts_per_page'=> '4',
                            'tax_query'     => [
                                [
                                    'taxonomy' => 'video_category',
                                    'field'    => 'id',
                                    'terms'    => [ $cat->term_id ]
                                ]
                            ]
                        ]);

                        if( $cat_query->have_posts() ) :
                            while ( $cat_query->have_posts() ) :

                                $cat_query->the_post();

                                $class = [ 'video-category__item', 'col-14' ];
                                $class = trim( implode( ' ', $class ) );

                                $args = [];
                                $args['post'] = Models::video( get_post() );

                                echo '<div class="' . esc_attr( $class ) . '">';
                                Timber::render( 'video-block.twig', $args );
                                echo '</div>';

                                wp_reset_postdata();

                            endwhile;
                        else :
                            echo '<p class="text text--uppercase no-posts">' . __( 'No videos found in the category.', 'am2' ) . '</p>';
                        endif;

                        wp_reset_query();
                    ?>
                </div>
            </div>
        </div>
        <?php
            endforeach;

            // Render NewsLetter
            get_template_part( 'subscribe' );

        ?>
    </main>
    <!-- /main -->
</div>
<!-- /content -->
<?php

    // Get Footer
    get_footer();

?>