import merge from 'lodash/merge';
/**
 * Parse data attrs
 * @param   {object/string} opts 
 * @todo - https://ba.northernbadger.co.uk/ba.html#options
 */
export default function (args) {
	var defaults = {
		"animationTime": 300
	};

	var options = merge({}, defaults, args);

	// Set first tab active
	$('#am2-tabs-' + options.blockId + ' .am2-tabs__title:first-child').addClass('am2-tabs__title--current');
	$('#am2-tabs-' + options.blockId + ' .am2-tabs__panel:first-child').addClass('am2-tabs__panel--current');

	// Get current tab from URL
	var hash = $.trim(window.location.hash);
	var $blockId = hash.split('-');
	if (hash) {
		var $index = $('#am2-tabs-' + $blockId[1] + ' .am2-tabs__link[href=' + hash + ']').parent().index();
		$('#am2-tabs-' + $blockId[1] + ' .am2-tabs__title').removeClass('am2-tabs__title--current');
		$('#am2-tabs-' + $blockId[1] + ' .am2-tabs__panel').removeClass('am2-tabs__panel--current');
		$('#am2-tabs-' + $blockId[1] + ' .am2-tabs__link[href=' + hash + ']').parent().addClass('am2-tabs__title--current');
		$('#am2-tabs-' + $blockId[1] + ' .am2-tabs__panel').eq($index).addClass('am2-tabs__panel--current');
	}

	// AM2 Tabs
	$('#am2-tabs-' + options.blockId + '').on('click', '.am2-tabs__title', function () {
		var $that = $(this);
		if (!$(this).hasClass('am2-tabs__title--current')) {
			$('#am2-tabs-' + options.blockId + ' .am2-tabs__title').removeClass('am2-tabs__title--current');
			$('#am2-tabs-' + options.blockId + ' .am2-tabs__panel').fadeOut(options.animationTime).promise().done(function () {
				$('#am2-tabs-' + options.blockId + ' .am2-tabs__panel').removeClass('am2-tabs__panel--current');
				$('#am2-tabs-' + options.blockId + ' .am2-tabs__panels')
					.children('.am2-tabs__panel')
					.eq($that.index())
					.fadeIn(options.animationTime)
					.addClass('am2-tabs__panel--current');
			});

			$(this).addClass('am2-tabs__title--current');
		}
	});
};