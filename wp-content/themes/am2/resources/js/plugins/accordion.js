import merge from 'lodash/merge'; 
/**
 * Parse data attrs
 * @param   {object/string} opts 
 * @todo - https://ba.northernbadger.co.uk/ba.html#options
 */
export default function am2Accordion(args) { 
	var defaults = {
		"blockId": "am2-accordion-1",
		"animationTime": 300,
	};

	var options = merge({}, defaults, args); 

	// AM2 Accordion
	$( '#' + options.blockId + '' ).on( 'click', '.am2-accordion__panel', function () {
		if ( $( this ).hasClass( 'am2-accordion__panel--current' ) ) {
			$( this ).removeClass( 'am2-accordion__panel--current' ).find( '.am2-accordion__panel-content' ).slideUp( options.animationTime );
		} else {
			var currentAccordionPanel = $( '#' + options.blockId + ' .am2-accordion__panel--current' );
			if ( currentAccordionPanel.length > 0 ) {
				currentAccordionPanel.removeClass( 'am2-accordion__panel--current' ).find( '.am2-accordion__panel-content' ).slideUp( options.animationTime );
			}
			$( this ).addClass( 'am2-accordion__panel--current' ).find( '.am2-accordion__panel-content' ).slideDown( options.animationTime );
		}
	});
};