import merge from 'lodash/merge'; 
//import mapStyles from './am2-google-maps-style';

// Just for demo porposes -> use as above instead { merge } from 'lodahs' since it ads
// less weight to JS than doing so.

export default function (args) {

	var defaults = {
		"zoom": 15,
		"scrollwheel": false,
		"draggable": false
	};

	var options = merge({}, defaults, args);

	var map = new google.maps.Map(document.getElementById(options.blockId), {
		zoom: options.zoom,
		scrollwheel: options.scrollwheel,
		draggable: options.draggable,
		center: new google.maps.LatLng(options.lat, options.lng),
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		//styles: mapStyles
	});

	var infowindow = new google.maps.InfoWindow({ maxWidth: 350 });

	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(options.lat, options.lng),
		map: map,
		icon: am2.templateDirectory + '/resources/svg-builder/location/pin-g.svg',
		animation: google.maps.Animation.DROP,

	});

	google.maps.event.addListener(marker, 'click', (function (marker) {
		return function () {
			infowindow.setContent('<p class="wca-map__info">' + options.address + '</p>');
			infowindow.open(map, marker);
		};
	})(marker));
};