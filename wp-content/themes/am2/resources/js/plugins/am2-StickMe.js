/**
 * Sticky Plugin
 * @author 		Bican Marian Valeriu
 * @license 	https://www.iambican.com/
 * @uses		Component class
 * @description	{
 *  Adapted for my plugin setup for @am2Studio
 * 	- See Main Bundle on how to use this plugin, or at least somewhere in the code you should find a refference
 *  to the plugin, lazy to write docs
 * }
 */

import Component from './am2-component';

export default (function (am2) {
	'use strict';

	var lastScrollTop = 0;

	let _defaults = {
		minWidth: 992,
		containerSelector: '',
		classes: {
			active: 'sticky--active',
			bottom: 'sticky--at-bottom'
		},
		scrollDirection: {
			enabled: false,
			delta: 200
		}
	};

	/**
	 * @class
	 */
	class stickMe extends Component {
		/**
		 * Construct Animate instance
		 * @constructor
		 * @param {Element} el
		 * @param {Object} options
		 */
		constructor(el, options) {
			super(stickMe, el, options);
			this.el = el;
			this.el.stickMe = this;
			/**
			 * Options for the animation
			 * @member stickMe#options
			 */
			this.options = Object.assign({}, stickMe.defaults, {
				containerSelector: el.parentNode || document.getElementsByTagName('BODY')[0]
			}, options);
			stickMe._elements.push(this);
			this._setupEventHandlers();
		}

		static get defaults() {
			return _defaults;
		}

		static init(els, options) {
			return super.init(this, els, options);
		}

		/**
		 * Get Instance
		 */
		static getInstance(el) {
			let domElem = !!el.jquery ? el[0] : el;
			return domElem.stickMe;
		}

		/**
		 * Teardown component
		 */
		destroy() {
			if (stickMe.getInstance(this.el)) {
				this._unwrap();
				this._removeStyles();
				this._removeClasses();
				this._removeEventHandlers();
				let index = stickMe._elements.indexOf(this);
				stickMe._elements.splice(index, 1);
				this.el.style.width = null;
				this.el.stickMe = undefined;
			}
		}

		getLimits() {
			var elem = this.el;

			if (parseInt(this.options.minWidth) && window.innerWidth <= parseInt(this.options.minWidth)) {
				if (elem.parentNode.classList.contains('stick-me')) this._unwrap();
				this._removeStyles();
				this._removeClasses();
				this.disabled = true;
				return;
			} else this.disabled = false;

			if (!elem.parentNode.classList.contains('stick-me')) this._wrap();

			elem.style.width = elem.parentNode.clientWidth + 'px';

			var containerEl = document.querySelector(this.options.containerSelector),
				elemOffsetTop = elem.getBoundingClientRect().top + window.scrollY,
				containerOffsetTop = containerEl.getBoundingClientRect().top + window.scrollY;

			let padding = Object.assign({}, { top: 0, bottom: 0 }, this.options.padding || {});

			this.data = {
				padding: padding,
				start: (this.options.containerSelector ? containerOffsetTop : elemOffsetTop) - padding.top, // Top Pad
				stop: containerOffsetTop + containerEl.offsetHeight - elem.offsetHeight - padding.bottom, // Bottom Pad
			};

			return;
		}

		getDirection() {
			if (this.options.scrollDirection.enabled === false) return;
			var scrollY = window.pageYOffset;
			if (Math.abs(lastScrollTop - scrollY) <= this.options.scrollDirection.delta) return;
			if (scrollY > lastScrollTop && scrollY > this.options.scrollDirection.delta)
				this.oldDirection = this.direction, this.direction = 'sticky--down';
			else if (scrollY + window.innerHeight < document.body.scrollHeight)
				this.oldDirection = this.direction, this.direction = 'sticky--up';
			lastScrollTop = scrollY;
		}

		onScroll() {
			var elem = this.el, scrollY = window.pageYOffset;

			if (this.disabled) return;

			if (this.options.scrollDirection.enabled && !elem.classList.contains(this.direction)) {
				elem.classList.remove(this.oldDirection);
				elem.classList.add(this.direction);
			}

			var to = this.data.stop - this.data.padding.top,
				from = this.data.start - this.data.padding.bottom;

			if (from < scrollY && to > scrollY) {
				if (this.options.classes.active) elem.classList.add(this.options.classes.active);
				if (this.options.classes.bottom) elem.classList.remove(this.options.classes.bottom);
				elem.style.position = 'fixed';
				elem.style.top = this.data.padding.top + 'px';
				elem.style.left = 'auto';
				elem.dispatchEvent(new CustomEvent('stickMe:isStuck'));
			} else if (scrollY >= to) {
				if (this.options.classes.active) elem.classList.add(this.options.classes.active);
				if (this.options.classes.bottom) elem.classList.add(this.options.classes.bottom);
				elem.style.position = 'absolute';
				elem.style.top = to - from + 'px';
				elem.style.left = 'auto';
				elem.dispatchEvent(new CustomEvent('stickMe:atBottom'));
			} else {
				this.el.style.top = null;
				this.el.style.left = null;
				this.el.style.position = null;
				elem.classList.remove(this.options.classes.active);
				elem.classList.remove(this.options.classes.bottom);
			}
		}

		sequence() {
			let updated = false;
			if (updated !== true) this.getLimits(), updated = true;
			this.onScroll();
			this.getDirection();
		}

		_wrap() {
			let el = this.el, wrapper = document.createElement('div');
			wrapper.classList.add('stick-me');
			wrapper.style.position = 'relative';
			wrapper.style.height = el.offsetHeight + 'px';
			el.parentNode.insertBefore(wrapper, el);
			el.classList.add('sticky');
			wrapper.appendChild(el);
		}

		_unwrap() {
			let el = this.el;
			jQuery(el).unwrap();
		}

		_removeStyles() {
			this.el.style.top = null;
			this.el.style.left = null;
			this.el.style.width = null;
			this.el.style.position = null;
		}

		_removeClasses() {
			// IE 11 bug (can't remove multiple classes in one line)
			this.el.classList.remove('sticky');
			this.el.classList.remove('sticky--up');
			this.el.classList.remove('sticky--down');
			this.el.classList.remove(this.options.classes.active);
			this.el.classList.remove(this.options.classes.bottom);
		}

		/**
		 * Setup Events
		 */
		_setupEventHandlers() {
			this._handleSequenceBound = this.sequence.bind(this);
			this._handleResizeBound = this.getLimits.bind(this);
			window.addEventListener('load', this._handleSequenceBound);
			window.addEventListener('scroll', this._handleSequenceBound);
			window.addEventListener('resize', this._handleResizeBound);
		}

		/**
		 * Remove Event Handlers
		 */
		_removeEventHandlers() {
			if (stickMe.getInstance(this.el)) {
				window.removeEventListener('load', this._handleSequenceBound);
				window.removeEventListener('scroll', this._handleSequenceBound);
				window.removeEventListener('resize', this._handleResizeBound);
			};
		}
	}

	/**
	 * @static
	 * @memberof stickMe
	 */
	stickMe._elements = [];
	am2.stickMe = stickMe;

}).apply(this, [window.am2]);