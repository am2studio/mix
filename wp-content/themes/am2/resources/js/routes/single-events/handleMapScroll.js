/**
 * Handle Single Events Map Scroll
 */
export default () => {
	$('.js-goto-map').on('click', function (e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("#jsMap").offset().top - $('#header').height() - 50
		}, 200);
	});
};