/**
 * Handle Submit Event Page
 */
export default () => {
	let optsH, optsM, optsP = '';

	// hours
	optsH += '<option value="">HH</option>';
	for (let hh = 0; hh <= 12; hh++) {
		optsH += '<option value="' + hh + '">' + pad(hh, 2) + '</option>';
	}

	// minutes
	optsM += '<option value="">MM</option>';
	for (let mm = 0; mm <= 59; mm++) {
		if (mm % 5 == 0) {
			optsM += '<option value="' + mm + '">' + pad(mm, 2) + '</option>';
		}
	}

	// timezone
	optsP += '<option value="AM">AM</option>';
	optsP += '<option value="PM">PM</option>';

	$('.js-minute').find('select').empty().append(optsM);

	$('.js-hour').find('select').empty().append(optsH);
	$('.js-minute').find('select').empty().append(optsM);
	$('.js-period').find('select').empty().append(optsP);

	function pad(num, size) {
		var s = num + "";
		while (s.length < size) s = "0" + s;
		return s;
	}

	// on submit press
	$('.gform_footer').find('button').on('click', function (e) {
		$('.js-gf-loading').addClass('active');
		$('html, body').animate({
			scrollTop: $("body").offset().top - $('#header').height() - 50
		}, 200);
	});
};