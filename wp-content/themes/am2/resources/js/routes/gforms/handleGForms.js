/**
 * Handle Gforms Labels for Contact
 */
const handleGFormsLabels = () => {
	var inpsToMonitor = document.querySelectorAll(".gform_wrapper form span[class^='name_'] input");

	for (var J = inpsToMonitor.length - 1; J >= 0; --J) {
		inpsToMonitor[J].addEventListener("change", adjustStyling, false);
		inpsToMonitor[J].addEventListener("keyup", adjustStyling, false);
		inpsToMonitor[J].addEventListener("focus", adjustStyling, false);
		inpsToMonitor[J].addEventListener("blur", adjustStyling, false);
		inpsToMonitor[J].addEventListener("mousedown", adjustStyling, false);

		var evt = document.createEvent("HTMLEvents");
		evt.initEvent("change", false, true);
		inpsToMonitor[J].dispatchEvent(evt);
	}

	function adjustStyling(zEvent) {
		let input = zEvent.target, inpVal = input.value;
		if (inpVal && inpVal.replace(/^\s+|\s+$/g, "")) input.classList.add("has-value");
		else input.classList.remove("has-value");
	}
};

// Button
const handleGFormsButton = () => {
	jQuery(document).on('gform_post_render', () => {
		gform.addFilter('gform_spinner_target_elem', function ($targetElem, formId) {
			// Empty span since their script inserts after for some reason
			return $('.gform_footer .button--full-width .button__icon');  
		});
	});
};

// Upload Field
const handleGFormsUpload = () => {
	let selector = '.ginput_container_fileupload input[type="file"]', $field = $(selector);
	let hasPreview = $field.find('div[id^="gform_preview_"]'), previewDel = $field.find('.gform_delete');

	if (hasPreview) $field.parent().addClass('has-val');
	previewDel.on('click', () => $field.parent().removeClass('has-val'));

	$field.parent().attr('data-text', $field.parent().prev().text());

	$("form").on("change", ".ginput_container_fileupload input[type='file']", function () {
		let $this = $(this), value = $this.val();
		if (value !== '') {
			$this.parent(".ginput_container_fileupload")
				.attr("data-text", $(this).val().replace(/.*(\/|\\)/, ''))
				.toggleClass('has-val');
		}
	});
};

export { handleGFormsLabels, handleGFormsButton, handleGFormsUpload };