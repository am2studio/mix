/**
 * Events Archive Filters
 */
export default () => {

	// fields
	let fieldDisplay = 	document.getElementById('dateDisplay');
	let fieldStart = 	document.getElementById('dateStart');
	let fieldEnd = 		document.getElementById('dateEnd');
	let elemForm = 		document.querySelector('.js-filter-form');
	let elemDate = 		document.querySelector('.js-filter-calendar');
	let elemCategory = 	elemForm.querySelector('.js-filter-category');
	let elemLoading = 	elemForm.querySelector('.js-filter-loading');
	let elemClear = 	elemForm.querySelector('.js-filter-clear');

	// submit form with loader
	function formSubmit(){
		elemLoading.classList.add('active');
		setTimeout(function(){
			elemForm.submit();
		},0);
	}

	// init selectize
	$('.selectize').selectize({
		create: true,
		sortField: 'text'
	});

	// form listener
	elemForm.addEventListener("submit",
		function(e) {
			e.preventDefault();
			formSubmit();
		}
	);
	
	// date picker
	var tmpStart,tmpEnd;
	var picker = new Lightpick({
		field: document.getElementById('dateDisplay'),
		format: 'D MMM YYYY',
		separator: ' - ',
		singleDate: false,
		footer: false,
		onSelect: function (start, end) {
			if (start !== null && end !== null) {
				var str = '';
				str += start.format('D MMM');
				str += ' - ';
				str += end.format('D MMM');
				fieldDisplay.value = str;
				fieldStart.value = start.format();
				fieldEnd.value = end.format();
			}
		},
		onOpen: function() {
			// save dates on open
			let vStart = picker.getStartDate();
			let vEnd = picker.getEndDate();

			tmpStart = ( vStart !== null ) ? vStart._d : 0;
			tmpEnd = ( vEnd !== null ) ? vEnd._d : 0;
		},
		onClose: function () {
			// update calendar container if date selected
			elemDate.classList.toggle('active', ( fieldDisplay.value ) );
			// check dates on close
			let vStartClose = picker.getStartDate();
			let vEndClose = picker.getEndDate();
			let tmpStartClose = ( vStartClose !== null ) ? vStartClose._d : 0;
			let tmpEndClose = ( vEndClose !== null ) ? vEndClose._d : 0;
			// if dates changed, submit form
			if( tmpStartClose != tmpStart || tmpEndClose != tmpEnd ) {
				formSubmit();
			}
			
		}
	});

	// initial date picker values
	if (fieldStart.value && fieldEnd.value) {
		picker.setDateRange(fieldStart.value, fieldEnd.value);
		fieldDisplay.value = moment(fieldStart.value).format('D MMM') + ' - ' + moment(fieldEnd.value).format('D MMM');
	}
	
	// category listener
	let elemCategorySelectize = elemCategory.selectize;
	elemCategorySelectize.on("change",
		function(e) {
			formSubmit();
		}
	);

	// clear filters
	elemClear.addEventListener("click",
		function(e) {
			elemLoading.classList.add('active');
		}
	);
};