/**
 * Handle Single Post Lightbox
 */
export default () => {
	// if there are hidden photos
	if ($('.js-gallery .hidden').length > 0) {
		$('.js-show-hidden').on('click', function (e) {
			e.preventDefault();
			// remove cta button class / remove span element / show hidden images
			$(this).removeClass('js-show-hidden').find('span').fadeOut(100, function () {
				$(this).remove();
				$('.js-gallery .hidden').removeClass('hidden');
			});
		});
	}
	// init lightbox
	lightbox.option();
};