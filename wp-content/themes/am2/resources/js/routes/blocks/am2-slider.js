/**
 * Handle Slide Change
 */
const handleSlideChange = () => {
	let slicks = document.querySelectorAll('[data-plugin-slick]');
	for (var item of slicks) {

		var $slick = jQuery(item);

		$slick.on('init afterChange', function (event, slick, currentSlide, nextSlide) {
			// Handles the iframe loading
			currentSlide = currentSlide ? currentSlide : 0;
			let slide = $slick.find('.slick-slide:eq(' + currentSlide + ')');
			let plugin = slide.find('.am2-slider__video');
			let customVideo = slide.find('.js-am2-slider__video-custom');
			if (plugin.length) {
				let instance = am2.YTIframe.getInstance(plugin);
				instance.createFrame();
            }
            if (customVideo.length) {
                $(customVideo).get(0).play();
            }

		});

		$slick.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
			// Handles the iframe destroying
			let slide = $slick.find('.slick-slide:eq(' + currentSlide + ')');
            let plugin = slide.find('.am2-slider__video');
            let customVideo = slide.find('.js-am2-slider__video-custom');
            if (plugin.length) plugin.empty(), plugin.removeClass('loaded');
            if (customVideo.length) {
                $(customVideo).get(0).pause();
            }
		});
	}
};

// Export
export default handleSlideChange;
export { handleSlideChange };