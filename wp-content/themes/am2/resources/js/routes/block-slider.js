/** 
 * Export default Block Slider JS
 * @uses { 
 * 	- YT iFrame LazyLoad Plugin
 * }
 * @description Since most of the JS on this file is not needed ASAP, we lazy load this as a route
 */
import '../plugins/am2-YTIframe'; 

export default (function (am2, $) {
	return (function () {
		// YT iFrame Lazy Load  
		let iframeElems = document.querySelectorAll('[data-plugin-youtube]');
		am2.YTIframe.init(iframeElems); 

	})();
}).apply(this, [window.am2, jQuery]);
