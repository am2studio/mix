/** 
 * Export default Single Video
 * @uses {
 * 	- Sticky Plugin
 * 	- YT iFrame LazyLoad Plugin
 * }
 * @description Since most of the JS on this file is not needed ASAP, we lazy load this as a route
 */
import './../plugins/am2-StickMe';
import './../plugins/am2-YTIframe';

export default (function (am2) {
	return (function () {

		// Log something to know is working fine
		console.log('The JS from this page should be lazyloaded and appended to body.');

		// YT iFrame Lazy Load
		let iframeElems = document.querySelectorAll('[data-plugin-youtube]');
		am2.YTIframe.init(iframeElems);

		// Sticky Plugin
		let stickedElems = document.querySelectorAll('[data-plugin-sticky]');
		am2.stickMe.init(stickedElems); 

	})();
}).apply(this, [window.am2]);
