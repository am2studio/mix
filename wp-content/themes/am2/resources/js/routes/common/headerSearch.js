/**
 * Header Search
 */
export default function () {
	let search = document.forms[0];
	if( !search ) return;
	let input = search.elements['s'];

	let value = input.value;
	if (value !== '') search.classList.add('has-focus');

	input.addEventListener('focus', () => {
		search.classList.add('has-focus');
	});

	input.addEventListener('blur', () => {
		let value = input.value;
		if (value === '') search.classList.remove('has-focus');
	});

	search.addEventListener('submit', (e) => {
		let value = input.value;
		if (value === '') e.preventDefault();
		return;
	});
};