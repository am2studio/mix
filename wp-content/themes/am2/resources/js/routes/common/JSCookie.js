/**
 * Set Landing Page Cookie
 */
const handleLandingCookie = () => { 

	// Handle landing location links
	let locations = [...document.querySelectorAll('.landing-locations__item-link')];

	if (locations) { // Not a necessary check due to our JS manager, but just in case
		for (var index in locations) {
			locations[index].addEventListener('click', (e) => {
				e.preventDefault();
				let item = e.target.parentElement;
				let value = {
					id: item.dataset.site_id,
					url: item.getAttribute('href')
				};
				Cookies.set('am2_user_location', JSON.stringify(value), { expires: 365 });
				window.location = value.url;
			});
		}
	}
};

/**
 * Set Location Switcher Cookie
 */
const handleLocationSwitcher = () => {
	// Handle landing location links
	let $field = $('.location-select__field'); 

	$field.change((e) => {
		let value = {
			id: undefined, // Since selectize JS do not adds data attributes, not a big deal, not used
			url: $field.val()
		};
		Cookies.set('am2_user_location', JSON.stringify(value), { expires: 365 });
		window.location = $field.val();
	});

	$('.location-visit-global__link').on('click', () => Cookies.remove('am2_user_location'));
};

/**
 * Maybe Redirect if user has Cookie Set
 */
const maybeRedirect = () => {
	let am2Cookie = Cookies.get('am2_user_location');
	let hasPreferedLocation = am2Cookie !== undefined && JSON.parse(am2Cookie);
	let hasLocationUrl = hasPreferedLocation && hasPreferedLocation.url;
	hasLocationUrl = hasLocationUrl !== false && window.location.href.includes(hasLocationUrl);
	console.log('inludesLocationHref::', hasLocationUrl);
	if (am2Cookie !== undefined && !hasLocationUrl) window.location = hasPreferedLocation.url;
};

export { handleLocationSwitcher, handleLandingCookie, maybeRedirect };