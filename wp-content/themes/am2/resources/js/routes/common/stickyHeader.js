/**
 * Sticky Header
 * @param   {object/string} opts
 * @return  {object/string}
 */
export default function () {
	var stickyHeader = function () {
		var header = $('.header--site');
		if( ! header ) return;
		
		$('.site-container').css("padding-top", header.outerHeight());

		if ($(window).scrollTop() > 1) {
			header.addClass('sticky');
		} else {
			header.removeClass('sticky');
		}
	};

	window.addEventListener('load', stickyHeader);
	window.addEventListener('scroll', am2.fn.debounce(stickyHeader, 150));
	window.addEventListener('resize', am2.fn.debounce(stickyHeader, 150));
};