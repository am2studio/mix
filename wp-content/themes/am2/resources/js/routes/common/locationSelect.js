/**
 * Location Select
 */
export default function () {
	if ($.isFunction($.fn.selectize)) {
		let prevSetup = Selectize.prototype.setup, $field = $('.location-select__field');

		// Disable Search
		Selectize.prototype.setup = function () {
			prevSetup.call(this);
			this.$control_input.prop('readonly', true);
		};

		$field.selectize();
	};
};