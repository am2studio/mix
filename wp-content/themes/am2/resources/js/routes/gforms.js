// Gforms  
import { handleGFormsLabels, handleGFormsButton, handleGFormsUpload} from './gforms/handleGForms'; 

// Init the above
(() => {
	handleGFormsLabels();
	handleGFormsUpload();
	handleGFormsButton();

	jQuery(document).on('gform_post_render', () => {
		handleGFormsLabels();
		handleGFormsUpload(); 
	}); 
})();
