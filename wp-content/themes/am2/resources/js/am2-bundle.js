/**
 * ===========================
 * @author: 	am2Studio	==
 * @package: 	JS Bundle	==
 * @version:	2.0.0		==
 * ===========================
 * 
 * @description: {
 * --- @usage {
 * 	a) any file create into /routes/ directory will be auto picked up by webpack and will generate a new entry, 
 * 	for code splitting ( note: subfolders are intended to be ignored )
 *  EG: you have a popup plugin/tooltips and other js that runs on user clicks or is needed under viewport - 
 * 	use this to code split your JS for improving the performance of the site a bit
 * 	b) routes generated can be enqueued with wp_enqueue inside assets or if needed later (under viewport) is recommended
 * 	to be lazyloaded using js examples bellow.
 * }
 * --- @naming {
 * 	a) any plugin we create goes to /plugins/ folder (eg, the reusable stuff, current project has sticky.js and iframe.js )
 *  b) any helper functions goes to /helpers/ folder (debounce, parseData etc)
 * 	c) route specific js goes to /routes/your-route-name.js while its functions/classes/etc goes into a subfolder with same name
 * 	if required but is recommended if it gets big (not the case for this project and is up to personal prefference)
 * }
 * 
 * @JSManager { The class that handles JS loading/running
 * 	- common js init key runs first
 *  - route specific js init/complete keys runs after (same order)
 *  - common js complete key runs after
 *  - JSManager.loadScript() - runs last since is async 
 * }
 * 
 * @loadScript {
 * --- @arguments {
 * 		1 -> handle name/script id
 *  	2 -> script path
 * 		3 -> callback ( function to run after the script is loaded - not required)
 *  	4 -> string for cache busting ( adds a ?ver=randomchars to the string - not required / may get removed ) 
 *	}
 * --- @usage {
 * 		a) loadScript - lazy loads a piece of js by first checking into am2.assetsEnqueue key ( @see class-am2-assets.php )
 * 		b) if the script is already enqueued using WP, your callback will run imediately
 * 		c) if the script is not found above, it will create it, load it then run callback
 * 		d) second call of this, with the same handle name will not generate another script, but run the callback
 * 	} 
 */

// JSManager and Helpers
import _JSManager from './am2-JSManager';
import helperDebounce from './helpers/debounce';
import helperParseData from './helpers/parseData';
import helperGenParams from './helpers/createParams';

// Plugins import pluginTabs from './plugins/tabs';
import pluginGoogleMaps from './plugins/googleMaps';

// Common JS - loaded on all pages
import handleSearchForm from './routes/common/headerSearch';
import handleMobileMenu from './routes/common/mobileMenu';
import handleStickyHeader from './routes/common/stickyHeader';
import handleLocationSelect from './routes/common/locationSelect'; 
import { maybeRedirect, handleLandingCookie, handleLocationSwitcher } from './routes/common/JSCookie';

// Single Post
import handleLightbox from './routes/single-post/handleLightbox';

// Single Events
import handleMapScroll from './routes/single-events/handleMapScroll';

// Events Archive
import handleEventsFilters from './routes/events-archive/handleFilters';

// Submit Event Page
import handleSubmitEvent from './routes/submit-event/handleSubmitEvent';

/**
 * Blocks JS - loaded on specific block (if it exists in body class)
 * @see		- class-am2-hooks.php @ body_class() filter function
 * @info 	- each block will generate a class like: am2-block-{ acf_name } where
 * acf_name is your field name in flexible editor. This class can be used as `am2_block_acf_name`
 * Inside the routes bellow, as a key. Any JS located there will be run only when that block exists
 */

// AM2 Slider Block
import { handleSlideChange } from './routes/blocks/am2-slider';

(function (am2) {

	'use strict';

	// Attach Some Required Plugins
	am2.fn = {};
	am2.fn.debounce = helperDebounce;
	am2.fn.getOptions = helperParseData;
	am2.fn.createParams = helperGenParams;

	// Attach Specific Route JS
	am2 = {
		...am2,
		...{
			routes: {
				/**
				 * This prop includes the global JS fired on every page
				 */
				common: {
					/**
					 * Runs first before any JS
					 */
					init: () => {
						/**
						 * Here we have all common js loaded asap on all pages
						 */
						var html = document.documentElement;
						html.classList.remove('no-js');
						html.classList.add('js');

						// Redirect
						//maybeRedirect();

						handleStickyHeader();
						handleSearchForm();
						handleMobileMenu();

						(() => { // Simply look for an selector ASAP and add class to body for a route JS
							let isGFormOnPage = document.querySelector('.gform_wrapper');
							if (isGFormOnPage) document.body.classList.add('am2-has-gform');
						})();
					},
					// Should be removed when main site will have just landing page
					// Is still here just for the sake of having selectized location switcher
					// on main site for QA
					complete: () => {
						let jsPath = am2.templateDirectory + '/resources/js/vendor/selectize.min.js';
						JSManager.loadScript('selectize', jsPath, () => handleLocationSelect());
					}
				},
				// Pages with Tab Component
				page_template_page_tabbed: {
					init: () => {
						pluginTabs({ blockId: 'content' });
					},
				},
				// Landing Template
				page_template_page_landing: {
					// Or `am2_is_main_site` for a sitewide usage of the main site
					// But main site main have subpages that user might visit so we stick with just landing
					//init: () => handleLandingCookie()
				},
				// For other sites that are not landing (aka main)
				am2_is_not_main_site: { // Custom class added via body_class filter
					init: () => handleLocationSwitcher(),
					/* complete: () => {
						let jsPath = am2.templateDirectory + '/resources/js/vendor/selectize.min.js';
						JSManager.loadScript('selectize', jsPath, () => handleLocationSelect());
					} */
				},
				// Pages with GForm on it - we hack this route by adding a class before ( it does not really exists )
				am2_has_gform: {
					complete: () => { // Lazy Load on pages with gforms
						let jsPath = am2.templateDirectory + '/assets/dev/js/routes/gforms.js';
						JSManager.loadScript('gforms', jsPath, null, '@#r24m2#');
					}
				},
				// Single Post JS
				single_post: {
					init: () => handleLightbox()
				},
				// Single Video JS
				single_video: {
					init: () => { // Lazy Load the full JS Route for single video
						let jsPath = am2.templateDirectory + '/assets/dev/js/routes/single-video.js';
						JSManager.loadScript('single-video', jsPath, null, '@2s24fs');
					}
				},
				// Events Listing JS
				post_type_archive_events: {
					init: () => {
						let jsPath = am2.templateDirectory + '/resources/js/vendor/selectize.min.js';
						JSManager.loadScript('selectize', jsPath, () => handleEventsFilters()); 
					}
				},
				// Single Event JS
				single_events: {
					init: () => {
						// Scroll To Google Map
						handleMapScroll();

						// Lazy Load Gmap
						let mapBlock = document.getElementById('jsMap'); 
						let options = am2.fn.getOptions(mapBlock.dataset.map_options);
						let jsPath = 'https://maps.googleapis.com/maps/api/js?key=' + options.api_key;
						JSManager.loadScript('googleMaps', jsPath, () => new pluginGoogleMaps(options)); 
					}
				},
				// Runs on Submit Event Template
				page_template_page_submit_event: {
					init: () => handleSubmitEvent()
				},
				// Runs if slider block is on page
				am2_block_slider: {
					init: () => {
						// Lazy Load Iframe plugin before slick since it will init on slick events
						let routePath = am2.templateDirectory + '/assets/dev/js/routes/block-slider.js';
						JSManager.loadScript('am2-slider', routePath, () => handleSlideChange());
						
						// Load slick slider and init automatically
						let jsPath = am2.templateDirectory + '/resources/js/vendor/slick.min.js';
						JSManager.loadScript('slick', jsPath, () => {
							let slicks = document.querySelectorAll('[data-plugin-slick]');
							for (var item of slicks) jQuery(item).slick(am2.fn.getOptions(item.dataset.options)); 
						});
					}
				}
			}
		}
	};

	/**
	 * Init the awesomeness itself
	 */
	var JSManager = new _JSManager(am2);
	document.addEventListener('DOMContentLoaded', JSManager.loadEvents());

}).apply(this, [window.am2]);