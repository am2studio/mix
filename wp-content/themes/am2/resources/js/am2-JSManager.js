import _pickBy from 'lodash/pickBy';
import _filter from 'lodash/filter';

/**
 * Utility to trigger JS code based on clasname routing
 */
export default class JSManager {
	/**
	 * Generic constructor 
	 * @constructor
	 * @param {object} namespace 	| JS Object
	 * @param {string} routes		| Key name containing routes 
	 */
	constructor(namespace, routes = 'routes') {
		this.namespace = namespace; // WP Localized head obj
		this.routesKey = routes;
		this.routesDir = this.namespace.templateDirectory + '/assets/js/routes/';
		this.extendedR = _pickBy(this.namespace[this.routesKey], v => v instanceof Array);
		if (typeof this.namespace.assetsLoaded == 'undefined') this.namespace.assetsLoaded = [];
		if (typeof this.namespace.functionsQueue == 'undefined') this.namespace.functionsQueue = [];
	}

	fireRoute(func, funcname, args) {
		var fire;
		funcname = funcname === undefined ? 'init' : funcname;
		fire = func !== '';
		fire = fire && this.namespace[this.routesKey][func];
		fire = fire && typeof this.namespace[this.routesKey][func][funcname] === 'function';

		if (fire) return this.namespace[this.routesKey][func][funcname](args);
	} 

	/**
	 * Meant to be run on load
	 * @info 	It handles all necessary JS init from routes
	 */
	loadEvents() {
		this.fireRoute('common');
		let wpRoutesClass = [...document.body.classList];
		for (let cls of wpRoutesClass) {
			let route = cls.replace(/-/g, '_').split(/\s+/);
			// Fire Manual Routes
			this.fireRoute(route);
			this.fireRoute(route, 'complete');

			// AutoLoad Routes Directory
			// @todo with glob probably

			// Additional Extended Routes 
			_filter(this.extendedR, (v, k) => v.includes(cls) && this.loadScript(k, this.routesDir.concat(k, '.js')));
		}
		this.fireRoute('common', 'complete');
	}

	hashId(id) {
		var hash = 0, i, chr;
		if (id.toString().length === 0) return hash;
		for (i = 0; i < id.length; i++) {
			chr = id.charCodeAt(i);
			hash = ((hash << 5) - hash) + chr;
			hash |= 0; // Convert to 32bit integer
		}
		return hash;
	}

	createScript(options) {
		let script = document.createElement('script');
		let url = options.url;
		if (options.hash.length) url = url.concat('?ver=', this.hashId(options.hash));
		script.type = options.type || 'text/javascript';
		script.async = !!options.async;
		script.id = options.id;
		script.src = url;
		return script;
	}

	getScriptById(id) {
		return (id && document.getElementById(id));
	}

	getScriptFromObj(id) {
		return ((this.namespace.assetsEnqueue.indexOf(id) > -1) ? true : false);
	}

	executeFnQueue(key, queue_array) {
		while (queue_array[key].length > 0) (queue_array[key].shift())();
	}

	/**
	 * Used to lazyload any script/route
	 * @param {string} 		id 			The id of the script
	 * @param {string} 		url			The url of the script
	 * @param {function} 	callback 	The callback to be run after the script is loaded
	 * @param {string} 		hash 		Random chars used for creating cache hash version
	 */
	loadScript(id, url, callback, hash = false) {
		let foundScript = this.getScriptFromObj(id) || this.namespace.assetsLoaded[id] === true;
		if (foundScript && typeof callback === 'function') return callback();
		if (typeof this.namespace.functionsQueue[id] === 'undefined') this.namespace.functionsQueue[id] = [];

		if (callback && typeof callback === 'function') this.namespace.functionsQueue[id].push(callback);

		if (this.getScriptById(id)) return;

		let body = document.getElementsByTagName('body')[0] || document.documentElement;
		let script = this.createScript({ id, url, hash });

		script.onerror = () => console.warn(`JSManager - the url for script id '${id}' was not resolved. Please check.`);

		script.onload = script.onreadystatechange = () => {
			let isReady = (!script.readyState || script.readyState === 'loaded' || script.readyState === 'complete');
			if (!this.namespace.assetsLoaded[id] && isReady) {
				// Handle memory leak in IE
				script.onload = script.onreadystatechange = null;
				this.namespace.assetsLoaded[id] = true;
				this.executeFnQueue(id, this.namespace.functionsQueue);
			}
		};

		// Fire the script
		body.appendChild(script);
	}
};