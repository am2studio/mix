/**
 * Check if client is on IE/Edge Browser 
 * @return  {bolean}
 */
export default () => {
	var ua = window.navigator.userAgent, msie = ua.indexOf('MSIE '), trident = ua.indexOf('Trident/');
	if (msie > 0 || trident > 0) return true;
	return false;
};

export const isEDGE = () => {
	var ua = window.navigator.userAgent, edge = ua.indexOf('Edge/');
	if (edge > 0) return true;
	return false;
};

export const isIE = () => {
	var undef,
		v = 3,
		div = document.createElement('div'),
		all = div.getElementsByTagName('i');
	while (
		div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
		all[0]
	);
	console.log(v);
	return v > 4 ? v : undef;
};