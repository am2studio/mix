<?php

/*
Plugin Name: AM2 Detect Shortcodes
Plugin URI: am2studio.hr
Description: Detect shortcode from posts
Version: 0.0.1
Author: AM2
*/

class AM2_Detect_Shortcodes {

    private $post_type = 'post';

    public function __construct()
    {
        add_action('admin_menu', array($this, 'addPage'), 1000);

    }

    public function addPage() {
        add_menu_page(
            'Detect Shortcodes',
            'Detect Shortcodes',
            'read',
            'detect_shortcode', array($this, 'renderPage')
        );
    }

    public function renderPage() {
        $posts = $this->getPosts();
        if( !$posts ) {
            return false;
        }
        ?>
        <div style="width: 100%; background: white; float: left; padding: 50px; margin: 50px;">
            <h2>These posts might have a shortcode in them</h2>
            <ul>
        <?php
        foreach($posts as $post) {
            ?>
            <li>
                <a target="_blank" href="<?php echo get_edit_post_link($post->ID);?>">
                    <?php echo $post->post_title;?>
                </a>
            </li>
            <?php
        }
        ?>
            </ul>
        </div>
        <?php
    }

    private function getPosts() {
        global $wpdb;
        $sql = "SELECT * FROM $wpdb->posts 
                WHERE post_type = '{$this->post_type}' 
                AND post_content LIKE '%[%' 
                AND post_content LIKE '%]%' LIMIT 100";

        return $wpdb->get_results($sql);
    }

}
new AM2_Detect_Shortcodes();