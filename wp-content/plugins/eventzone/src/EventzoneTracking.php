<?php
namespace AM2;

if ( ! defined( 'ABSPATH' ) ) exit;

class EventzoneTracking {

    private $eventzone_object;
    private $table_name;

    public function __construct() {
        global $wpdb;
        $this->table_name = $wpdb->prefix.'eventzone_events_views';
        add_action( 'template_redirect', array($this, 'track') );

    }

    public function track() {
        if(is_singular('events')) {
            global $wpdb;
            global $post;
            $table_name = $wpdb->prefix.'eventzone_events_views';
            $sql = "SELECT * FROM {$table_name} WHERE post_id = {$post->ID}";
            $views = $wpdb->get_row($sql);

            if(empty($views)) {
                $this->insert($post->ID);
            }
            else {
                $this->update($views);
            }
        }
    }

    private function insert($post_id) {
        global $wpdb;
        $args = array(
            'post_id'       => $post_id,
            'first_viewed'  => date("Y-m-d H:i:s"),
            'last_viewed'   => date("Y-m-d H:i:s"),
            'pageviews'     => 1
        );
        $insert = $wpdb->insert($this->table_name, $args);
    }

    private function update($views) {
        global $wpdb;
        $args_where = array(
            'post_id' => $views->post_id
        );
        $new_pageviews = $views->pageviews + 1;
        $args = array(
            'pageviews'     => $new_pageviews,
            'last_viewed'   => date("Y-m-d H:i:s")
        );
        $update = $wpdb->update($this->table_name, $args, $args_where);
    }

}
?>