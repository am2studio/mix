<?php

namespace AM2;

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'EventzoneCPT' ) ) :
    class EventzoneCPT {
        /**
         * Constructor
         */
        public function __construct() {
            add_action( 'init', array( $this, 'registerPostTypes' ), 1000 );
        }
        /**
         * Setup the post type for CPTS.
         *
         * @since   1.0
         * @access  public
         */
        public function registerPostTypes() {
            $places_labels = apply_filters( 'eventzone_post_type_labels',
                array(
                    'name'                  => __( 'Places', 'eventzone' ),
                    'singular_name'         => _x( 'Place', 'Singular Name', 'eventzone' ),
                    'menu_name'             => _x( 'Places', 'Menu Name', 'eventzone' ),
                    'all_items'             => __( 'All Place Items', 'eventzone' ),
                    'name_admin_bar'        => _x( 'Example Place', 'Admin bar add new', 'eventzone'),
                    'add_new'               => __( 'Add New Place', 'eventzone' ),
                    'add_new_item'          => __( 'Add New Place', 'eventzone' ),
                    'edit'                  => __( 'Edit', 'eventzone' ),
                    'edit_item'             => __( 'Edit Place', 'eventzone' ),
                    'new_item'              => __( 'New Place', 'eventzone' ),
                    'view'                  => __( 'View Place', 'eventzone' ),
                    'view_item'             => __( 'View Place', 'eventzone' ),
                    'search_items'          => __( 'Search Places', 'eventzone' ),
                    'not_found'             => __( 'No Places found', 'eventzone' ),
                    'not_found_in_trash'    => __( 'No Places found in trash', 'eventzone' )
                )
            );
            $places_args = apply_filters( 'eventzone_places_args',
                array(
                    'labels'                => $places_labels,
                    'public'                => true,
                    'show_ui'               => true,
                    'capability_type'       => 'post',
                    'map_meta_cap'          => true,
                    'publicly_queryable'    => true,
                    'exclude_from_search'   => false,
                    'hierarchical'          => false,
                    'query_var'             => true,
                    'rewrite'               => true,
                    'supports'              => array('title', 'author', 'editor', 'custom-fields', 'excerpt', 'thumbnail'),
                    'has_archive'           => false,
                    'show_in_nav_menus'     => true,
                    'taxonomies'            => array('type', 'post_tag'),
                    'menu_position'         => null,
                    'menu_icon'             => 'dashicons-location'
                ),
                $places_labels
            );
            if(!post_type_exists('places')) {
                register_post_type('places', $places_args);
            }

            $events_labels = apply_filters( 'eventzone_events_args',
                array(
                    'name'                  => __( 'Events', 'eventzone' ),
                    'singular_name'         => _x( 'Event', 'Singular Name', 'eventzone' ),
                    'menu_name'             => _x( 'Events', 'Menu Name', 'eventzone' ),
                    'all_items'             => __( 'All Event Items', 'eventzone' ),
                    'name_admin_bar'        => _x( 'Example Event', 'Admin bar add new', 'eventzone'),
                    'add_new'               => __( 'Add New Event', 'eventzone' ),
                    'add_new_item'          => __( 'Add New Event', 'eventzone' ),
                    'edit'                  => __( 'Edit', 'eventzone' ),
                    'edit_item'             => __( 'Edit Event', 'eventzone' ),
                    'new_item'              => __( 'New Event', 'eventzone' ),
                    'view'                  => __( 'View Event', 'eventzone' ),
                    'view_item'             => __( 'View Event', 'eventzone' ),
                    'search_items'          => __( 'Search Events', 'eventzone' ),
                    'not_found'             => __( 'No Events found', 'eventzone' ),
                    'not_found_in_trash'    => __( 'No Events found in trash', 'eventzone' )
                )
            );
            $events_args = apply_filters( 'eventzone_post_type_args',
                array(
                    'labels'                => $events_labels,
                    'public'                => true,
                    'show_ui'               => true,
                    'capability_type'       => 'post',
                    'map_meta_cap'          => true,
                    'publicly_queryable'    => true,
                    'exclude_from_search'   => false,
                    'hierarchical'          => false,
                    'query_var'             => true,
                    'supports'              => array('title', 'author', 'editor', 'custom-fields', 'excerpt', 'thumbnail'),
                    'has_archive'           => true,
                    'rewrite'               => true,
                    'show_in_nav_menus'     => true,
                    'taxonomies'            => array('type', 'cities', 'post_tag', 'category'),
                    'menu_position'         => null,
                    'menu_icon'             => 'dashicons-format-audio'
                ),
                $events_labels
            );
            if(!post_type_exists('events')) {
                register_post_type( 'events', $events_args );
            }

        }
    }
endif;
?>