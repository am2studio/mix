<?php

namespace AM2;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


class Place
{

    public function get($post, $return = 'array')
    {

        if (!$post)
            return false;

        if( is_a($post, 'WP_Post') )
            $the_post = $post;

        else if( is_int($post) )
            $the_post = get_post($post);

        else
            return false;

        return [
            'name'      => $the_post->post_title,
            'info'      => $the_post->post_content,
            'location'  => [
                'city'          => $the_post->city,
                'address'       => $the_post->address,
                'geo_latitude'  => $the_post->geo_latitude,
                'geo_longitude' => $the_post->geo_longitude
            ]
        ];
    }
}