<?php

namespace AM2;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
if ( ! class_exists( 'EventzoneTax' ) ) :
    class EventzoneTax {

        public function __construct() {
            add_action( 'init', array( $this, 'register' ), 10 );
        }

        /**
         * Setup the post type for taxonomies.
         *
         * @since   1.0
         * @access  public
         */
        public function register() {
            $labels = array(
                'name' => _x( 'Place type', 'taxonomy general name' ),
                'singular_name' => _x( 'Place type', 'taxonomy singular name' ),
                'search_items' =>  __( 'Search items' ),
                'popular_items' => __( 'Popular items' ),
                'all_items' => __( 'All items' ),
                'parent_item' => null,
                'parent_item_colon' => null,
                'edit_item' => __( 'Edit item' ),
                'update_item' => __( 'Update item' ),
                'add_new_item' => __( 'Add new item' ),
                'new_item_name' => __( 'New item name' ),
                'separate_items_with_commas' => __( 'Separate items with commas' ),
                'add_or_remove_items' => __( 'Add/Remove items' ),
                'choose_from_most_used' => __( 'Choose from most used' ),
                'menu_name' => __( 'Place type' ),
            );
            register_taxonomy('place_type','places',array(
                'hierarchical' => true,
                'labels' => $labels,
                'show_ui' => true,
                'update_count_callback' => '_update_post_term_count',
                'query_var' => true,
                //'rewrite' => array( 'slug' => $GLOBALS['am2_settings']['slugs']['place_type'] )
                'public' => true,
                'rewrite' => false,
            ));

            $labels_event_tag = array(
                'name' => _x( 'Eventzone Event Tags', 'taxonomy general name' ),
                'singular_name' => _x( 'Eventzone Event Tag', 'taxonomy singular name' ),
                'search_items' =>  __( 'Search items' ),
                'popular_items' => __( 'Popular items' ),
                'all_items' => __( 'All items' ),
                'parent_item' => null,
                'parent_item_colon' => null,
                'edit_item' => __( 'Edit item' ),
                'update_item' => __( 'Update item' ),
                'add_new_item' => __( 'Add new item' ),
                'new_item_name' => __( 'New item name' ),
                'separate_items_with_commas' => __( 'Separate items with commas' ),
                'add_or_remove_items' => __( 'Add/Remove items' ),
                'choose_from_most_used' => __( 'Choose from most used' ),
                'menu_name' => __( 'Eventzone Event Tags' ),
            );
            register_taxonomy('eventzone_event_tags',array('events'),array(
                'hierarchical' => false,
                'labels' => $labels_event_tag,
                'show_ui' => true,
                'update_count_callback' => '_update_post_term_count',
                'query_var' => true,
                //'rewrite' => array( 'slug' => $GLOBALS['am2_settings']['slugs']['place_type'] )
                'public' => true,
                'rewrite' => false,
            ));

            $labels_place_tag = array(
                'name' => _x( 'Eventzone Place Tags', 'taxonomy general name' ),
                'singular_name' => _x( 'Eventzone Place Tags', 'taxonomy singular name' ),
                'search_items' =>  __( 'Search items' ),
                'popular_items' => __( 'Popular items' ),
                'all_items' => __( 'All items' ),
                'parent_item' => null,
                'parent_item_colon' => null,
                'edit_item' => __( 'Edit item' ),
                'update_item' => __( 'Update item' ),
                'add_new_item' => __( 'Add new item' ),
                'new_item_name' => __( 'New item name' ),
                'separate_items_with_commas' => __( 'Separate items with commas' ),
                'add_or_remove_items' => __( 'Add/Remove items' ),
                'choose_from_most_used' => __( 'Choose from most used' ),
                'menu_name' => __( 'Eventzone Place Tags' ),
            );
            register_taxonomy('eventzone_place_tags',array('places'),array(
                'hierarchical' => false,
                'labels' => $labels_place_tag,
                'show_ui' => true,
                'update_count_callback' => '_update_post_term_count',
                'query_var' => true,
                //'rewrite' => array( 'slug' => $GLOBALS['am2_settings']['slugs']['place_type'] )
                'public' => true,
                'rewrite' => false,
            ));
        }
    }
endif;
?>