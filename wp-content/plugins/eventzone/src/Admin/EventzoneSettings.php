<?php

namespace AM2\Admin;

if ( ! defined( 'ABSPATH' ) ) exit;

class EventzoneSettings {
    /**
     * The single instance of eventzone_settings.
     * @var     object
     * @access  private
     * @since   1.0.0
     */
    private static $_instance = null;
    /**
     * Prefix for plugin settings.
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $base = '';
    /**
     * Available settings for plugin.
     * @var     array
     * @access  public
     * @since   1.0.0
     */
    public $settings = array();
    public function __construct () {
        $this->base = 'eventzone_';
        // Initialise settings
        add_action( 'init', array( $this, 'init' ), 11 );
        // Register plugin settings
        add_action( 'admin_init' , array( $this, 'registerSettings' ) );

        add_action( 'wp_ajax_eventzone_event_tag_mapping', array($this, 'eventzoneEventTagMapping') );
    }
    /**
     * Initialise settings
     * @return void
     */
    public function init () {
        $this->settings = $this->settingsFields();
    }

    public function getSettingsFields (){
        return $this->settings;
    }
    /**
     * Build settings fields
     * @return array Fields to be displayed on settings page
     */
    private function settingsFields() {
//        $settings['standard'] = array(
//            'title'                 => __( 'Map Categories', 'wordpress-plugin-template' ),
//            'description'           => __( 'Here you can map categories from eventzone', 'wordpress-plugin-template' ),
//            'fields'                => array(
//                array(
//                    'id'            => 'event_tag',
//                    'label'         => __( 'Eventzone Categories', 'wordpress-plugin-template' ),
//                    'description'   => __( 'Categories that are not mapped', 'wordpress-plugin-template' ),
//                    'type'          => 'select',
//                    //'options'       => array( 'drupal' => 'Drupal', 'joomla' => 'Joomla', 'wordpress' => 'WordPress' ),
//                    'options'       => $this->unmapped_event_tags(),
//                    'default'       => 'wordpress'
//                ),
//                array(
//                    'id'            => 'internal_category',
//                    'label'         => __( 'The Mix Categories', 'wordpress-plugin-template' ),
//                    'description'   => __( 'Internal Categories', 'wordpress-plugin-template' ),
//                    'type'          => 'select',
//                    //'options'       => array( 'drupal' => 'Drupal', 'joomla' => 'Joomla', 'wordpress' => 'WordPress' ),
//                    'options'       => $this->unmapped_event_tags(),
//                    'default'       => 'wordpress'
//                ),
//            )
//        );
//        $settings['extra'] = array(
//            'title'                 => __( 'Extra', 'wordpress-plugin-template' ),
//            'description'           => __( 'These are some extra input fields that maybe aren\'t as common as the others.', 'wordpress-plugin-template' ),
//            'fields'                => array(
//                array(
//                    'id'            => 'number_field',
//                    'label'         => __( 'A Number' , 'wordpress-plugin-template' ),
//                    'description'   => __( 'This is a standard number field - if this field contains anything other than numbers then the form will not be submitted.', 'wordpress-plugin-template' ),
//                    'type'          => 'number',
//                    'default'       => '',
//                    'placeholder'   => __( '42', 'wordpress-plugin-template' )
//                ),
//                array(
//                    'id'            => 'colour_picker',
//                    'label'         => __( 'Pick a colour', 'wordpress-plugin-template' ),
//                    'description'   => __( 'This uses WordPress\' built-in colour picker - the option is stored as the colour\'s hex code.', 'wordpress-plugin-template' ),
//                    'type'          => 'color',
//                    'default'       => '#21759B'
//                ),
//                array(
//                    'id'            => 'an_image',
//                    'label'         => __( 'An Image' , 'wordpress-plugin-template' ),
//                    'description'   => __( 'This will upload an image to your media library and store the attachment ID in the option field. Once you have uploaded an imge the thumbnail will display above these buttons.', 'wordpress-plugin-template' ),
//                    'type'          => 'image',
//                    'default'       => '',
//                    'placeholder'   => ''
//                ),
//                array(
//                    'id'            => 'multi_select_box',
//                    'label'         => __( 'A Multi-Select Box', 'wordpress-plugin-template' ),
//                    'description'   => __( 'A standard multi-select box - the saved data is stored as an array.', 'wordpress-plugin-template' ),
//                    'type'          => 'select_multi',
//                    'options'       => array( 'linux' => 'Linux', 'mac' => 'Mac', 'windows' => 'Windows' ),
//                    'default'       => array( 'linux' )
//                )
//            )
//        );
        $settings = array();
        $settings = apply_filters( 'eventzone_settings_fields', $settings );
        return $settings;
    }
    /**
     * Register plugin settings
     * @return void
     */
    public function registerSettings () {
        if ( is_array( $this->settings ) ) {
            // Check posted/selected tab
            $current_section = '';
            if ( isset( $_POST['tab'] ) && $_POST['tab'] ) {
                $current_section = $_POST['tab'];
            } else {
                if ( isset( $_GET['tab'] ) && $_GET['tab'] ) {
                    $current_section = $_GET['tab'];
                }
            }
            foreach ( $this->settings as $section => $data ) {
                if ( $current_section && $current_section != $section ) continue;
                // Add section to page
                add_settings_section( $section, $data['title'], array( $this, 'settings_section' ), 'eventzone_settings' );
                foreach ( $data['fields'] as $field ) {
                    // Validation callback for field
                    $validation = '';
                    if ( isset( $field['callback'] ) ) {
                        $validation = $field['callback'];
                    }
                    // Register field
                    $option_name = $this->base . $field['id'];
                    register_setting( 'eventzone_settings', $option_name, $validation );
                    // Add field to page
                    add_settings_field( $field['id'], $field['label'], array( $this, 'display_field' ), 'eventzone_settings', $section, array( 'field' => $field, 'prefix' => $this->base ) );
                }
                if ( ! $current_section ) break;
            }
        }
    }
    public function settings_section( $section ) {
        $html = '<p> ' . $this->settings[ $section['id'] ]['description'] . '</p>' . "\n";
        echo $html;
    }
    /**
     * Generate HTML for displaying fields
     * @param  array   $field Field data
     * @param  boolean $echo  Whether to echo the field HTML or return it
     * @return void
     */
    public function display_field ( $data = array(), $post = false, $echo = true ) {
        // Get field info
        if ( isset( $data['field'] ) ) {
            $field = $data['field'];
        } else {
            $field = $data;
        }
        // Check for prefix on option name
        $option_name = '';
        if ( isset( $data['prefix'] ) ) {
            $option_name = $data['prefix'];
        }
        // Get saved data
        $data = '';
        if ( $post ) {
            // Get saved field data
            $option_name .= $field['id'];
            $option = get_post_meta( $post->ID, $field['id'], true );
            // Get data to display in field
            if ( isset( $option ) ) {
                $data = $option;
            }
        } else {
            // Get saved option
            $option_name .= $field['id'];
            $option = get_option( $option_name );
            // Get data to display in field
            if ( isset( $option ) ) {
                $data = $option;
            }
        }
        // Show default data if no option saved and default is supplied
        if ( $data === false && isset( $field['default'] ) ) {
            $data = $field['default'];
        } elseif ( $data === false ) {
            $data = '';
        }
        $html = '';
        switch( $field['type'] ) {
            case 'text':
            case 'url':
            case 'email':
                $html .= '<input id="' . esc_attr( $field['id'] ) . '" type="text" name="' . esc_attr( $option_name ) . '" placeholder="' . esc_attr( $field['placeholder'] ) . '" value="' . esc_attr( $data ) . '" />' . "\n";
                break;
            case 'password':
            case 'number':
            case 'hidden':
                $min = '';
                if ( isset( $field['min'] ) ) {
                    $min = ' min="' . esc_attr( $field['min'] ) . '"';
                }
                $max = '';
                if ( isset( $field['max'] ) ) {
                    $max = ' max="' . esc_attr( $field['max'] ) . '"';
                }
                $html .= '<input id="' . esc_attr( $field['id'] ) . '" type="' . esc_attr( $field['type'] ) . '" name="' . esc_attr( $option_name ) . '" placeholder="' . esc_attr( $field['placeholder'] ) . '" value="' . esc_attr( $data ) . '"' . $min . '' . $max . '/>' . "\n";
                break;
            case 'text_secret':
                $html .= '<input id="' . esc_attr( $field['id'] ) . '" type="text" name="' . esc_attr( $option_name ) . '" placeholder="' . esc_attr( $field['placeholder'] ) . '" value="" />' . "\n";
                break;
            case 'textarea':
                $html .= '<textarea id="' . esc_attr( $field['id'] ) . '" rows="5" cols="50" name="' . esc_attr( $option_name ) . '" placeholder="' . esc_attr( $field['placeholder'] ) . '">' . $data . '</textarea><br/>'. "\n";
                break;
            case 'checkbox':
                $checked = '';
                if ( $data && 'on' == $data ) {
                    $checked = 'checked="checked"';
                }
                $html .= '<input id="' . esc_attr( $field['id'] ) . '" type="' . esc_attr( $field['type'] ) . '" name="' . esc_attr( $option_name ) . '" ' . $checked . '/>' . "\n";
                break;
            case 'checkbox_multi':
                foreach ( $field['options'] as $k => $v ) {
                    $checked = false;
                    if ( in_array( $k, (array) $data ) ) {
                        $checked = true;
                    }
                    $html .= '<label for="' . esc_attr( $field['id'] . '_' . $k ) . '" class="checkbox_multi"><input type="checkbox" ' . checked( $checked, true, false ) . ' name="' . esc_attr( $option_name ) . '[]" value="' . esc_attr( $k ) . '" id="' . esc_attr( $field['id'] . '_' . $k ) . '" /> ' . $v . '</label> ';
                }
                break;
            case 'radio':
                foreach ( $field['options'] as $k => $v ) {
                    $checked = false;
                    if ( $k == $data ) {
                        $checked = true;
                    }
                    $html .= '<label for="' . esc_attr( $field['id'] . '_' . $k ) . '"><input type="radio" ' . checked( $checked, true, false ) . ' name="' . esc_attr( $option_name ) . '" value="' . esc_attr( $k ) . '" id="' . esc_attr( $field['id'] . '_' . $k ) . '" /> ' . $v . '</label> ';
                }
                break;
            case 'select':
                $html .= '<select name="' . esc_attr( $option_name ) . '" id="' . esc_attr( $field['id'] ) . '">';
                foreach ( $field['options'] as $k => $v ) {
                    $selected = false;
                    if ( $k == $data ) {
                        $selected = true;
                    }
                    $html .= '<option ' . selected( $selected, true, false ) . ' value="' . esc_attr( $k ) . '">' . $v . '</option>';
                }
                $html .= '</select> ';
                break;
            case 'select_multi':
                $html .= '<select name="' . esc_attr( $option_name ) . '[]" id="' . esc_attr( $field['id'] ) . '" multiple="multiple">';
                foreach ( $field['options'] as $k => $v ) {
                    $selected = false;
                    if ( in_array( $k, (array) $data ) ) {
                        $selected = true;
                    }
                    $html .= '<option ' . selected( $selected, true, false ) . ' value="' . esc_attr( $k ) . '">' . $v . '</option>';
                }
                $html .= '</select> ';
                break;
            case 'image':
                $image_thumb = '';
                if ( $data ) {
                    $image_thumb = wp_get_attachment_thumb_url( $data );
                }
                $html .= '<img id="' . $option_name . '_preview" class="image_preview" src="' . $image_thumb . '" /><br/>' . "\n";
                $html .= '<input id="' . $option_name . '_button" type="button" data-uploader_title="' . __( 'Upload an image' , 'wordpress-plugin-template' ) . '" data-uploader_button_text="' . __( 'Use image' , 'wordpress-plugin-template' ) . '" class="image_upload_button button" value="'. __( 'Upload new image' , 'wordpress-plugin-template' ) . '" />' . "\n";
                $html .= '<input id="' . $option_name . '_delete" type="button" class="image_delete_button button" value="'. __( 'Remove image' , 'wordpress-plugin-template' ) . '" />' . "\n";
                $html .= '<input id="' . $option_name . '" class="image_data_field" type="hidden" name="' . $option_name . '" value="' . $data . '"/><br/>' . "\n";
                break;
            case 'color':
                ?><div class="color-picker" style="position:relative;">
                <input type="text" name="<?php esc_attr_e( $option_name ); ?>" class="color" value="<?php esc_attr_e( $data ); ?>" />
                <div style="position:absolute;background:#FFF;z-index:99;border-radius:100%;" class="colorpicker"></div>
                </div>
                <?php
                break;

            case 'editor':
                wp_editor($data, $option_name, array(
                    'textarea_name' => $option_name
                ) );
                break;
        }
        switch( $field['type'] ) {
            case 'checkbox_multi':
            case 'radio':
            case 'select_multi':
                $html .= '<br/><span class="description">' . $field['description'] . '</span>';
                break;
            default:
                if ( ! $post ) {
                    $html .= '<label for="' . esc_attr( $field['id'] ) . '">' . "\n";
                }
                $html .= '<span class="description">' . $field['description'] . '</span>' . "\n";
                if ( ! $post ) {
                    $html .= '</label>' . "\n";
                }
                break;
        }
        if ( ! $echo ) {
            return $html;
        }
        echo $html;
    }

    public function unmapped_event_tags() {
        $terms = get_terms( array(
            'taxonomy' => 'eventzone_event_tags',
            'hide_empty' => false,
        ) );
        $terms_array = array();
        $terms_mapped = get_option('eventzone_terms_mapped');
        if(!empty($terms)) {
            foreach($terms as $term) {
                if(!empty($terms_mapped)) {
                    if(array_key_exists($term->slug, $terms_mapped)) {
                        continue;
                    }
                }

                $terms_array[$term->slug] = $term->name;
            }
        }

        return $terms_array;
    }


    public function internal_categories($slug) {
        $terms = get_terms( array(
            'taxonomy' => 'category',
            'hide_empty' => false,
        ) );
        $terms_array = array();
        if(!empty($terms)) {
            foreach($terms as $term) {
                $terms_array['terms'][$term->slug] = $term->name;
            }
        }
        $terms_mapped = get_option('eventzone_terms_mapped');

        if(!empty($terms_mapped)) {
            if( array_key_exists( $slug, $terms_mapped ) ) {
                $terms_array['selected']  = $terms_mapped[$slug];
            }
        }
        return $terms_array;
    }

    public function add_relation($postdata) {
        $option = get_option('eventzone_terms_mapped');

        $option[$postdata['eventzone_event_tag']] = $postdata['mix_category'];

        update_option('eventzone_terms_mapped', $option);
    }

    /**
     * eventzone_map_events_without_category
     *
     * Gets all events without a categories and checks against mapped array if there is a need to add one
     *
     * @since 1.0.0
     * @return
     */
    public function mapEventsWithoutCategory() {
        $mapped = get_option(eventzone()->mapped_option_name);
        $events_without_categories = new WP_Query(array(
            'post_type'         => 'events',
            'posts_per_page'     => -1,
            'category__not_in'  => get_terms('category', array(
                'fields'        => 'ids'
            ))
        ));
        foreach($events_without_categories->posts as $event_without_category) {

            $eventzone_tags = wp_get_post_terms($event_without_category->ID, 'eventzone_event_tags');
            if( empty($eventzone_tags) ) {
                continue;
            }

            foreach($eventzone_tags as $eventzone_tag) {
                if( array_key_exists($eventzone_tag->slug, $mapped) ) {
                    $category_slug = $mapped[$eventzone_tag->slug];
                    $category = get_term_by('slug', $category_slug, 'category');
                    wp_set_post_terms($event_without_category->ID, $category->term_id, 'category', true);
                }
            }


        }
    }

    /**
     * eventzone_event_tag_mapping
     *
     * Maps eventzone tags into categories as an array key (eventzone_tag) => value (category)
     * and stores into options table with key eventzone()->mapped_option_name
     *
     * @since 1.0.0
     * @return
     */
    public function eventzoneEventTagMapping() {

        if( !isset($_POST['pairs']) ) {
            return false;
        }

        $final_array = array();

        foreach( $_POST['pairs'] as $pair ) {
            foreach( $pair as $key => $value ) {
                $final_array[$key] = $value;
            }
        }
        if(empty($final_array)) {
            wp_send_json_error(array('message' => 'Something went wrong'));
            die;
        }
        update_option(eventzone()->mapped_option_name, $final_array);
        $this->mapEventsWithoutCategory();
        wp_send_json_success();
    }
    /**
     * Main WordPress_Plugin_Template_Settings Instance
     *
     * Ensures only one instance of WordPress_Plugin_Template_Settings is loaded or can be loaded.
     *
     * @since 1.0.0
     * @static
     * @see WordPress_Plugin_Template()
     * @return EventzoneSettings instance
     */
    public static function instance()
    {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getCity() {
        return get_option('eventzone_city');
    }

    public function saveCity($city) {
        return update_option('eventzone_city', $city );
    }
}