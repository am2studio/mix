<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @since      1.0.0
 *
 */

if( isset($_POST['city']) ) {
    $settings_api->saveCity($_POST['city']);
}

?>
<div style="background-color:#985DC4;min-height:120px;margin-left:-20px;">
    <h1 style="color:#fff;margin:0;line-height:120px;margin-left:25px;"><?php _e('Eventzone','eventzone'); ?></h1>
</div>
<div style="background-color:#fff;min-height:60px;margin-left:-20px;">
    <h2 style="color:#222;margin:0;line-height:60px;margin-left:25px;"><?php _e('Eventzone Settings Page.','eventzone'); ?></h2>

</div>
<div class="wrap">
    <div id="poststuff">
        <div id="post-body" class="metabox-holder columns-2" style="margin-right:0;">
            <div id="postbox-container-2" class="postbox-container">
                <form action="" method="post">
                    <label for="city">Enter a city</label>
                    <input type="text" name="city" value="<?php echo $settings_api->getCity();?>">
                    <input type="submit" class="button button-primary button-large">
                </form>
            </div>
        </div>
    </div>
</div>