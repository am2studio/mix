<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @since      1.0.0
 *
 */
?>
<div style="background-color:#985DC4;min-height:120px;margin-left:-20px;">
    <h1 style="color:#fff;margin:0;line-height:120px;margin-left:25px;"><?php _e('Eventzone','eventzone'); ?></h1>
</div>
<div style="background-color:#fff;min-height:60px;margin-left:-20px;">
    <h2 style="color:#222;margin:0;line-height:60px;margin-left:25px;"><?php _e('Eventzone Map Categories.','eventzone'); ?></h2>
</div>
<div class="wrap">
    <div id="poststuff">
        <div id="post-body" class="metabox-holder columns-2" style="margin-right:0;">
            <div id="postbox-container-2" class="postbox-container">

                <?php
                $tax_name = eventzone()->default_tax_to_map_into;
                $terms = get_terms( $tax_name, array(
                    'hide_empty' => false,
                ) );
                //print_r($terms);
                $already_mapped  = get_option('eventzone_terms_mapped');
                $unmapped = $settings_api->unmapped_event_tags();
                ?>
                <div style="clear: both; width: 100%;">
                    <a id="mapthis" href="#" class="button button-primary button-large">Map</a>
                </div>
                <div class="draggable-wrapper">


                    <div id="unmapped" class="container" data-role="drag-drop-container">
                        <h2>Unmapped Eventzone Tags</h2>
                        <?php
                        foreach($unmapped as $key => $value) {
                            ?>
                            <div draggable="true" id="<?php echo $key;?>" data-term="<?php echo $key;?>"><?php echo $value;?></div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php

                    foreach($terms as $term) {
                        if($term->slug == 'uncategorized') {
                            continue;
                        }
                        ?>
                        <div id="<?php echo $term->slug;?>" class="container" data-role="drag-drop-container">
                            <h2><?php echo get_term_by('slug', $term->slug, eventzone()->default_tax_to_map_into)->name;?></h2>
                            <?php
                            if(!empty($already_mapped)) {
                                $mapped_slugs = array_keys( $already_mapped, $term->slug );
                                //var_dump($mapped_slugs);
                                foreach($mapped_slugs as $map) {
                                    $the_mapped_term = get_term_by('slug', $map, 'eventzone_event_tags');

                                   if($the_mapped_term) {
                                       echo '<div draggable="true" id="'.$the_mapped_term->slug.'" data-term="'.$the_mapped_term->slug.'">'.$the_mapped_term->name.'</div>';
                                   }

                                }
                            }

                            ?>
                        </div>
                        <?php
                    }
                    ?>

                </div>

            </div>
        </div>
    </div>
    <style type="text/css">
        /**
         * Drag and Drop Basic
         **/
        [draggable="true"] {
            /*
             To prevent user selecting inside the drag source
            */
            user-select: none;
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
            cursor: move;
        }

        .draggable-wrapper {
            display: block;
        }
        .draggable-wrapper .container {
            min-height: 400px;
            width: 300px;
            margin: 15px;
            border: 2px solid #CCC;
            display: block;
            float: left;
        }
        .container div {
            height: 40px;
            background: #0073aa;
            border: 1px solid #fff;
            margin: 5px;
            color:#fff;
        }

    </style>
    <script type="text/javascript">
        /**
         * Drag and Drop multiple elements in multiple sources
         **/

        let sourceContainerId = ''

        // Allow multiple draggable items
        var dragSources = document.querySelectorAll('[draggable="true"]')
        dragSources.forEach(dragSource => {
            dragSource.addEventListener('dragstart', dragStart)
        })

        // Allow multiple dropped targets
        let dropTargets = document.querySelectorAll('[data-role="drag-drop-container"]')
        dropTargets.forEach(dropTarget => {
            dropTarget.addEventListener('drop', dropped)
            dropTarget.addEventListener('dragenter', cancelDefault)
            dropTarget.addEventListener('dragover', cancelDefault)
        })

        function cancelDefault (e) {
            e.preventDefault()
            e.stopPropagation()
            return false
        }

        function dragStart (e) {
            e.dataTransfer.setData('text/plain', e.target.id)
            sourceContainerId = this.parentElement.id
            //console.log('sourceContainerId', sourceContainerId)
        }

        function dropped (e) {
            // execute function only when target container is different from source container
            if (this.id !== sourceContainerId) {
                cancelDefault(e);
                let id = e.dataTransfer.getData('text/plain');
                //console.log(id);
                e.target.appendChild(document.querySelector('#' + id))
            }
        }
        // End of Drag and Drop Basic
    </script>
    <script>
        jQuery(document).ready(function($){
            var pairs = [];
            var final_array = [];
            $('#mapthis').on('click', function(e){
                e.preventDefault();
                $('.container').not('#unmapped').each(function(){
                    var internalTax = $(this).attr('id');
                    $(this).find('div').each(function(){
                        var externalTax = $(this).attr('id');
                        pairs.push({
                            [externalTax] : internalTax
                        });
                        //pairs[externalTax] = internalTax;
                    });
                });

                $.ajax({
                    type: 'POST',
                    url: eventzone.adminAjax,
                    data: {
                        action: 'eventzone_event_tag_mapping',
                        pairs: pairs
                    },
                    success: function(data) {
                        if(data.success === true) {

                            location.reload();

                        }
                        if( typeof data.data.selected !== 'undefined' ) {
                        }
                    },
                    complete: function(xhr, status) {

                    },
                    always: function() {

                    }
                });
            });
        });
    </script>