<?php

namespace AM2\Admin;
use AM2\Admin\EventzoneSettings;

if ( ! defined( 'ABSPATH' ) ) exit;

class EventzoneMenu {
    private $settings;

    public function __construct() {
        // Add submenu items
        add_action( 'admin_menu', array( $this, 'addMenuItem') );
        // Add links under plugin page.
        add_filter( 'plugin_action_links_' . EVENTZONE_BASENAME , array( $this, 'addSettingsLink' ) );

    }


    /**
     * Define submenu page under Woocommerce Page
     *
     * @since 1.0.0
     */
    public function addMenuItem() {
        // change to add_options_page to add it under general options.
        add_menu_page( __( 'Eventzone', 'wordpress-plugin-template' ) , __( 'Eventzone', 'wordpress-plugin-template' ) , 'manage_options' , 'eventzone_settings' ,  array( $this, 'settingsPage' ) );
        add_submenu_page(
            'eventzone_settings',
            __( 'Pick City', 'eventzone' ),
            __( 'Pick City', 'eventzone' ),
            'manage_options',
            'eventzone_city',
            array( $this, 'settingsCityPage' )
        );
    }

    /**
     * Load settings page content
     * @return void
     */
    public function settingsPage() {

        $settings_api = new EventzoneSettings();
        $html = '';
        include( EVENTZONE_ABSPATH . 'src/Admin/partials/eventzone-page-view.php' );
    }

    /**
     * Load settings page content
     * @return void
     */
    public function settingsCityPage() {

        $settings_api = new EventzoneSettings();
        $html = '';
        include( EVENTZONE_ABSPATH . 'src/Admin/partials/eventzone-city.php' );
    }


    /**
     * Plugin Settings Link on plugin page
     *
     * @since 		1.0.0
     */
    function addSettingsLink( $links ) {
        $mylinks = array(
            '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=gens_raf' ) . '">Settings</a>',
        );
        return array_merge( $links, $mylinks );
    }
}