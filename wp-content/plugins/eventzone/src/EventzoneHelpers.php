<?php

namespace AM2;

trait EventzoneHelpers {

    public static function insertAttachmentFromUrl($url, $post_id, $alternative_title = '') {
        if(empty($url) || empty($post_id))
            return false;
        if( !class_exists( 'WP_Http' ) )
            include_once( ABSPATH . WPINC . '/class-http.php' );

        $http = new \WP_Http();
        $response = $http->request( $url );
        if( is_wp_error($response) or $response['response']['code'] != 200 ) {
            // AM2_Debug::message('Error getting image!');
            return false;
        }
        $url = strstr($url, '?', true) ? : $url;
        $file_name = basename($url);
        $file_extension = pathinfo($url, PATHINFO_EXTENSION);
        if(empty($file_extension)) {
            $file_extension = 'jpg';
        }

        if(!empty($alternative_title)){
            $file_name = sanitize_title($alternative_title).'.'.$file_extension;
        }

        $upload = wp_upload_bits( $file_name, null, $response['body'] );
        if( !empty( $upload['error'] ) ) {
            // AM2_Debug::message('Error uploading image!');
            return false;
        }

        $image_dimensions = getimagesize($upload['url']);
        $image_size = filesize($upload['file']);
        $image_size_in_mb = $image_size / 1000000;


        if($image_dimensions[0] > 8000 || $image_dimensions[1] > 8000) {
            // AM2_Debug::message('image too big, skipping');
            // AM2_Errors::add(__FILE__, 'image is too big. Width: ' . $image_dimensions[0] . 'Height: ' . $image_dimensions[1] . ' / Image was supposed to be uploaded at post_id' . $post_id);
            return false;
        }
        $file_path = $upload['file'];
        $file_name = basename( $file_path );
        $file_type = wp_check_filetype( $file_name, null );
        $attachment_title = sanitize_file_name( pathinfo( $file_name, PATHINFO_FILENAME ) );
        if(!empty($alternative_title)){
            $attachment_title = $alternative_title;
        }
        $wp_upload_dir = wp_upload_dir();

        $post_info = array(
            'guid'            => $wp_upload_dir['url'] . '/' . $file_name,
            'post_mime_type'  => $file_type['type'],
            'post_title'      => $attachment_title,
            'post_content'    => '',
            'post_status'     => 'inherit',
            'post_author'     => 1
        );
        $attach_id = wp_insert_attachment( $post_info, $file_path, $post_id );
        require_once( ABSPATH . 'wp-admin/includes/image.php' );

        // Define attachment metadata
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file_path );
        // Assign metadata to attachment
        wp_update_attachment_metadata( $attach_id,  $attach_data );

        return $attach_id;
    }

    public static function getFeaturedImageSizes($post_id) {

        $featured_image = (int) get_post_meta($post_id, '_thumbnail_id', true);
        if( !$featured_image ) {
            return false;
        }

        $att_metadata = wp_get_attachment_metadata($featured_image);

        $att_metadata['sizes']['full'] = array( 'file' => $att_metadata['file'], 'width' => $att_metadata['width'], 'height' => $att_metadata['height'] );
        foreach($att_metadata['sizes'] as $size => $size_info) {
            $src_array = wp_get_attachment_image_src( $featured_image, $size );
            $att_metadata['sizes'][$size]['file'] = $src_array[0];
            unset($att_metadata['sizes'][$size]['mime-type']);
        }
        $all_registered_sizes = self::getImageSizes();
        foreach ($all_registered_sizes as $key => $value) {
            if(!array_key_exists($key, $att_metadata['sizes'])) {
                $att_metadata['sizes'][$key] = $att_metadata['sizes']['full'];
            }
        }
        return $att_metadata['sizes'];
    }

    public static function getImageSizes() {
        $image_sizes = get_intermediate_image_sizes();

        foreach ( $image_sizes as $_size ) {
            if ( in_array( $_size, array('thumbnail', 'medium', 'medium_large', 'large') ) ) {
                $sizes[ $_size ]['width']  = get_option( "{$_size}_size_w" );
                $sizes[ $_size ]['height'] = get_option( "{$_size}_size_h" );
                $sizes[ $_size ]['crop']   = (bool) get_option( "{$_size}_crop" );
            } elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
                $sizes[ $_size ] = array(
                    'width'  => $_wp_additional_image_sizes[ $_size ]['width'],
                    'height' => $_wp_additional_image_sizes[ $_size ]['height'],
                    'crop'   => $_wp_additional_image_sizes[ $_size ]['crop'],
                );
            }
        }

        return $sizes;
    }

}