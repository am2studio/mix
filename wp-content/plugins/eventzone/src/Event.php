<?php

namespace AM2;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

use AM2\Place;
use AM2\EventzoneHelpers;

class Event {

    private $fillable = [
        'post_title'            => 'posts',
        'post_content'          => 'posts',
        'event_start'           => 'postmeta',
        'event_end'             => 'postmeta',
        'place'                 => 'places',
        'organizer_name'        => 'postmeta',
        'featured_event'        => 'postmeta',
        'pinned_event'          => 'postmeta',
        'submission_details'    => 'submission_details',
        'image'                 => 'featured_image'
    ];

    private $fillable_map = [
        'post_title'        => 'name',
        'post_content'      => 'info',
        'organizer_name'    => 'organizer',
        'featured_event'    => 'is_featured',
        'pinned_event'      => 'is_pinned'
    ];

    public $filter_args;

    public $query_args;

    public $max_num_pages;

    public $event_query;

    public function __construct()
    {

        add_action('pre_get_posts', array($this, 'eventsPreGetPosts'));

    }

    public function eventsPreGetPosts( $query ) {
        if( is_admin() ) return $query;

        if( $query->is_main_query() && in_array( $query->get( 'post_type' ), [ 'events' ] ) ) {
            $query->set('posts_per_page', 6);

            $meta_query = array(
                'relation'      => 'AND',
                'event_end_clause' => array(
                    'key'       => 'event_end',
                    'value'     => date('Y-m-d H:i:s'),
                    'compare'   => '>=',
                    'type'      => 'DATETIME'
                ),
                'event_start_clause' => array(
                    'key'       => 'event_start',
                    'compare'   => 'EXISTS',
                ),
                'featured_clause' => array(
                    'key'       => 'featured_event',
                    'compare'   => 'exists'
                )

            );
            $query->set('order', 'ASC');
            $query->set('orderby', ['featured_clause' => 'DESC', 'event_start_clause' => 'ASC']);
            $query->set('meta_key', 'event_start');
            $query->set('meta_query', $meta_query);

            if( !empty($_GET['filter-start']) && !empty($_GET['filter-end']) ) {

                $start = substr($_GET['filter-start'], 0, strpos($_GET['filter-start'], "T")) . '00:00:00';
                $end = substr($_GET['filter-end'], 0, strpos($_GET['filter-end'], "T")) . '23:59:59';



                $date_range = array( 
                    date('Y-m-d H:i:s', strtotime($start)),
                    date('Y-m-d H:i:s', strtotime($end))
                );

                if( count($date_range) === 2 ) {

                    $meta_query = array(
                        'relation'  => 'AND',
                        array(
                            'key'       => 'event_start',
                            'value'     => $date_range,
                            'compare'   => 'BETWEEN',
                            'type'      => 'DATE'
                        )
                    );

                    $this->filter_args['filter-start'] = $_GET['filter-start'];
                    $this->filter_args['filter-end'] = $_GET['filter-end'];

                    $query->set('meta_query', $meta_query);
                }


            }

            if( isset($_GET['filter-category']) ) {
                $category_slug = $_GET['filter-category'];

                if($category_slug !== 'all' && $category_slug !== 'uncategorized') {
                    $category = get_term_by('slug', $category_slug, 'category');
                    $query->set('cat', $category->term_id);
                }

                $this->filter_args['filter-category'] = $_GET['filter-category'];

            }

            if( isset($_GET['filter-search']) && !empty($_GET['filter-search'])) {
                $search_query =  filter_input( INPUT_GET, 'filter-search', FILTER_SANITIZE_STRING );
                $query->set( 's', $search_query );
                $this->filter_args['filter-search'] = $_GET['filter-search'];
            }





//            add_filter('posts_join_paged',array($this, 'joinEventViews'));
//            add_filter('posts_orderby', array($this, 'orderEventsByViews'));
        }
    }

    public function orderEventsByViews($orderby_statement) {
        global $wpdb;
        $orderby_statement = " MAX(CAST($wpdb->postmeta.meta_value AS CHAR)) DESC, (ezi.pageviews) DESC";
        return $orderby_statement;
    }

    public function joinEventViews($join_paged_statement) {
        global $wpdb;
        $join_paged_statement.= "LEFT JOIN {$wpdb->prefix}eventzone_events_views ezi ON ezi.post_id = {$wpdb->prefix}posts.ID";
        return $join_paged_statement;
    }

    public function buildArgs($page = 1) {

        $this->query_args = array(
            'post_type'         => 'events',
            'posts_per_page'    => 6,
            'post_status'       => array('publish'),
            'orderby'           => 'meta_value',
            'order'             => 'ASC',
            'paged'             => $page,
            'meta_key'          => 'event_start',
            'meta_query'        => array(
                array(
                    'key'       => 'event_end',
                    'value'     => date('Y-m-d H:i:s'),
                    'compare'   => '>=',
                    'type'      => 'DATETIME'
                )

            )
        );



        if( isset($_POST['filter-date']) && !empty($_POST['filter-date']) ) {

            $date_range = array_map('trim', explode('-', $_POST['filter-date']));
            if( count($date_range) !== 2 ) {
                return false;
            }

            $date_range[0] = date('Y-m-d H:i:s', strtotime( $date_range[0] . ' 00:00:00') );
            $date_range[1] = date('Y-m-d H:i:s', strtotime( $date_range[1] . ' 23:59:59') );

            $this->filter_args['filter-date'] = $date_range;

            $this->query_args['meta_query'] = array(
                'relation'  => 'AND',
                array(
                    'key'       => 'event_start',
                    'value'     => $date_range,
                    'compare'   => 'BETWEEN',
                    'type'      => 'DATE'
                )
            );

        }

        if( isset($_POST['filter-category']) ) {
            $category_slug = $_POST['filter-category'];

            if($category_slug !== 'all' && $category_slug !== 'uncategorized') {
                $category = get_term_by('slug', $category_slug, 'category');
                $this->query_args['cat'] = $category->term_id;
            }

            $this->filter_args['filter-category'] = $_POST['filter-category'];

        }

        if( isset($_POST['filter-search']) ) {
            $this->query_args['s'] = $_POST['filter-search'];
            $this->filter_args['filter-search'] = $_POST['filter-search'];
        }

        return $this;

    }

    public function get($post, $return = 'array') {
        if( !$post )
            return false;

        if( is_a($post, 'WP_Post') )
            $the_post = $post;

        else if( is_int($post) )
            $the_post = get_post($post);

        else
            return false;


        $return = [];

        $return['ID'] = $the_post->ID ;
        $return['permalink'] = get_post_permalink($the_post->ID);

        foreach( $this->fillable as $key => $type ) {

            $new_key = array_key_exists($key, $this->fillable_map) ? $this->fillable_map[$key] : $key;

            switch ($type) {

                case 'posts':
                    $return[$new_key] = $the_post->{$key};
                    break;

                case 'postmeta':
                    $return[$new_key] = $the_post->{$key};
                    break;

                case 'places':
                    $return[$new_key] = (new Place())->get(((int) get_post_meta( $the_post->ID,'place_id', true)));
                    break;

                case 'submission_details':
                    $return[$new_key]  = $this->getSubmission($the_post->ID);
                    break;

                case 'featured_image':
                    $return[$new_key] = $this->getFeaturedImageSizes($the_post->ID);
                    break;
            }

        }

        $tickets = get_post_meta( $the_post->ID, 'tickets', true );

        if( $tickets ) {
            if( !is_array($tickets) ) {
                $return['tickets'] = $tickets;
            }
            else if( isset($tickets[0]) && isset($tickets[0]['link']) ) {
                $return['tickets'] = $tickets[0]['link'];
            }
        }

        $categories = wp_get_post_terms( $the_post->ID, 'category' );
        $return['categories'] = [];

        if( $categories ) {
            foreach($categories as $category) {
                $return['categories'][] = (array) $category;
            }
        }

        $tags = wp_get_post_terms( $the_post->ID, 'post_tag' );
        $return['tags'] = [];

        if( $tags ) {
            foreach($tags as $tag) {
                $return['tags'][] = (array) $tag;
            }
        }

        $return['dates_formatted'] = $this->getDateRanges($the_post);

        return $return;
    }

    public function getDateRanges($post) {
        $start_month = date('M', strtotime($post->event_start));
        $end_month = date('M', strtotime($post->event_end));

        $start_day = date('d', strtotime($post->event_start));
        $end_day = date('d', strtotime($post->event_end));

        $return['month_start'] = $start_month;
        $return['month_end']    = $end_month;

        if( $start_month != $end_month ) {
            $return['month_range'] = $start_month . ' - ' . $end_month;
        }
        else {
            $return['month_range'] = $start_month;
        }

        $return['day_start'] = $start_day;
        $return['day_end'] = $end_day;

        if( $start_day != $end_day ) {
            $return['day_range'] = $start_day . ' - ' . $end_day;
        }
        else {
            $return['day_range'] = $start_day;
        }

        $return['date'] = date('M d - gA', strtotime($post->event_start));

        return $return;

    }

    public function query() {

        $query = new \WP_Query($this->query_args);


        $return = [];
//        echo '<pre>';
        //print_r($query);

        $this->event_query = $query;

        if( $query->have_posts() ) {
            foreach( $query->posts as $post ) {
                $return[] = $this->get($post);
            }
        }

        $this->max_num_pages = $query->max_num_pages;

        return $return;

    }

    private function getSubmission($event_id) {
        if( !$event_id ) {
            return [];
        }

        if( $name = get_post_meta($event_id, 'event_by_name', true) ) {
            return [
                'first_name'    => $name,
                'last_name'     => get_post_meta($event_id, 'event_by_last_name', true),
                'email'         => get_post_meta($event_id, 'event_by_email', true)
            ];
        }
        else {
            return [];
        }

    }

    public  function getFeaturedImageSizes($event_id) {

        return EventzoneHelpers::getFeaturedImageSizes($event_id);

    }
}