<?php

namespace AM2\EventzoneImporter;

use AM2\EventzoneHelpers;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists('EventzoneSave') ) :


    class EventzoneSave {

        private $external_id = 'eventzone_id';
        public  $the_item;
        private $event_exists;
        private $place_exists;
        private $post_status = 'publish';
        private $event_array;
        private $place_array;


        /*
        *  __construct
        *
        *  saves the passed data in the database. Saves events and places
        *
        *  @param	array $item
        *  @return	n/a
        */

        public function __construct($item, $soft_update = true) {

            $this->the_item = $item;
            $validate = $this->validate();
            if(!$validate) {
                echo PHP_EOL . 'validation failed';
                return false;
            }


            $this->event_array = array(
                'post_title'                => 'posts',
                'post_content'              => 'posts',
                'post_excerpt'              => 'posts',
                'event_start'               => 'postmeta',
                'event_end'                 => 'postmeta',
                'cover_image'               => 'attachment',
                'flyer_image'               => 'attachment',
                'eventzone_id'              => 'postmeta',
                'place_id'                  => 'postmeta',
                'eventzone_event_tags'      => 'taxonomy',
                'category'                  => 'taxonomy',
                'tickets'                   => 'postmeta',
                'organizer_name'            => 'postmeta',
                'organizer_link'            => 'postmeta',
                'featured_event'            => 'postmeta',
                'pinned_event'              => 'postmeta',
                'city'                      => 'postmeta'
            );

            $this->place_array = array(
                'post_title'                => 'posts',
                'post_content'              => 'posts',
                'post_excerpt'              => 'posts',
                'geo_latitude'              => 'postmeta',
                'geo_longitude'             => 'postmeta',
                'address'                   => 'postmeta',
                'city'                      => 'postmeta',
                'phone'                     => 'postmeta',
                'website'                   => 'postmeta',
                'featured_image'            => 'postmeta',
                'eventzone_id'              => 'postmeta',
                'eventzone_place_tags'      => 'taxonomy'
            );

            $this->event_exists = $this->eventExists();
            $this->place_exists = $this->placeExists();



            if(!$this->place_exists) {
                echo 'save_place' . PHP_EOL;
                $place_id = $this->savePlace();
            }
            elseif($this->place_exists && $soft_update) {
                echo PHP_EOL . 'place exists, soft update';
                $this->place_array = array(
                    'post_title'        => 'posts',
                    'post_content'      => 'posts',
                    'post_excerpt'      => 'posts',
                    'geo_latitude'      => 'postmeta',
                    'geo_longitude'     => 'postmeta',
                    'address'           => 'postmeta',
                    'city'              => 'postmeta',
                    'phone'             => 'postmeta',
                    'website'           => 'postmeta',
                    'featured_image'    => 'postmeta',
                    'eventzone_id'      => 'postmeta'
                );
                $place_id = $this->savePlace();
            }

            if(empty($place_id)) {
                // todo add log
                return false;
            }

            if(!$this->event_exists) {
                echo 'save_event' . PHP_EOL;
                $this->saveEvent($place_id);
            }
            elseif($this->event_exists && $soft_update) {
                $this->event_array = array(
                    'post_title'        => 'posts',
                    'post_content'      => 'posts',
                    'post_excerpt'      => 'posts',
                    'event_start'       => 'postmeta',
                    'event_end'         => 'postmeta',
                    'tickets'           => 'postmeta',
                    'organizer_name'    => 'postmeta',
                    'organizer_link'    => 'postmeta'
                );
                echo PHP_EOL . 'event exists, soft update event with ID' .  $this->event_exists . PHP_EOL;
                $this->saveEvent($place_id);
            }


        }


        /*
        *  validate_data
        *
        *  validates the passed data
        *
        *  @param	array $item
        *  @return	bool
        */
        private function validate() {

            // bail early
            if(empty($this->the_item) || !is_array($this->the_item) || empty($this->the_item['eventzone_id'])) {
                return false;
            }

            // check place data

            if(
                empty($this->the_item['location'])
                ||  empty($this->the_item['location']['eventzone_id'])
                ||  empty($this->the_item['location']['post_title'])
                ||  empty($this->the_item['location']['address'])
                ||  empty($this->the_item['location']['city'])
                ||  empty($this->the_item['location']['geo_latitude'])
                ||  empty($this->the_item['location']['geo_longitude'])
            ) {
                // todo add log, this should never happen
                return false;
            }

            // check minimum required event data

            if(empty($this->the_item['post_title']) || empty($this->the_item['event_start'])) {
                return false;
            }

            // todo check other sensitive data and maybe send a log that its empty

            return true;

        }


        /*
        *  save_data
        *
        *  saves the event
        *
        *  @param	array $item
        *  @return	bool
        */

        private function saveEvent($place_id) {
            if($this->event_exists) {
                $ID = $this->event_exists;
            }
            $args = array(
                'post_type'     => 'events',
                'post_title'    => html_entity_decode($this->the_item['post_title']),
                'post_status'   => $this->post_status,
                'post_content'  => $this->the_item['post_content']
            );

            if(empty($ID)) {
                $event_id = wp_insert_post($args);
            }
            else {
                $event_id = $ID;
                unset($args['post_status']);
                $args['ID'] = $event_id;
                wp_update_post($args);
            }



            if(!$event_id) {
                // todo add log wp_insert_post failed
                return false;
            }

            update_post_meta($event_id, 'place_id', $place_id);
            foreach($this->event_array as $key => $value) {
                switch($value) {
                    case 'postmeta':

                        if(!empty($this->the_item[$key])) {
                            update_post_meta($event_id, $key, $this->the_item[$key]);
                        }
                        else if($key == 'featured_event' || $key == 'pinned_event') {
                            update_post_meta($event_id, $key, $this->the_item[$key]);
                        }
                        break;

                    case 'taxonomy':

                        if(!empty($this->the_item[$key])) {
                            foreach($this->the_item[$key] as $external_cat) {
                                if (empty($external_cat)) {
                                    continue;
                                }
                                // check if term exists in the database
                                if (term_exists($external_cat, $key)) {
                                    // if the term exists grab its id
                                    $get_wp_term = get_term_by('name', $external_cat, $key);
                                    $term_to_update = $get_wp_term->term_id;
                                }
                                else {
                                    // if doenst exist, create the term
                                    $wp_inserted_term = wp_insert_term($external_cat, $key);
                                    $term_to_update = $wp_inserted_term['term_id'];
                                }

                                // update the place with the terms/place_type
                                if($term_to_update!=0 && !has_term( $term_to_update, $key, $event_id )) {
                                    //AM2_Debug::message('Added '.$key.': '.$external_cat.'...');
                                    $set_term = wp_set_object_terms($event_id, $term_to_update, $key, true);
                                }
                                else {
                                    // AM2_Errors::add(basename(__FILE__),'Place_Type exists but there was an error when updating taxonomy on Place with ID:'.$final_place_object['ID'].'');
                                }
                            }
                        }

                        break;
                    case 'attachment':
                        if(!empty($this->the_item[$key])) {
                            $attachment_key_exists = get_post_meta($event_id, $key, true);
                            if(empty($attachment_key_exists)):
                                //AM2_Debug::message('Attachment key --'.$key.' does not exist. Adding attachment...');
                                $attachment_id = EventzoneHelpers::insertAttachmentFromUrl($this->the_item[$key], $event_id, get_the_title($event_id));
                                if($attachment_id):
                                    //update_field($key, $final_place_object[$key], $final_place_object['ID'] );
                                    update_post_meta($event_id, $key, $attachment_id);
                                else:
                                    // AM2_Debug::message('problem attaching: ' .$final_place_object[$key]);
                                endif;
                            endif;
                        }

                        break;
                }
            }
            if(!empty($this->the_item['cover_image']) && !$this->event_exists) {
                update_post_meta($event_id, '_thumbnail_id', get_post_meta($event_id, 'cover_image', true));
            }
            elseif(!empty($this->the_item['flyer_image']) && !$this->event_exists) {
                update_post_meta($event_id, '_thumbnail_id', get_post_meta($event_id, 'flyer_image', true));
            }
        }


        /*
        *  save_data
        *
        *  saves the place
        *
        *  @param	array $item
        *  @return	bool
        */

        private function savePlace() {
            if($this->place_exists) {
                $ID = $this->place_exists;
            }
            $args = array(
                'post_type'     => 'places',
                'post_title'    => $this->the_item['location']['post_title'],
                'post_status'   => $this->post_status
            );

            if(empty($ID)) {
                $place_id = wp_insert_post($args);
            }
            else {
                $place_id = $ID;
            }


            if(!$place_id) {
                // todo add log wp_insert_post failed
                return false;
            }

            foreach($this->place_array as $key => $value) {
                switch($value) {
                    case 'postmeta':
                        if(!empty($this->the_item['location'][$key])) {
                            update_post_meta($place_id, $key, $this->the_item['location'][$key]);
                        }
                        break;
                }
            }

            return $place_id;
        }


        /*
        *  event_exists
        *
        *  checks if the event exists
        *
        *  @param	array $item
        *  @return	mixed | array if a place exists, bool otherwise
        */

        private function eventExists() {

            global $wpdb;
            $sql = "SELECT * FROM {$wpdb->prefix}postmeta where meta_key = '{$this->external_id}' and meta_value = '{$this->the_item['eventzone_id']}'";
            $result = $wpdb->get_results($sql);

            if(!empty($result) && $result) {
                if(count($result) > 1) {
                    // todo alert, same place twice
                }
                return $result[0]->post_id;
            }
            return false;
        }


        /*
        *  place_exists
        *
        *  checks if the place exists
        *
        *  @param	array $item
        *  @return	mixed | array if a place exists, bool otherwise
        */

        private function placeExists() {

            global $wpdb;
            $sql = "SELECT * FROM {$wpdb->prefix}postmeta where meta_key = '{$this->external_id}' and meta_value = '{$this->the_item['location']['eventzone_id']}'";
            $result = $wpdb->get_results($sql);

            if(!empty($result) && $result) {
                if(count($result) > 1) {
                    // todo alert, same place twice
                }
                return $result[0]->post_id;
            }
            return false;
        }

    }

endif;
?>