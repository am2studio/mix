<?php

namespace AM2\EventzoneImporter;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

use AM2\EventzoneImporter\Consumers\JSONConsumer;
use AM2\EventzoneImporter\Consumers\JSONWrapper;
use AM2\EventzoneImporter\Consumers\RemoteResourceConsumer;
use AM2\EventzoneImporter\EventzoneSave;
use  AM2\EventzoneImporter\EventzoneSchedule;

if ( ! class_exists( 'EventzoneImporter' ) ) :

    class EventzoneImporter {

        use RemoteResourceConsumer, JSONConsumer, JSONWrapper;

        private $api_type = 'json';
        private $url = 'http://themix.eventzone.co/';
        private $endpoint = 'api/rest/v1/events/';
        private $limit = -1;
        private $max_num_pages;
        private $expected_events;
        private $api_args;
        private $data_returned;
        private $current_page = 1;
        private $run_id;
        public $eventzone_term_mapping;
        protected $schedule;
        private $city;
        protected $page;


        /*
        *  __construct
        *
        *  set the data and start the importer
        *
        *  @param	n/a
        *  @return	n/a
        */

        public function __construct( ) {

            if( get_current_blog_id() === 1 ) {
                return false;
            }

            $this->schedule = new EventzoneSchedule();

            $this->eventzone_term_mapping = get_option('eventzone_terms_mapped');

        }

        public function setCity($city) {
            $this->city = $city;
        }

        public function setPage($page) {
            $this->page = $page;
        }

        public function fetchPages() {
            $this->api_args = array(
                'am2_studio'    =>  '1',
                'tags'          => '4,6,8,9,10,17,18,24,35,38,47,50,52,55,58,61,62,65,67',
                'city'          => $this->city
            );

            $query = http_build_query($this->api_args);
            $url = "{$this->url}{$this->endpoint}?{$query}";

            $fetch  = $this->fetch($url, 1);
            //print_r($fetch['body']);
            $json = JSONWrapper::decode($fetch, TRUE);

            if( isset($json['max_num_pages']) ) {
                return $json['max_num_pages'];
            }
        }

        /*
        *  execute
        *
        *  start the importer
        */

        public function execute() {
            $this->schedule->ping();

            $this->current_page = $this->page;

            $this->fetchData();
            $this->executeLoop();
            $this->schedule->update(['page' => $this->page]);


        }

        /*
        *  fetch_data
        *
        *  fetch data from eventzone
        */

        public function fetchData() {

            // TODO set authentication
            $this->api_args = array(
                'am2_studio'    =>  '1',
                'city'          => $this->city,
                'page'          => $this->current_page
            );

            switch ($this->api_type) {
                case "json":
                    $query = http_build_query($this->api_args);
                    $url = "{$this->url}{$this->endpoint}?{$query}";

                    $fetch  = $this->fetch($url, 1);
                    //print_r($fetch['body']);
                    $json = JSONWrapper::decode($fetch, TRUE);
                    //var_dump($fetch);

                    $this->data_returned = $json;
                    if(empty($this->max_num_pages)) {
                        $this->max_num_pages =  (int) $this->data_returned['max_num_pages'];
                        $this->schedule->update(array('total_pages' => $this->max_num_pages), $this->run_id);
                    }

                    break;
            }

        }

        /*
        *  execute_loop
        *
        *  loop trough all items in one page of the api
        *
        */
        public function executeLoop() {

            foreach($this->data_returned['posts'] as $item) {
                $mapped_item = $this->map($item);
                $this->executeLoopItem($mapped_item);
            }
            //echo 'current_page is :' . $this->current_page . PHP_EOL;
            $this->schedule->update(array('page' => $this->current_page));
//            $this->increment_page();
//            $this->fetchData();
            $this->schedule->ping();

        }


        /*
        *  map_event_data
        *
        *  transfers the event data in the expected format
        *
        *  @param	array $item
        *  @return	array $mapped
        */

        public function map($item) {

            $mapped =  array(
                'post_title'                => $item['title'],
                'eventzone_id'              => $item['global_id'],
                'post_content'              => $item['description'],
                'post_excerpt'              => $item['summary'],
                'event_start'               => $item['event_start'],
                'event_end'                 => $item['event_end'],
                'eventzone_event_tags'      => $item['tags'],
                'city'                      => $item['location']['city'],
                'featured_event'            => '0',
                'pinned_event'              => '0',
                'location'          => array(
                    'eventzone_id'              => $item['location']['global_id'],
                    'post_title'                => $item['location']['title'],
                    'address'                   => $item['location']['address'],
                    'city'                      => $item['location']['city'],
                    'geo_latitude'              => $item['location']['geo_latitude'],
                    'geo_longitude'             => $item['location']['geo_longitude'],
                    'eventzone_place_tags'      => $item['location']['tags']
                )
            );

            if( isset($item['tickets']) && !empty($item['tickets']) ) {
                $mapped['tickets'] = $item['tickets'][0]['link'];
            }

            $mapped = $this->checkCity($mapped);

            if( !empty( $this->eventzone_term_mapping ) && !empty($mapped['eventzone_event_tags']) ) {

                foreach( $mapped['eventzone_event_tags'] as $event_tag ) {

                    $eventzone_term = get_term_by( 'name', $event_tag, 'eventzone_event_tags' );

                    if( empty($eventzone_term) ) {
                        continue;
                    }

                    if( isset( $this->eventzone_term_mapping[$eventzone_term->slug] ) ) {

                        $internal_term = get_term_by( 'slug', $this->eventzone_term_mapping[$eventzone_term->slug], 'category' );

                        $mapped['category'][] = $internal_term->name;
                    }
                }


            }


            if(!empty($item['cover_image']['full']['file'])) {
                $mapped['cover_image'] = $item['cover_image']['full']['file'];
            }
            if(!empty($item['flyer_image']['full']['file'])) {
                $mapped['flyer_image'] = $item['flyer_image']['full']['file'];
            }
            if(isset($item['organizer'])) {
                $mapped['organizer_name'] = $item['organizer']['name'];
            }
            return $mapped;


        }


        /*
        *  check_city
        *
        *  checks if a city is defined in eventzone()->locations
        *
        */
        private function checkCity($mapped_data) {

            if( !isset($mapped_data['city']) ) {
                return $mapped_data;
            }

            $same_city = false;

            if( !empty(eventzone()->locations) ) {
                foreach( eventzone()->locations as $location ) {
                    if( strtolower($mapped_data['city']) == strtolower($location) ) {
                        $same_city = true;
                        break;
                    }
                }
            }

            if( !$same_city ) {

                $city = array_keys($this->checkDistance($mapped_data), min($this->checkDistance($mapped_data)));

                if( !empty($city[0]) ) {
                    $mapped_data['city'] = $city[0];
                    $mapped_data['location']['city'] = $city[0];
                }
            }

            return $mapped_data;
        }

        /*
        *  increment_page
        *
        *  increments the page for the api
        *
        */

        private function increment_page() {
            $this->current_page = $this->current_page + 1;
            $this->api_args['page'] = $this->api_args['page'] + 1;
        }

        /*
        *  check_distance
        *
        *  check distance from the current city to the cities definied in eventzone()->locations
        *
        */

        public function checkDistance($mapped_data, $earthRadius = 6371000) {

            $latFrom = deg2rad($mapped_data['location']['geo_latitude']);
            $lonFrom = deg2rad($mapped_data['location']['geo_longitude']);

            $distance_to_major_city = array();
            foreach(eventzone()->location_coordinates as $city => $coordinates) {
                foreach($coordinates as $key => $value) {
                    if($key == 'lat') {
                        $latTo = deg2rad($value);
                    }
                    else {
                        $lonTo = deg2rad($value);
                    }
                }
                $lonDelta = $lonTo - $lonFrom;
                $a = pow(cos($latTo) * sin($lonDelta), 2) +
                    pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
                $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

                $angle = atan2(sqrt($a), $b);
                $distance_to_major_city[$city] = $angle * $earthRadius;
            }
            return $distance_to_major_city;
        }


        /*
        *  execute_loop_item
        *
        *  wraps the save data function and adds statistic data
        *
        */

        private function executeLoopItem($item) {
            // todo count batch
            $this->save($item);
        }

        public function save($item) {
            $save = new EventzoneSave($item, true);
        }

    }
endif;

?>