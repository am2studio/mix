<?php

namespace AM2\EventzoneImporter;

use AM2\BackgroundProcessing\WP_Background_Process;

class EventzoneRunner extends WP_Background_Process {

    /**
     * @var string
     */
    protected $action = 'eventzone_process';

    protected $eventzone_importer;

    public function __construct( EventzoneImporter $importer )
    {
        parent::__construct();
        $this->eventzone_importer = $importer;
    }

    protected function task( $item ) {

        $this->eventzone_importer->setPage( $item );

        $this->eventzone_importer->execute();

        return false;
    }

    protected function complete() {
        parent::complete();


    }

}