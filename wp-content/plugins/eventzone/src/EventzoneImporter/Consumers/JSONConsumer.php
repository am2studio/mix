<?php

namespace AM2\EventzoneImporter\Consumers;

trait JSONConsumer {

    function parse ($json_string) {
        return json_decode($json_string, true);
    }

}