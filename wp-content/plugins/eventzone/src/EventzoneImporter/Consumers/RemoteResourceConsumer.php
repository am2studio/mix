<?php

namespace AM2\EventzoneImporter\Consumers;

trait RemoteResourceConsumer {

    function fetch($url, $try, $max_retry = 10, $timeout = 900, $args = [ 'sslverify' => false ]) {

        if ($try > $max_retry) {
            return false;
        }

        $response = wp_remote_get( $url, $args );



        if (is_wp_error($response)) {
            $try = $try + 1;
            $this->fetch($url, $try, $max_retry, $timeout, $args);
            return;
        }
        return $response['body'];
    }

}