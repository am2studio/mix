<?php

namespace AM2\EventzoneImporter;

class EventzoneSchedule {

    protected $site_id;

    public function __construct() {

        $this->site_id = get_current_blog_id();
        if( $this->site_id === 1 ) {

        }
        $this->createTable();
    }

    private function createTable() {
        global $wpdb;
        if ($wpdb->get_var("show tables like 'eventzone_schedules'") != 'eventzone_schedules') {

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            $sql = "CREATE TABLE `eventzone_schedules` (
				  `ID` INT(255) NOT NULL AUTO_INCREMENT,
				  `site_id` INT(255) NOT NULL,
				  `start` DATETIME NOT NULL,
				  `end` DATETIME NOT NULL,
				  `ping` DATETIME NOT NULL,
				  `page` INT(255) NOT NULL,
				  `total_pages` INT(255),
				  UNIQUE KEY `ID` (`ID`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            dbDelta($sql);

        }
    }

    public function start() {
        $last_run = $this->getLastSchedule();

        $status = $this->status();

        if( empty($last_run->end) ) {
            \wp_mail('mario.davchevski@myzone.com', 'THEMIX schedule didnt finish', 'Schedule with ID');
        }

        if( $status['status'] == 'start' ) {

            global $wpdb;
            $wpdb->insert('eventzone_schedules', array('site_id' => $this->site_id, 'start' => date('Y-m-d H:i:s')));
            if( $wpdb->insert_id ) {
                return [
                    'run_id'    => $wpdb->insert_id,
                    'page'      => 1
                    ];
            }
        }
        else if( $status['status'] == 'continue' ) {
            return $status['args'];
        }

    }

    public function add($pages_to_fetch) {
        global $wpdb;
        $last_schedule = $this->getLastSchedule();
        if( !$last_schedule ) {
            $wpdb->insert(
                'eventzone_schedules',
                array(
                    'site_id' => $this->site_id,
                    'start' => date('Y-m-d H:i:s'),
                    'total_pages' => $pages_to_fetch
                )
            );
            return $wpdb->insert_id;
        }

        $this->ping();

    }

    public function end() {
        global $wpdb;
        $wpdb->update('eventzone_schedules', array('end' => date('Y-m-d H:i:s')), array('site_id' => $this->site_id));
    }

    public function update($array) {
        global $wpdb;
        $wpdb->update('eventzone_schedules', $array, array('site_id' => $this->site_id));
    }

    public function ping() {
        global $wpdb;
        $wpdb->update('eventzone_schedules', array('ping' => date('Y-m-d H:i:s')), array('site_id' => $this->site_id));
    }

    public function getLastSchedule() {
        global $wpdb;
        $sql = "SELECT * FROM eventzone_schedules WHERE site_id = {$this->site_id} ORDER BY ID DESC LIMIT 1";
        $run = $wpdb->get_row($sql);
        return $run;
    }

    public function status() {
        $last_run = $this->getLastSchedule();

        if( !$last_run ) {
            return ['status' => 'start'];
        }

        if( $last_run->end == '0000-00-00 00:00:00' ) {
            if( $last_run->page == $last_run->total_pages ) {
                $this->update( array('end' => date('Y-m-d H:i:s')) );
                return false;
            }

            return [
                'status'    => 'continue',
                'args'      => [
                    'run_id'    => $last_run->ID,
                    'page'      => (int) $last_run->page
                ]
            ];
        }
        $now = date('Y-m-d H:i:s');

        $hourdiff = round((strtotime($now) - strtotime($last_run->start))/3600, 1);
        if($hourdiff >= 24) {
            return ['status' => 'start'];
        }
        return false;
    }

}