<?php
/**
 * Plugin Name: Eventzone
 * Plugin URI: https://am2studio.hr
 * Description: Pulling events and locations from eventZone
 * Version: 1.1
 * Author: Am2 Studio
 * Author URI: https://am2studio.hr
 * Text Domain: eventzone
 */

if( file_exists( dirname( __FILE__) ) . '/vendor/autoload.php' ) {
    require_once(dirname( __FILE__) . '/vendor/autoload.php');
}

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

use AM2\EventzoneCPT;
use AM2\EventzoneTracking;
use AM2\EventzoneTax;
use AM2\Admin\EventzoneSettings;
use AM2\Admin\EventzoneMenu;
use AM2\EventzoneImporter\EventzoneImporter;
use AM2\EventzoneImporter\EventzoneSchedule;
use AM2\EventzoneImporter\EventzoneRunner;
use AM2\Event;

if ( !class_exists( 'Eventzone' ) ) :

    final class Eventzone {

        protected static $_instance = null;

        protected $tracking = true;

        private $version = '1.1';
        public $events_slug = 'events';
        public $places_slug = 'places';
        public $views_table_name = 'eventzone_events_views';
        public $default_tax_to_map_into = 'category';
        public $mapped_option_name = 'eventzone_terms_mapped';
        public $location;
        protected $importer;
        protected $schedule;

        protected $process;

        public $ads_option = 'eventzone_ads_code';

        public $events;

        public $upload_page_fields = array(
            'main_title'                        => 'title',
            'main_description'                  => 'desc',
            'your_details_title'                => 'title',
            'your_details_description'          => 'desc',
            'event_details_title'               => 'title',
            'event_details_description'         => 'desc',
            'location_details_title'            => 'title',
            'location_details_description'      => 'desc',
            'email_notifications'               => 'desc',
            'thank_you_page'                    => 'page/select'
        );

        public $upload_page_option_values;

        public $upload_page_option = 'eventzone_upload_page_text';

        // must be same as locations. Used when mapping data in eventzone_importer::check_distance
        public $location_coordinates = array(
            'Vancouver' => array(
                'lat' => '49.246292',
                'long' => '-123.116226'
            )
        );

        public function __construct() {

            // create CPTs
            new EventzoneCPT();

            // create Taxonomies
            new EventzoneTax();

            // track events?
            if( $this->tracking ) {
                new EventzoneTracking();
            }

            $this->events = new Event();

            add_action('admin_init', array($this, 'runImporter'));

            //add_action('admin_init', array($this, 'fixEventTickets'));

            add_action('eventzone_schedule', array('EventzoneSchedule', 'run'));

            $this->defineConstants();

            if( $this->isRequest('admin') ) {
                //new EventzoneSettings();
                new EventzoneMenu();
            }

            $this->location = get_option('eventzone_city');

            $this->categories = get_terms( 'category', array(
                'hide_empty' => false
            ) );

            $this->upload_page_option_values = get_option($this->upload_page_option);

            add_action( 'admin_init', array( $this, 'processHandler' ) );

            $this->importer = new EventzoneImporter();
            $this->schedule = new EventzoneSchedule();
        }


        public function fixEventTickets() {
            return false;
            if( isset($_GET['fixtickets']) && $_GET['fixtickets'] == '1' ) {
                $query = new WP_Query(
                    [
                        'post_type'         => ['events'],
                        'posts_per_page'    => -1,
                        'post_status'       => ['publish', 'draft']
                    ]
                );

                if( $query->posts ) {



                    foreach($query->posts as $post) {

                        $tickets = get_post_meta($post->ID, 'tickets', true);

                        if( is_array($tickets) && isset($tickets[0]['link']) ) {
                            update_post_meta($post->ID, 'tickets', $tickets[0]['link']);
                        }
                    }
                }
            }
        }


        public function processHandler() {


            $this->importer->setCity($this->location);

            $this->process = new EventzoneRunner($this->importer);

            $schedule_status = $this->schedule->status();

            if( $schedule_status['status'] === 'start' ) {

                $pages_to_fetch = $this->importer->fetchPages();
                $this->schedule->add($pages_to_fetch);
                if( !is_int($pages_to_fetch) ) {
                    return false;
                }
                for ($i=1; $i <= $pages_to_fetch; $i++) {
                    $this->process->push_to_queue( $i );
                }

                $this->process->save()->dispatch();
            }

            //new EventzoneSchedule(get_current_blog_id());
        }

        public function runImporter() {
            if( isset($_GET['run']) ) {
                $importer = new EventzoneImporter();

                $importer->setCity('Vancouver');
                $importer->setPage(1);
                $importer->execute();
            }
        }

        /**
         * Main Eventzone Instance.
         *
         * Ensures only one instance of Eventzone is loaded or can be loaded.
         *
         * @since 1.0
         * @static
         * @return Eventzone Name - Main instance.
         */
        public static function instance()
        {
            if ( is_null( self::$_instance ) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        /**
         * Define RAF Constants.
         * @since 1.0
         */
        private function defineConstants() {
            $this->define( 'EVENTZONE_URL', plugin_dir_url( __FILE__ ) );
            $this->define( 'EVENTZONE_ABSPATH', dirname( __FILE__ ) . '/' );
            $this->define( 'EVENTZONE_VERSION', $this->getVersion() );
            $this->define( 'EVENTZONE_BASENAME', plugin_basename( __FILE__ ) );
        }

        /**
         * Define constant if not already set.
         *
         * @since  1.0
         * @param  string $name
         * @param  string|bool $value
         */
        private function define( $name, $value ) {
            if ( ! defined( $name ) ) {
                define( $name, $value );
            }
        }

        /**
         * What type of request is this?
         *
         * @param  string $type admin, ajax, cron or frontend.
         * @return bool
         */
        private function isRequest( $type ) {
            switch ( $type ) {
                case 'admin' :
                    return is_admin();
                case 'ajax' :
                    return defined( 'DOING_AJAX' );
                case 'cron' :
                    return defined( 'DOING_CRON' );
                case 'frontend' :
                    return ( ! is_admin() || defined( 'DOING_AJAX' ) ) && ! defined( 'DOING_CRON' );
            }
        }

        /**
         * Returns Plugin version for global
         * @since  1.0
         */
        private function getVersion()
        {
            return $this->version;
        }

        /**
         * activation_methods
         *
         * Calls methods on activation
         * @since  1.0
         */

        static function activationMethods() {
            self::createTables();
            self::scheduler();
        }

        /**
         * deactivation_methods
         *
         * Calls methods on deactivation
         * @since  1.0
         */

        static function deactivation_methods() {
            self::remove_scheduler();
        }

        static function createTables() {
            global $wpdb;
            $table_name = $wpdb->prefix . 'eventzone_events_views';
            if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
                require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
                $sql = "CREATE TABLE {$table_name} (
				  `post_id` bigint(20) NOT NULL,
				  `first_viewed` datetime NOT NULL,
				  `last_viewed` datetime NOT NULL,
				  `pageviews` bigint(20) DEFAULT '1',
				  PRIMARY KEY (`post_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
			";
                dbDelta($sql);

            }
        }

        public function test() {

        }

        static function scheduler() {
            wp_schedule_event(time(), 'twicedaily', 'eventzone_schedule');
        }

        static function remove_scheduler() {
            wp_clear_scheduled_hook('eventzone_schedule');
        }

    }
endif;

register_activation_hook( __FILE__, array( 'eventzone', 'activationMethods') );
register_deactivation_hook(__FILE__, array( 'eventzone', 'deactivation_methods' ));
/**
 * Main instance of Plugin.
 *
 * Returns the main instance of plugin to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return Eventzone
 */
function eventzone() {
    return eventzone::instance();
}
eventzone();