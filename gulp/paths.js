//import glob from 'glob';

export default {
	theme: './wp-content/themes/am2/',   
	
	entry: {
		/* css: './wp-content/themes/am2/assets/css/*.css', */
		svg: './wp-content/themes/am2/resources/svg-builder/**/*.svg',
		scss: {
			watch: './wp-content/themes/am2/resources/scss/**/*',
			main: './wp-content/themes/am2/resources/scss/style.scss' 
		},
		js: {
			watch: './wp-content/themes/am2/resources/js/**/*',
			main: './wp-content/themes/am2/resources/js/am2-bundle.js' 
		}
	},

	output: {
		build: './wp-content/themes/am2/assets/build/',
		/* css: './wp-content/themes/am2/assets/css/', */
		svg: './wp-content/themes/am2/assets/images/',
		/* js: './wp-content/themes/am2/assets/js/' */
	}
};