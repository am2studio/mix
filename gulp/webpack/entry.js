const glob = require('glob');

const entryArray = [
	...glob.sync('./wp-content/themes/am2/resources/js/routes/*.js'), 
	'./wp-content/themes/am2/resources/js/am2-bundle.js', 
	/* './wp-content/themes/am2/resources/js/gutenberg/index.js' */
];

const entryObject = entryArray.reduce((acc, item) => {
	const name = item.replace('./wp-content/themes/am2/resources/js/','').replace('.js', ''); 
	acc[name] = item;
	return acc;
}, {});

module.exports = entryObject;